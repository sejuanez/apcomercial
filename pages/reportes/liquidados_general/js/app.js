// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectMunicipio: [],
        valorMun: "",

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectComercializador: [],
        valorComercializador: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: "Comercializador",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

    },


    methods: {

        loadClientes() {

            var app = this;

            app.selectUsuarios = [];

            $.post('./request/getClientes.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {

                //console.log(data)

                var usuarios = JSON.parse(data);

                var uPropios = usuarios['propio'];
                var uComer = uPropios.concat(usuarios['comer'])

                app.selectUsuarios = uComer;

            });

        },

        onChangeCliente(codClient) {
            var app = this;
            app.valorUsuario = codClient;
        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                console.log(data)
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
        },

        loadComercializador: function () {

            var app = this;

            $.post('./request/getComercializador.php', function (data) {
                app.selectComercializador = JSON.parse(data);
            });

        },

        loadCamposTabla: function () {

            var app = this;

            $.post('./request/getCamposConsumo.php', {
                mun: app.valorMun
            }, function (data) {
                //console.log(data)
                app.camposTabla = JSON.parse(data);


                if (app.camposTabla.length <= 0) {
                    alertify.error('Este municipio no cuenta con una configuracion de campos para importacion')
                } else {

                    app.verificar();
                }

            });

        },

        onChangeComercializador: function (cod) {
            var app = this;
            app.valorComercializador = cod;
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectComercializador')

            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];

            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;
            //console.log(app.valorMun)
            //console.log(app.valorAño)
            //console.log(app.valorMes)
            //console.log(app.valorComercializador)


            if (
                app.valorMun == ""
            ) {

                alertify.error("Por favor selecciona un municipio")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                    cliente: app.valorUsuario
                }, function (data) {

                    console.log(data)

                    try {
                        var array = JSON.parse(data);
                        app.tablaPropios = array['propio']
                        app.tablaNoPropios = array['comer']

                        var totalPropios = 0;
                        var totalComer = 0;

                        for (i in app.tablaNoPropios) {
                            totalComer += parseFloat(app.tablaNoPropios[i]['vr_alumb'])
                        }

                        for (i in app.tablaPropios) {
                            totalPropios += parseFloat(app.tablaPropios[i]['vr_alumb'])
                        }

                        app.sumaPropio = "Total Liquidado: $" + totalPropios;
                        app.sumaComer = "Total Liquidado: $" + totalComer;

                        if (app.tabActive == 1) {
                            app.sumatoria = app.sumaComer
                        } else {
                            app.sumatoria = app.sumaPropio
                        }

                        app.textComercializador = "Comercializador " + "(" + app.tablaNoPropios.length + ")";
                        app.textPropio = "Especiales Propios " + "(" + app.tablaPropios.length + ")";

                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );

                        $("#btnExportarPropio a").attr("href",
                            "r" +
                            "equest/exportarPropio.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );
                    } catch (e) {
                        alertify.error('Error')
                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        this.loadComercializador();


    },

});
