<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");


$mun = utf8_decode($_POST['mun']);
$anio = utf8_decode($_POST['anio']);
$mes = utf8_decode($_POST['mes']);
$comer = utf8_decode($_POST['comer']);
$cliente = utf8_decode($_POST['cliente']);

$return_arr = array();
$array_comer = array();
$array_propio = array();

$whereAnio = "";
if ($anio != "") {
    $whereAnio = " AND l.l_ano = " . $anio;
}

$whereMes = "";
if ($mes != "") {
    $whereMes = " AND l.l_mes = " . $mes;
}

$whereComer = "";
if ($comer != "") {
    $whereComer = " AND l.l_idcomer = " . $comer;
}

$whereCliente = "";
if ($cliente != "") {
    $whereCliente = " AND l.l_cliente = " . $cliente;
}

$stmt = "       select
                   l.l_empresa,
                    l.l_ano,
                    l.l_mes,
                    l.l_cliente,
                    c.c_nom_cliente,
                    c.c_zona,
                    c.c_tarifa,
                    c.c_nom_tarifa,
                    l.l_tarifa,
                    l.l_consumo_vr,
                    l.l_consumo_kwh,
                    0,
                    l.l_vr_alumb,
                    0
                from liquidados l
                inner join consumo  c on c.c_cliente = l.l_cliente and
                                    c.c_consumo_ano = l.l_ano and
                                    c.c_consumo_mes = l.l_mes and
                                    c.c_idcomercializ = l.l_idcomer and
                                    c.c_cliente = l.l_cliente
                inner join tarifa t on t.t_cod = c.c_tarifa and t.t_empresa = l.l_empresa
                where l.l_empresa =  '" . $mun . "' " . $whereAnio . $whereMes . $whereComer . $whereCliente . "                                   
";

$result = ibase_query($conexion, $stmt);


while ($fila = ibase_fetch_row($result)) {
    $row_array['mun'] = utf8_encode($fila[0]);
    $row_array['anio'] = utf8_encode($fila[1]);
    $row_array['mes'] = utf8_encode($fila[2]);
    $row_array['cliente'] = utf8_encode($fila[3]);
    $row_array['nom_cliente'] = utf8_encode($fila[4]);
    $row_array['zona'] = utf8_encode($fila[5]);
    $row_array['tarifa'] = utf8_encode($fila[6]);
    $row_array['nomTarifa'] = utf8_encode($fila[7]);
    $row_array['tarifa_l'] = utf8_encode($fila[8]);
    $row_array['consumo_vr_l'] = utf8_encode($fila[9]);
    $row_array['consumo_kwh_l'] = utf8_encode($fila[10]);
    $row_array['vr_fact'] = utf8_encode($fila[11]);
    $row_array['vr_alumb'] = utf8_encode($fila[12]);
    $row_array['diferencia'] = utf8_encode($fila[13]);
    array_push($array_comer, $row_array);
}


$stmt = "       select  l.l_empresa,
                        l.l_ano,
                        l.l_mes,
                        l.l_cliente,
                        ep.ep_nom,
                        e.e_zona,
                        e.cod_tarifa,
                        t.t_nom,
                        l.l_tarifa,
                        l.l_consumo_vr,
                        l.l_consumo_kwh,
                        0,
                        l.l_vr_alumb,
                        0
                from liquidados l
                join especiales e on e.cod_cliente = l.l_cliente and e.e_propio = '1'
                inner join especiales_propio ep on ep.ep_cod = l.l_cliente
                inner join tarifa t on t.t_cod = e.cod_tarifa and t.t_empresa = l.l_empresa
                left join fact_comerc f on f.fc_munic = l.l_empresa
                         and f.fc_ano = l.l_ano
                         and f.fc_mes = l.l_mes
                         and f.cod_comer = l.l_idcomer
                         and f.fc_cliente = l.l_cliente
                where l.l_empresa =  '" . $mun . "' " . $whereAnio . $whereMes . $whereComer . $whereCliente . "                                   
";

$result = ibase_query($conexion, $stmt);

while ($fila = ibase_fetch_row($result)) {
    $row_array['mun'] = utf8_encode($fila[0]);
    $row_array['anio'] = utf8_encode($fila[1]);
    $row_array['mes'] = utf8_encode($fila[2]);
    $row_array['cliente'] = utf8_encode($fila[3]);
    $row_array['nom_cliente'] = utf8_encode($fila[4]);
    $row_array['zona'] = utf8_encode($fila[5]);
    $row_array['tarifa'] = utf8_encode($fila[6]);
    $row_array['nomTarifa'] = utf8_encode($fila[7]);
    $row_array['tarifa_l'] = utf8_encode($fila[8]);
    $row_array['consumo_vr_l'] = utf8_encode($fila[9]);
    $row_array['consumo_kwh_l'] = utf8_encode($fila[10]);
    $row_array['vr_fact'] = utf8_encode($fila[11]);
    $row_array['vr_alumb'] = utf8_encode($fila[12]);
    $row_array['diferencia'] = utf8_encode($fila[13]);
    array_push($array_propio, $row_array);
}


$return_arr = array(
    "comer" => $array_comer,
    "propio" => $array_propio
);

echo json_encode($return_arr);
// echo json_encode($array);



