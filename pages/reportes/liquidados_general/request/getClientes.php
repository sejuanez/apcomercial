<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");


$mun = utf8_decode($_POST['mun']);
$anio = utf8_decode($_POST['anio']);
$mes = utf8_decode($_POST['mes']);
$comer = utf8_decode($_POST['comer']);

$return_arr = array();
$array_comer = array();
$array_propio = array();

$whereAnio = "";
if ($anio != "") {
    $whereAnio = " AND l.l_ano = " . $anio;
}

$whereMes = "";
if ($mes != "") {
    $whereMes = " AND l.l_mes = " . $mes;
}

$whereComer = "";
if ($comer != "") {
    $whereComer = " AND l.l_idcomer = " . $comer;
}

$stmt = "       select distinct
                    l.l_cliente,
                    c.c_nom_cliente
                from liquidados l
                inner join consumo  c on c.c_cliente = l.l_cliente and
                                    c.c_consumo_ano = l.l_ano and
                                    c.c_consumo_mes = l.l_mes and
                                    c.c_idcomercializ = l.l_idcomer and
                                    c.c_cliente = l.l_cliente
                where l.l_empresa =  '" . $mun . "' " . $whereAnio . $whereMes . $whereComer . "                                   
";

$result = ibase_query($conexion, $stmt);


while ($fila = ibase_fetch_row($result)) {
    $row_array['cliente'] = utf8_encode($fila[0]);
    $row_array['nom_cliente'] = ($fila[1]);
    array_push($array_comer, $row_array);
}


$stmt = "       select distinct 
                        l.l_cliente,
                        ep.ep_nom

                from liquidados l
                join especiales e on e.cod_cliente = l.l_cliente and e.e_propio = '1'
                inner join especiales_propio ep on ep.ep_cod = l.l_cliente
                where l.l_empresa =  '" . $mun . "' " . $whereAnio . $whereMes . $whereComer . "                                   
";

$result = ibase_query($conexion, $stmt);

while ($fila = ibase_fetch_row($result)) {
    $row_array['cliente'] = utf8_encode($fila[0]);
    $row_array['nom_cliente'] = utf8_encode($fila[1]);
    array_push($array_propio, $row_array);
}


$return_arr = array(
    "comer" => $array_comer,
    "propio" => $array_propio
);

echo json_encode($return_arr);
// echo json_encode($array);



