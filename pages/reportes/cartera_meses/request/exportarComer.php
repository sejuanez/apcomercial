<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
ini_set('max_execution_time', 0);

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$mes = $_GET['mes'];
$mesFin = $_GET['mesFin'];

$fecha = "";

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Expires: 0");

//si solo se selecciono el municipio
if ($anio == "" && $mes == "" && $anioFin == "" && $mesFin == "") {

    $fecha = "HISTORICO";

    $stmt = "Select * from INF_CARTERA_MESES( '1' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,                                                  
                                                      null,
                                                      null
                                                      )";

    //
} else if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

    $fecha = $anio . "_" . $mes;

    $stmt = "Select * from INF_CARTERA_MESES( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

} else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

    $fecha = $anio . "_" . $mes . "_Hasta_" . $anioFin . "_" . $mesFin;

    $stmt = "Select * from INF_CARTERA_MESES( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mesFin . "' 
                                                      )";

}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();

header("Content-Disposition: filename=Cartera_Generada_" . $mun . "_" . $fecha . " .xls");

$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='8'>Cartera Generada - $mun - $fecha</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Us Fact</th>" .
    "<th>Facturado</th>" .
    "<th>Us Rec</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

$usFact = 0;
$facturado = 0;
$usRec = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {

    $usFact += intval($fila[3]);
    $facturado += intval($fila[4]);
    $usRec += intval($fila[5]);
    $recaudado += intval($fila[8]);
    $generada += intval($fila[9]);
    $recuperada += intval($fila[10]);


    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[8]) . "</td>" .
        "<td>" . intval($fila[9]) . "</td>" .
        "<td>" . intval($fila[10]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $usFact . "</td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $usRec . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

