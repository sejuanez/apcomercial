// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        totalFacturado: 0,
        totalRecaudado: 0,
        totalRecuperada: 0,
        totalGenerada: 0,


    },


    methods: {


        modalUsuario: function () {
            var app = this;
            if (app.valorMun != "") {
                $("#inputBusqueda").focus();
                app.tablaClientes = [];
                $('#modalMateriales').modal('show');
            } else {
                alertify.warning('Por favor seleccione un municipio')
            }
        },

        getUsuarios: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                    mun: app.valorMun,
                    busqueda: app.buscarUsuario,
                },

                function (data) {
                    var data = JSON.parse(data);
                    app.tablaClientes = data;
                }
            )
            ;
        },

        seleccionarCliente: function (data) {
            var app = this;
            app.idUsuario = data.codigo;
            app.nombreUsuario = data.codigo + " -  " + data.nombre;
            app.buscarUsuario = "";
            $('#modalMateriales').modal('hide');
        },

        loadClientes() {

            var app = this;

            app.selectUsuarios = [];

            $.post('./request/getClientes.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {

                //console.log(data)

                var usuarios = JSON.parse(data);

                var uPropios = usuarios['propio'];
                var uComer = uPropios.concat(usuarios['comer'])

                app.selectUsuarios = uComer;

            });

        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectAñoFin')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectMesFin')
            app.enableSelect2('.selectComercializador')


            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;
            app.totalRecaudado = ""
            app.totalFacturado = ""
            app.totalRecuperada = 0
            app.totalGenerada = ""


            if (
                app.valorMun == ""
            ) {

                alertify.error("Por favor selecciona municipio")

            } else {

                //app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectAñoFin')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectMesFin')
                app.disableSelect2('.selectComercializador')

                console.log(app.valorMun)
                console.log(app.idUsuario)
                console.log(app.valorAño)
                console.log(app.valorMes)
                console.log(app.valorAñoFin)
                console.log(app.valorMesFin)

                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    cliente: app.idUsuario,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    anioFin: app.valorAñoFin,
                    mesFin: app.valorMesFin,
                }, function (data) {

                    console.log(data)

                    try {
                        var array = JSON.parse(data);
                        app.tablaHistorico = array;

                        var auxRecaudado = 0;
                        var auxFacturado = 0;

                        for (i in app.tablaHistorico) {

                            auxRecaudado += parseFloat(app.tablaHistorico[i].recaudado);
                            auxFacturado += parseFloat(app.tablaHistorico[i].facturado);

                        }


                        // app.totalRecaudado = "$ " + auxRecaudado.toLocaleString();
                        //app.totalFacturado = "$ " + auxFacturado.toLocaleString();

                        var saldo = auxFacturado - auxRecaudado;

                        console.log(saldo)

                        if (saldo < 0) {

                            app.totalRecuperada = (saldo * -1);

                        } else if (saldo > 0) {

                            app.totalGenerada = saldo;

                        } else {

                            app.totalGenerada = saldo;
                            app.totalRecuperada = saldo;

                        }

                        //app.sumaComer = "Cart Recuperada: $" + app.totalRecuperada.toLocaleString() + " - " + "Cart Generada: $" + app.totalGenerada.toLocaleString();

                        app.totalRecaudado = "$ " + auxRecaudado.toLocaleString();
                        app.totalFacturado = "$ " + auxFacturado.toLocaleString();
                        app.totalRecuperada = "$ " + app.totalRecuperada.toLocaleString();
                        app.totalGenerada = "$ " + app.totalGenerada.toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&cliente=" + app.idUsuario
                        );


                        /*
                        var totalPropios = 0;
                        var totalComer = 0;

                        for (i in app.tablaNoPropios) {
                            totalComer += parseFloat(app.tablaNoPropios[i]['vr_alumb'])
                        }

                        for (i in app.tablaPropios) {
                            totalPropios += parseFloat(app.tablaPropios[i]['vr_alumb'])
                        }

                        app.sumaPropio = "Total Liquidado: $" + totalPropios;
                        app.sumaComer = "Total Liquidado: $" + totalComer;

                        if (app.tabActive == 1) {
                            app.sumatoria = app.sumaComer
                        } else {
                            app.sumatoria = app.sumaPropio
                        }

                        app.textComercializador = "Comercializador " + "(" + app.tablaNoPropios.length + ")";
                        app.textPropio = "Especiales Propios " + "(" + app.tablaPropios.length + ")";

                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );

                        $("#btnExportarPropio a").attr("href",
                            "r" +
                            "equest/exportarPropio.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );


                        */
                    } catch (e) {
                        alertify.error('Busqueda mal formulada')
                    }


                    app.btn_cancelar = true;
                    //app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();

    },

});
