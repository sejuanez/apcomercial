<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=Reporte.xls");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$cliente = $_GET['cliente'];

$stmt = "Select * from REPORTE_HISTORICO('1' , '" . $mun . "', '" . $cliente . "')";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Facturado</th>" .
    "<th>Recaudado</th>" .
    "<th>Cartera Recuperada</th>" .
    "<th>Cartera Generada</th>" .
    "</tr>";

while ($fila = ibase_fetch_row($result)) {

    $generada = 0;
    $recuperada = 0;

    if (floatval($fila[4]) < 0) {

        $generada = (floatval($fila[4]) * -1);

    } else {

        $recuperada = floatval($fila[4]);

    }

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . ($fila[3]) . "</td>" .
        "<td>" . $generada . "</td>" .
        "<td>" . $recuperada . "</td>" .
        "</tr>";

}

$tabla .= "</table>";

echo $tabla;

