// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        usuarioBuscar: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        totalFacturado: 0,
        totalFacturas: 0,
        totalRecaudos: 0,
        totalRecaudado: 0,
        totalRecuperada: 0,
        totalGenerada: 0,


    },


    methods: {

        modalUsuario: function () {
            var app = this;
            if (app.valorMun != "") {
                $("#inputBusqueda").focus();
                app.tablaClientes = [];

                if (app.usuarioBuscar != "") {
                    app.buscarUsuario = app.usuarioBuscar;
                    $('#modalMateriales').modal('show');
                    app.getUsuarios();
                    app.usuarioBuscar = "";
                } else {
                    app.usuarioBuscar = "";
                    $('#modalMateriales').modal('show');
                }


            } else {
                alertify.warning('Por favor seleccione un municipio')
            }
        },

        getUsuarios: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                    mun: app.valorMun,
                    busqueda: app.buscarUsuario,
                },

                function (data) {
                    var data = JSON.parse(data);
                    app.tablaClientes = data;
                }
            )
            ;
        },

        seleccionarCliente: function (data) {
            var app = this;
            app.idUsuario = data.codigo;
            app.nombreUsuario = data.codigo + " -  " + data.nombre;
            app.buscarUsuario = "";
            app.verificar();
            $('#modalMateriales').modal('hide');
        },

        loadClientes() {

            var app = this;

            app.selectUsuarios = [];

            $.post('./request/getClientes.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {

                //console.log(data)

                var usuarios = JSON.parse(data);

                var uPropios = usuarios['propio'];
                var uComer = uPropios.concat(usuarios['comer'])

                app.selectUsuarios = uComer;

            });

        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')


            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "
            app.usuarioBuscar = "";

        },

        verificar: function (msg) {

            var app = this;

            if (
                app.valorMun == "" ||
                app.nombreUsuario == ""
            ) {

                alertify.error("Por favor selecciona un Usuario")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')

                console.log(app.valorMun)
                console.log(app.idUsuario)

                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    cliente: app.idUsuario
                }, function (data) {

                    console.log(data)

                    try {
                        var array = JSON.parse(data);
                        app.tablaHistorico = array;

                        var auxRecaudado = 0;
                        var auxFacturado = 0;
                        var auxFacturas = 0;
                        var auxRecaudos = 0;

                        for (i in app.tablaHistorico) {

                            auxRecaudado += parseFloat(app.tablaHistorico[i].recaudado);
                            auxFacturado += parseFloat(app.tablaHistorico[i].facturado);
                            auxFacturas += parseFloat(app.tablaHistorico[i].nFact);
                            auxRecaudos += parseFloat(app.tablaHistorico[i].nRec);

                        }


                        var saldo = auxFacturado - auxRecaudado;

                        console.log(saldo)

                        if (saldo < 0) {

                            app.totalRecuperada = (saldo * -1);

                        } else if (saldo > 0) {

                            app.totalGenerada = saldo;

                        } else {

                            app.totalGenerada = saldo;
                            app.totalRecuperada = saldo;

                        }

                        //app.sumaComer = "Cart Recuperada: $" + app.totalRecuperada.toLocaleString() + " - " + "Cart Generada: $" + app.totalGenerada.toLocaleString();

                        app.totalRecaudado = "$ " + auxRecaudado.toLocaleString();
                        app.totalFacturado = "$ " + auxFacturado.toLocaleString();
                        app.totalFacturas = "" + auxFacturas.toLocaleString();
                        app.totalRecaudos = "" + auxRecaudos.toLocaleString();
                        app.totalRecuperada = "$ " + app.totalRecuperada.toLocaleString();
                        app.totalGenerada = "$ " + app.totalGenerada.toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&cliente=" + app.idUsuario
                        );


                        /*
                        var totalPropios = 0;
                        var totalComer = 0;

                        for (i in app.tablaNoPropios) {
                            totalComer += parseFloat(app.tablaNoPropios[i]['vr_alumb'])
                        }

                        for (i in app.tablaPropios) {
                            totalPropios += parseFloat(app.tablaPropios[i]['vr_alumb'])
                        }

                        app.sumaPropio = "Total Liquidado: $" + totalPropios;
                        app.sumaComer = "Total Liquidado: $" + totalComer;

                        if (app.tabActive == 1) {
                            app.sumatoria = app.sumaComer
                        } else {
                            app.sumatoria = app.sumaPropio
                        }

                        app.textComercializador = "Comercializador " + "(" + app.tablaNoPropios.length + ")";
                        app.textPropio = "Especiales Propios " + "(" + app.tablaPropios.length + ")";

                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );

                        $("#btnExportarPropio a").attr("href",
                            "r" +
                            "equest/exportarPropio.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador +
                            "&cliente=" + app.valorUsuario
                        );


                        */
                    } catch (e) {
                        alertify.error('Error')
                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();


    },

});
