<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
ini_set('max_execution_time', 900);

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$cliente = $_GET['cliente'];

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=Historico_Usuario_" . $cliente . " .xls");
header("Expires: 0");

$stmt = "Select * from REPORTE_HISTORICO('1' , '" . $mun . "', '" . $cliente . "')";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='8'>Historico por Usuario - $cliente</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Facturado</th>" .
    "<th>N Facturas</th>" .
    "<th>Recaudado</th>" .
    "<th>N Recaudos</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

$facturado = 0;
$recaudado = 0;

$facturas = 0;
$recaudos = 0;

$generada = 0;
$recuperada = 0;


while ($fila = ibase_fetch_row($result)) {


    $facturado += intval($fila[2]);
    $recaudado += intval($fila[3]);
    $facturas += intval($fila[4]);
    $recaudos += intval($fila[5]);
    $generada += intval($fila[6]);
    $recuperada += intval($fila[7]);


    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . intval($fila[2]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "</tr>";

}

$carteraGen = 0;
$carteraRec = 0;

$saldo = $generada - $recuperada;


if ($saldo < 0) {

    $carteraRec = ($saldo * -1);

} else if ($saldo > 0) {

    $carteraGen = $saldo;

} else {

    $carteraGen = $saldo;
    $carteraRec = $saldo;

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $facturas . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $recaudos . "</td>" .
    "<td>" . $carteraGen . "</td>" .
    "<td>" . $carteraRec . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

