<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=Reporte.xls");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['ano'];
$mes = $_GET['mes'];
$comer = $_GET['comer'];
$cliente = $_GET['cliente'];


$whereAnio = "";
if ($anio != "") {
    $whereAnio = " AND l.l_ano = " . $anio;
}

$whereMes = "";
if ($mes != "") {
    $whereMes = " AND l.l_mes = " . $mes;
}

$whereComer = "";
if ($comer != "") {
    $whereComer = " AND l.l_idcomer = " . $comer;
}

$whereCliente = "";
if ($cliente != "") {
    $whereCliente = " AND l.l_cliente = " . $cliente;
}


$stmt = "       select  l.l_empresa,
                        l.l_ano,
                        l.l_mes,
                        l.l_cliente,
                        ep.ep_nom,
                        e.e_zona,
                        e.cod_tarifa,
                        t.t_nom,
                        l.l_tarifa,
                        l.l_consumo_vr,
                        l.l_consumo_kwh,
                        0,
                        l.l_vr_alumb,
                        0
                from liquidados l
                join especiales e on e.cod_cliente = l.l_cliente and e.e_propio = '1'
                inner join especiales_propio ep on ep.ep_cod = l.l_cliente
                inner join tarifa t on t.t_cod = e.cod_tarifa and t.t_empresa = l.l_empresa
                left join fact_comerc f on f.fc_munic = l.l_empresa
                         and f.fc_ano = l.l_ano
                         and f.fc_mes = l.l_mes
                         and f.cod_comer = l.l_idcomer
                         and f.fc_cliente = l.l_cliente
                where l.l_empresa =  '" . $mun . "' " . $whereAnio . $whereMes . $whereComer . $whereCliente . "  ";

$result = ibase_query($conexion, $stmt);


$tabla = "<table>" .
    "<tr class='cabecera'>" .
    "<th>MUNICIPIO</th>" .
    "<th>ANIO</th>" .
    "<th>MES</th>" .
    "<th>COD CLIENTE</th>" .
    "<th>NOM CLIENTE</th>" .
    "<th>ZONA</th>" .
    "<th>TARIFA</th>" .
    "<th>NOM TARIFA</th>" .
    "<th>TARIFA LIQ</th>" .
    "<th>CONSUMO VR LIQ</th>" .
    "<th>CONSUMO KWH LIQ</th>" .
    "<th>VR FACTURA</th>" .
    "<th>VR ALUMBRADO</th>" .
    "<th>DIFERENCIA</th>" .
    "</tr>";

while ($fila = ibase_fetch_row($result)) {


    $tabla .= "<tr class='fila'>" .
        "<td>" . utf8_encode($fila[0]) . "</td>" .
        "<td>" . utf8_encode($fila[1]) . "</td>" .
        "<td>" . utf8_encode($fila[2]) . "</td>" .
        "<td>" . utf8_encode($fila[3]) . "</td>" .
        "<td>" . utf8_encode($fila[4]) . "</td>" .
        "<td>" . utf8_encode($fila[5]) . "</td>" .
        "<td>" . utf8_encode($fila[6]) . "</td>" .
        "<td>" . utf8_encode($fila[7]) . "</td>" .
        "<td>" . utf8_encode($fila[8]) . "</td>" .
        "<td>" . utf8_encode($fila[9]) . "</td>" .
        "<td>" . utf8_encode($fila[10]) . "</td>" .
        "<td>" . utf8_encode($fila[11]) . "</td>" .
        "<td>" . utf8_encode($fila[12]) . "</td>" .
        "<td>" . utf8_encode($fila[13]) . "</td>" .
        "</tr>";

}

$tabla .= "</table>";

echo $tabla;

