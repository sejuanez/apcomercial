<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
ini_set('max_execution_time', 0);

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$comer = $_GET['comer'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$mes = $_GET['mes'];
$mesFin = $_GET['mesFin'];

$fecha = "";

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Expires: 0");

//si solo se selecciono el municipio
if ($anio == "" && $mes == "" && $anioFin == "" && $mesFin == "") {

    $fecha = "HISTORICO";

    $stmt = "Select * from INF_TARIFA( '1' , 
                                                      '" . $mun . "', 
                                                      '" . $comer . "', 
                                                      null,
                                                      null,                                                  
                                                      null,
                                                      null
                                                      )";

    //
} else if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

    $fecha = $anio . "_" . $mes;

    $stmt = "Select * from INF_TARIFA( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $comer . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $anio . "', 
                                                      '" . $mes . "'
                                                      )";

} else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

    $fecha = $anio . "_" . $mes . "_Hasta_" . $anioFin . "_" . $mesFin;

    $stmt = "Select * from INF_TARIFA( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $comer . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $anioFin . "', 
                                                      '" . $mesFin . "' 
                                                      )";

}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();

header("Content-Disposition: filename=Consolidado_Tarifas_" . $mun . "_" . $fecha . " .xls");

$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='8'>Consolidado por Tarifas - $mun</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Tarifa</th>" .
    "<th>Cantidad</th>" .
    "<th>Us Rec</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

$facturado = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;
$cant = 0;

while ($fila = ibase_fetch_row($result)) {


    $facturado += intval($fila[3]);
    $recaudado += intval($fila[4]);
    $generada += intval($fila[5]);
    $recuperada += intval($fila[6]);
    $cant += intval($fila[9]);


    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . ($fila[8]) . "</td>" .
        "<td>" . intval($fila[9]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='3'> <strong>TOTALES</strong> </td>" .
    "<td>" . $cant . "</td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

