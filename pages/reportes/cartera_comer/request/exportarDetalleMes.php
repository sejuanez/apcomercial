<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$codComer = $_GET['comer'];
$comer = $_GET['nomComer'];
$esp = $_GET['esp'];
$gen = $_GET['gen'];

header("Content-Disposition: filename=Consolidado_Detallado_por_Comercializador(" . $codComer . ")_" . $mun . "_" . $anio . ".xls");


if ($gen == 'false' && $esp == 'false') {

    $stmt = "Select * from INF_CARTERA_COMERCIALIZADOR_DET( '1' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "',                                                 
                                                      '" . $comer . "'
                                                      )";

} else {
    if ($gen == 'true') {

        $stmt = "Select * from INF_CARTERA_COMERCIALIZADOR_DET( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "',                                                 
                                                      '" . $comer . "'
                                                      )";

    } else {

        $stmt = "Select * from INF_CARTERA_COMERCIALIZADOR_DET( '3' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "',                                                 
                                                      '" . $comer . "'
                                                      )";

    }
}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='11'>Consolidado Detallado Por Comercializador($comer) $mun $anio</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Comercializador</th>" .
    "<th>Us Fact</th>" .
    "<th>Vr Facturado</th>" .
    "<th>Us Liq</th>" .
    "<th>Vr Liquidado</th>" .
    "<th>Us Rec</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

$usFact = 0;
$facturado = 0;
$usLiq = 0;
$liquidado = 0;
$usRec = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {

    $usFact += intval($fila[3]);
    $facturado += intval($fila[4]);
    $usLiq += intval($fila[5]);
    $liquidado += intval($fila[6]);
    $usRec += intval($fila[7]);
    $recaudado += intval($fila[8]);
    $generada += intval($fila[10]);
    $recuperada += intval($fila[11]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[9]) . "</td>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "<td>" . intval($fila[8]) . "</td>" .
        "<td>" . intval($fila[10]) . "</td>" .
        "<td>" . intval($fila[11]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='3'> <strong>TOTALES</strong> </td>" .
    "<td>" . $usFact . "</td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $usLiq . "</td>" .
    "<td>" . $liquidado . "</td>" .
    "<td>" . $usRec . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

