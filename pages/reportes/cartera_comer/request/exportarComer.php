<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$comer = $_GET['comer'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$esp = $_GET['esp'];
$gen = $_GET['gen'];

$comerReporte = "";
$fechaReporte = "";


if ($gen == 'false' && $esp == 'false') {

    if ($comer == "") {

        $comerReporte = '(Todos)_Todos_Usuarios';

        if ($anio == "" && $anioFin == "") {

            $fechaReporte = 'Historico';

            $stmt = "Select * from INF_CART_COMER_GYE( '1' , 
                                                      '" . $mun . "', 
                                                      null,                                                    
                                                      null,
                                                      null
                                                      )";

        } else if ($anio != "" && $anioFin == "") {

            $fechaReporte = $anio;

            $stmt = "Select * from INF_CART_COMER_GYE( '4' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      null
                                                      )";

        } else if ($anio != "" && $anioFin != "") {

            $fechaReporte = $anio . "_Hasta_" . $anioFin;

            $stmt = "Select * from INF_CART_COMER_GYE( '4' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      null
                                                      )";

        }

    } else {

        if ($anio == "" && $anioFin == "") {

            $fechaReporte = 'Historico';

            $stmt = "Select * from INF_CART_COMER_GYE( '2' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,
                                                      '" . $comer . "'
                                                      )";

            //
        } else if ($anio != "" && $anioFin == "") {
            $fechaReporte = $anio;
            $stmt = "Select * from INF_CART_COMER_GYE( '4' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $comer . "'
                                                      )";

        } else if ($anio != "" && $anioFin != "") {


            $fechaReporte = $anio . "_Hasta_" . $anioFin;

            $stmt = "Select * from INF_CART_COMER_GYE( '4' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $comer . "' 
                                                      )";

        }

    }

} else {

    if ($gen == 'true') {

        $comerReporte = '(Todos)_Uusarios_Generales';

        if ($comer == "") {

            if ($anio == "" && $anioFin == "") {

                $fechaReporte = 'Historico';

                $stmt = "Select * from INF_CART_COMER_GYE( '5' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,                                                  
                                                      null
                                                      )";

            } else if ($anio != "" && $anioFin == "") {

                $fechaReporte = $anio;

                $stmt = "Select * from INF_CART_COMER_GYE( '7' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      null
                                                      )";

            } else if ($anio != "" && $anioFin != "") {

                $fechaReporte = $anio . "_Hasta_" . $anioFin;

                $stmt = "Select * from INF_CART_COMER_GYE( '7' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      null
                                                      )";

            }

        } else {

            if ($anio == "" && $anioFin == "") {

                $fechaReporte = 'Historico';

                $stmt = "Select * from INF_CART_COMER_GYE( '6' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,                                                  
                                                      '" . $comer . "'
                                                      )";

                //
            } else if ($anio != "" && $anioFin == "") {


                $fechaReporte = $anio;

                $stmt = "Select * from INF_CART_COMER_GYE( '8' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $comer . "'
                                                      )";

            } else if ($anio != "" && $anioFin != "") {

                $fechaReporte = $anio . "_Hasta_" . $anioFin;

                $stmt = "Select * from INF_CART_COMER_GYE( '8' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "',                                                    
                                                      '" . $comer . "' 
                                                      )";

            }

        }

    } else {

        $comerReporte = '(Todos)_Usuarios_Especiales';

        if ($comer == "") {

            if ($anio == "" && $anioFin == "") {
                $fechaReporte = 'Historico';

                $stmt = "Select * from INF_CART_COMER_GYE( '9' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,                                                  
                                                      null
                                                      )";

            } else if ($anio != "" && $anioFin == "") {
                $fechaReporte = $anio;


                $stmt = "Select * from INF_CART_COMER_GYE( '11' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      null
                                                      )";

            } else if ($anio != "" && $anioFin != "") {

                $fechaReporte = $anio . "_Hasta_" . $anioFin;

                $stmt = "Select * from INF_CART_COMER_GYE( '11' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      null
                                                      )";

            }

        } else {

            if ($anio == "" && $anioFin == "") {

                $fechaReporte = 'Historico';

                $stmt = "Select * from INF_CART_COMER_GYE( '10' , 
                                                      '" . $mun . "', 
                                                      null,
                                                      null,                                                  
                                                      '" . $comer . "'
                                                      )";

                //
            } else if ($anio != "" && $anioFin == "") {

                $fechaReporte = $anio;

                $stmt = "Select * from INF_CART_COMER_GYE( '12' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $comer . "'
                                                      )";

            } else if ($anio != "" && $anioFin != "") {

                $fechaReporte = $anio . "_Hasta_" . $anioFin;

                $stmt = "Select * from INF_CART_COMER_GYE( '12' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "',                                                    
                                                      '" . $comer . "' 
                                                      )";

            }

        }

    }
}

header("Content-Disposition: filename=Consolidado_por_Comercializador" . $comerReporte . "_" . $mun . "_" . $fechaReporte . ".xls");


$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='10'>Consolidado Por Comercializador$comerReporte $mun</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Comercializador</th>" .
    "<th>Us Liq</th>" .
    "<th>Vr Liquidado</th>" .
    "<th>Us Fact</th>" .
    "<th>Vr Facturado</th>" .
    "<th>Us Rec</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

$usFact = 0;
$facturado = 0;
$usLiq = 0;
$liquidado = 0;
$usRec = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {

    $usFact += intval($fila[3]);
    $facturado += intval($fila[4]);
    $usLiq += intval($fila[5]);
    $liquidado += intval($fila[6]);
    $usRec += intval($fila[7]);
    $recaudado += intval($fila[8]);
    $generada += intval($fila[10]);
    $recuperada += intval($fila[11]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[9]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "<td>" . intval($fila[8]) . "</td>" .
        "<td>" . intval($fila[10]) . "</td>" .
        "<td>" . intval($fila[11]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $usFact . "</td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $usLiq . "</td>" .
    "<td>" . $liquidado . "</td>" .
    "<td>" . $usRec . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

