<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=Reporte.xls");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];


$stmt = "Select * from INF_CARTERA_MESES( '3' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Cod Cliente</th>" .
    "<th>Nom Cliente</th>" .
    "<th>Tarifa</th>" .
    "<th>Facturado</th>" .
    "<th>Vr Liquidado</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";

while ($fila = ibase_fetch_row($result)) {

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[11]) . "</td>" .
        "<td>" . ($fila[12]) . "</td>" .
        "<td>" . ($fila[13]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[8]) . "</td>" .
        "<td>" . intval($fila[9]) . "</td>" .
        "<td>" . intval($fila[10]) . "</td>" .
        "</tr>";

}

$tabla .= "</table>";

echo $tabla;

