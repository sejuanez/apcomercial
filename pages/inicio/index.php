<?php
session_start();

//incluimos el archivo que contiene la configuracion de la conexion a la bd
//include('gestion.php');


?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Stylish Portfolio - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->

    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">

</head>

<body id="page-top">

<?php
if (!$_SESSION["user"]) {
    ?>

    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container text-center my-auto">
            <h1 class="mb-1" style="background-color: #c3c3c375;">
                Página no disponible.
            </h1>
        </div>
        <div class="overlay"></div>
    </header>
    <?php
} else {
    ?>

    <!-- Header -->
    <header class="masthead d-flex">
        <div class="container text-center my-auto">
            <h1 class="mb-1" style="background-color: #c3c3c375;">
                Hola <?php echo $_SESSION["user"]; ?> !
            </h1>
            <h3 class="mb-5" style="background-color: #c3c3c375;">
                <em>Nos place tenerte nuevamente por aquí.</em>
            </h3>
        </div>
        <div class="overlay"></div>
    </header>
<?php } ?>
<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for this template -->

</body>

</html>
