<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");


$stmt = "Select * from GRAFICO_RECAUDO_REAL('1' , null)";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


$return_arr = array();


while ($fila = ibase_fetch_row($result)) {
    $row_array['anio'] = utf8_encode($fila[0]);
    $row_array['mun'] = utf8_encode($fila[1]);
    $row_array['cant_fac'] = utf8_encode($fila[2]);
    $row_array['fac'] = utf8_encode($fila[3]);
    $row_array['cant_rec'] = utf8_encode($fila[4]);
    $row_array['rec'] = utf8_encode($fila[5]);
    $row_array['gen'] = utf8_encode($fila[6]);
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);



