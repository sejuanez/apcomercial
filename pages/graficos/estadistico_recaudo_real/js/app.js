// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,


    },


    methods: {

        loadData: function () {

            let app = this;

            $.ajax({
                url: './request/getDatos.php',
                type: 'POST',
                async: true,
                data: {},

            }).done(function (response) {
                let datos = JSON.parse(response);

                let cantidad = datos.length;
                console.log(datos[0])

                let container = $("#graficos");

                for (let i = 1; i <= cantidad; i++) {

                    let data = [parseInt(datos[i - 1].fac, 10), parseInt(datos[i - 1].rec, 10)];

                    //si la cantidad de municipios es inpar (se agrega un espacio para centrar el grafico)
                    if (!(i % 2 == 0)) {


                        container.append(
                            "<div class='col-sm-1'></div>");

                        container.append(
                            "<div style='margin-bottom: 35px;' class='col-sm-4'>" +
                            "                <h5 class='text-center'> " + datos[i - 1].mun + " </h5>" +
                            "                <h6 class='text-center'>Us Facturados: " + datos[i - 1].cant_fac + "  </h6>" +
                            "                <h6 class='text-center'>Us Recaudados: " + datos[i - 1].cant_rec + "  </h6>" +
                            "                <canvas id='myChart" + i + "' width='400' height='400'></canvas>" +
                            "</div>");

                        container.append(
                            "<div class='col-sm-2'></div>");

                        app.graficar('myChart' + i, data)

                    } else {


                        container.append(
                            "<div style='margin-bottom: 35px;' class='col-sm-4'>" +
                            "                <h5 class='text-center'> " + datos[i - 1].mun + " </h5>" +
                            "                <h6 class='text-center'>Us Facturados: " + datos[i - 1].cant_fac + "  </h6>" +
                            "                <h6 class='text-center'>Us Recaudados: " + datos[i - 1].cant_rec + "  </h6>" +
                            "                <canvas id='myChart" + i + "' width='400' height='400'></canvas>" +
                            "</div>");

                        container.append(
                            "<div class='col-sm-1'></div>");

                        app.graficar('myChart' + i, data)

                    }
                }

            }).fail(function (error) {

                console.log(error)

            });


        },

        graficar: function (id, data) {

            let ctx = document.getElementById(id).getContext('2d');
            let myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: ["Facturado", "Recaudado"],
                    datasets: [{
                        label: '# of Votes',
                        data: data,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(153, 102, 255, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(153, 102, 255, 1)'
                        ],
                        //borderWidth: 1
                    }]
                },

                options: {}

            });

        },

        reset: function () {
            let app = this;
            $("#graficos").html('');
            app.loadData();
        }

    },


    watch: {},

    mounted() {

        this.loadData();

    },

});
