<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 57vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }


        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #00034A; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Factura Individual

            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verifica();">
					<i class="fa fa-search" aria-hidden="true"></i> Verificar
		    	</span>

            <span id="btnBuscar" v-show="btn_liquidar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="liquidarTodo(1);">
					<i class="fa fa-list" aria-hidden="true"></i> Facturar
		    	</span>

            <span id="btnBuscar" v-show="btn_Re_liquidar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="liquidarTodo(2);">
					<i class="fa fa-list" aria-hidden="true"></i> Re-Facturar
		    	</span>

            <span id="btnBuscar" v-show="btn_Re_liquidar || btn_liquidar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="imprimir();">
					<i class="fa fa-print" aria-hidden="true"></i> Imprimir
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">


        <div v-if="ajax" class="loading">Loading&#8230;</div>


        <div class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectAño" id="selectAño" name="selectAño"
                            v-model="valorAño">
                        <option value="">Año...</option>
                        <option v-for="selectActividad in selectAño"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMes" id="selectMes" name="selectMes"
                            v-model="valorMes">
                        <option value="">Mes...</option>
                        <option v-for="selectActividad in selectMes"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="codigo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="codigo">
                    <label for="codigo">Codigo</label>
                </div>
            </div>

            <!--
            <div class="col-12 col-sm-4" style="padding-top: 5px">

                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm"><input type="text"
                                                                               placeholder="Filtrar por codigo nombre"
                                                                               v-model="busqueda"
                                                                               class="form-control"> <span
                                class="input-group-btn"><button type="button"
                                                                v-on:click="filtrar()"
                                                                class="btn btn-secondary">Buscar</button></span>
                    </div>
                </div>

            </div>
-->

        </div>

        <br>

        <!--
                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th width="150">Municipio</th>
                                    <th width="100">Año</th>
                                    <th width="100">Mes</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaConsumos.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaConsumos">
                                    <td v-text="dato.mun" width="150"></td>
                                    <td v-text="dato.anio" width="100"></td>
                                    <td v-text="dato.mes" width="100"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
        -->

    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
