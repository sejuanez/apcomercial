<?php
require('fpdf.php');
include("../../../../init/gestion.php");


$mun = utf8_decode($_POST['mun']);
$anio = utf8_decode($_POST['anio']);
$mes = utf8_decode($_POST['mes']);
$cliente = utf8_decode($_POST['cliente']);
$opcion = 2;

/*
$mun = 'CO23189';
$anio = '2016';
$mes = '1';
$cliente = '4551204';
$opcion = 2;
*/

$data = getDatos($mun, $anio, $mes, $cliente);


class PDF extends FPDF
{
    function Header()
    {

        global $arrayDatos;

        global $title;
        global $consecutivo;
        global $periodo;
        global $anio, $codigoContribuyente;

        global $razonSocial;
        global $propietario;
        global $nit;
        global $direccion;
        global $telefono;
        global $actividad;
        global $claseActividad;
        global $norma;
        global $rango;
        global $impuesto;

        global $razonSocial;
        global $nota;

        global $header;
        global $nota1;
        global $nota2;

        global $logoIzq;
        global $logoDer;

        global $texto1fact;

        global $total;
        global $fechaPago;

        $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
        $this->SetFont('Lato-Regular', '', 10);

        // Ancho de pagina 190 en X
        // $this->setDrawColor(80,80,80);
        // $this->setFillColor(0,80,170);
        // $this->setTextColor(255,255,255);
        $this->setTextColor(80, 80, 80);
        if ($logoIzq != null || $logoIzq != "") {
            $this->Image('../' . $logoIzq, 10, 5, 30, 30, 'jpg');
        }
        if ($logoDer != null || $logoDer != "") {
            $this->Image('../' . $logoDer, 160, 5, 30, 30, 'jpg');

        }

        $headerT = explode('/', $header);
        $this->SetFont('Lato-Regular', '', 10);

        foreach ($headerT as $item) {

            $this->Cell(190, 5, utf8_decode($item), 0, 0, 'C', false);
            $this->Ln(5);

        }

        $this->SetFont('Lato-Regular', '', 11);
        $this->Ln(3);
        $this->Cell(190, 5, $title, 0, 0, 'C', false);


        // Line break
        $this->Ln(5);

        $this->AddFont('Lato-Regular', '', 'Lato-Regular.php');
        $this->SetFont('Lato-Regular', '', 10);

        $this->setTextColor(0, 0, 0);
        $this->Cell(31, 7, 'CONSECUTIVO:', 0, 0, 'L');
        $this->setTextColor(80, 80, 80);
        $this->Cell(26, 7, $consecutivo, 0, 0, 'L');

        $this->setTextColor(0, 0, 0);
        $this->Cell(26, 7, 'PERIODO:', 0, 0, 'R');
        $this->setTextColor(80, 80, 80);
        $this->Cell(26, 7, $periodo, 0, 0, 'L');

        $this->setTextColor(0, 0, 0);
        $this->Cell(15, 7, utf8_decode('AÑO'), 0, 0, 'R');
        $this->setTextColor(80, 80, 80);
        $this->Cell(20, 7, $anio, 0, 0, 'L');

        $this->setTextColor(0, 0, 0);
        $this->Cell(26, 7, 'CONTRIBUYENTE', 0, 0, 'R');
        $this->setTextColor(80, 80, 80);
        $this->Cell(22, 7, $codigoContribuyente, 0, 0, 'L');
        $this->Ln(7);


    }

    function Footer()
    {
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        $this->AddFont('LatoL', '', 'Lato-Light.php');
        $this->SetFont('LatoL', '', 8);
        // Print centered page number
        $this->setFillColor(0, 80, 170);
        $this->setTextColor(80, 80, 80);
        //$this->Cell(95, 10, 'IMPRESO POR CONELEC: ' . date('d/m/Y  -H:i:s', time() - 21600), 'T', 0, 'L');
        $this->Cell(95, 10, 'IMPRESO POR CONELEC: ', 'T', 0, 'L');
        $this->Cell(95, 10, utf8_decode('PAGINA ' . $this->PageNo()) . '/{nb}', 'T', 0, 'R');
    }
}


$arrayDatos = $data;


$header = $data[0]['TEXTOENCAB'];
$logoIzq = $data[0]['LOGOIZQ'];
$logoDer = $data[0]['LOGODER'];

$title = 'LIQUIDACION INFORMATIVA IMPUESTO DE ALUMBRADO PUBLICO';
$consecutivo = 'AP ' . $data[0]['PREFIJO'] . " - " . $data[0]['NROLLENO'];
$periodo = $data[0]['NOMMES'];
$anio = $data[0]['CODANO'];
$codigoContribuyente = $data[0]['CODCLIENTE'];

$razonSocial = $data[0]['NOMCLIENTE'];

$propietario = $data[0]['NOMREPRESLEG'];
$nit = $data[0]['NIT'];

$direccion = $data[0]['DIRECCLIENTE'];
$telefono = $data[0]['TELEFCLIENTE'];

$actividad = $data[0]['NOMACTIV'];
$claseActividad = $data[0]['NOMCLASEACTIV'];

$norma = 'ACUERDO NRO: ' . $data[0]['NROACUERDO'];
$rango = $data[0]['CODTARIFA'];

$impuesto = $data[0]['IMPUESTOCARGO'];

$fechaPago = $data[0]['FECHALIMPAGO'];

$nota1 = $data[0]['NOTA1'];
$nota2 = $data[0]['NOTA2'];

$texto1fact = $data[0]['TEXTO1FACT'];


$pdf = new PDF();
$pdf->SetTitle($title);
$pdf->AliasNbPages();
$pdf->AddPage();
// Header table
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 10);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(150, 150, 150);
$pdf->setTextColor(255, 255, 255);
$pdf->Cell(190, 7, 'IDENTIFICACION', 1, 0, 'C', true);
$pdf->Ln();

// ------------------------------------------------------------------------------- //

$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(120, 5, '  RAZON SOCIAL', 1, 0, 'l', true);
$pdf->Cell(70, 5, '', 1, 0, 'l', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);

$pdf->MultiCell(120, 5, '   ' . $razonSocial, '', 'L', true);
$pdf->Ln(0);

// ------------------------------------------------------------------------------- //

$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(120, 5, '  PROPIETARIO O REPRESENTANTE LEGAL', 1, 0, 'l', true);
$pdf->Cell(70, 5, '  NIT O CC', 1, 0, 'l', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);


$pdf->Cell(120, 5, '   ' . $propietario, '', 0, 'L', true);
$pdf->Cell(70, 5, '   ' . $nit, '', 0, 'L', true);
$pdf->Ln(5);

// ------------------------------------------------------------------------------- //
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(120, 5, '  DIRECCION DEL ESTABLECIMIENTO', 1, 0, 'l', true);
$pdf->Cell(70, 5, '  TELEFONO', 1, 0, 'l', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);


$pdf->Cell(120, 5, '   ' . $direccion, '', 0, 'l', true);
$pdf->Cell(70, 5, '   ' . $telefono, '', 0, 'l', true);
$pdf->Ln(5);

// ------------------------------------------------------------------------------- //// ------------------------------------------------------------------------------- //
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(190, 5, '   DESCRIPCION DE LA ACTIVIDAD', 1, 0, 'l', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);


$pdf->multicell(190, 5, '   ' . $actividad, 0, 'l', true);

// ------------------------------------------------------------------------------- //
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(70, 5, '  CLASE DE ACTIVIDAD', 1, 0, 'l', true);
$pdf->Cell(120, 5, '  NORMA QUE  REGULA EL ATRIBUTO DE ALUMBRADO PUBLICO', 1, 0, 'C', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);


$pdf->Cell(70, 5, '   ' . $claseActividad, '', 0, 'L', true);
$pdf->Cell(120, 5, '   ' . $norma, '', 0, 'C', true);
$pdf->Ln(5);

// ------------------------------------------------------------------------------- //
$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(70, 5, '  RANGO CONTRIBUYENTE', 1, 0, 'l', true);
$pdf->Cell(120, 5, '   IMPUESTO A CARGO', 1, 0, 'C', true);
$pdf->Ln();

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 8);


$pdf->Cell(70, 5, '   ' . $rango, '', 0, 'L', true);
$pdf->Cell(120, 5, '   $' . number_format(floatval($impuesto)) . " " . $data[0]['NOMIMPUCARGO'], '', 0, 'C', true);
$pdf->Ln(5);


// ------------------------------------------------------------------------------- //
// ------------------------------------------------------------------------------- //

$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 10);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(150, 150, 150);
$pdf->setTextColor(255, 255, 255);

$pdf->Cell(190, 7, 'LIQUIDACION DEL IMPUESTO', 1, 0, 'C', true);
$pdf->Ln();

// ------------------------------------------------------------------------------- //
// ------------------------------------------------------------------------------- //

for ($i = 0; $i < count($arrayDatos); $i++) {

    $pdf->SetFont('ARIAL', '', 8);
    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(210, 210, 210);
    $pdf->setTextColor(0, 0, 0);

    $pdf->Cell(120, 5, utf8_decode($arrayDatos[$i]['NOMCONCEPTO']), '1', 0, 'L', true);

    $pdf->setDrawColor(200, 200, 200);
    $pdf->setFillColor(255, 255, 255);
    $pdf->setTextColor(90, 80, 80);
    $pdf->AddFont('Lato', '', 'Lato-Regular.php');
    $pdf->SetFont('Lato', '', 8);

    $total += floatval($arrayDatos[$i]['VRCONCEPTO']);

    $pdf->Cell(70, 5, '$ ' . number_format(floatval($arrayDatos[$i]['VRCONCEPTO'])) . "   ", '1', 0, 'R', true);

    $pdf->Ln();
}
$pdf->Ln();


// ------------------------------------------------------------------------------- //

$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(120, 5, 'NUEVO SALDO A PAGAR', 1, 0, 'C', true);

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
$pdf->AddFont('Lato', '', 'Lato-Regular.php');
$pdf->SetFont('Lato', '', 9);


$pdf->Cell(70, 5, '$ ' . number_format($total) . "   ", 1, 0, 'R', true);
$pdf->Ln();


$pdf->AddFont('LatoL', '', 'Lato-Light.php');
$pdf->SetFont('ARIAL', '', 8);
$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(210, 210, 210);
$pdf->setTextColor(0, 0, 0);

$pdf->Cell(120, 5, 'FECHA LIMITE DE PAGO', 1, 0, 'C', true);

$pdf->setDrawColor(200, 200, 200);
$pdf->setFillColor(255, 255, 255);
$pdf->setTextColor(80, 80, 80);
//$pdf->AddFont('Lato', '', 'Lato-Regular.php');
//$pdf->SetFont('Lato', '', 8);

$pdf->Cell(70, 5, $fechaPago, 1, 0, 'C', true);


// ------------------------------------------------------------------------------- //

if ($i >= 11) {
    $pdf->AddPage();
}

$pdf->Ln(10);
$pdf->MultiCell(190, 5, '  NOTA: ' . $nota1, 'LRT', 'L', false);

$pdf->Ln(0);
$pdf->Cell(190, 30, '', 'RL', 0, 'C', true);
$pdf->Ln(15);


$pdf->setTextColor(80, 80, 80);
$pdf->Cell(10, 5, ' ');
if ($texto1fact == null || $texto1fact == "") {

    $pdf->Cell(80, 5, $texto1fact, 0, 0, 'C', true);
} else {
    $pdf->Cell(80, 5, $texto1fact, 'T', 0, 'C', true);
}
$pdf->Cell(10, 5, ' ');
$pdf->Cell(80, 5, 'FIRMA Y SELLO DE CONTRIBUYENTE', 'TR', 0, 'C', true);
$pdf->Cell(10, 5, '', 'R', 0, 'C', true);
$pdf->Ln(10);
$pdf->Cell(190, 8, $nota2, 'LRB', 0, 'L', true);
$pdf->Ln(8);
$pdf->Cell(63, 5, 'ORIGINAL: CONTRIBUYENTE ', '', 0, 'L', true);
$pdf->Cell(63, 5, 'COPIA 1: CONSECUTIVO', '', 0, 'C', true);
$pdf->Cell(65, 5, 'COPIA 2: MUNICIPIO', '', 0, 'R', true);

// ------------------------------------------------------------------------------- //


$pdf->Ln(10);
$pdf->Output('I', 'FACTURA_' . $codigoContribuyente . "_" . $anio . "_" . $periodo . '.pdf');


function getDatos($mun, $anio, $mes, $cliente): array
{


    $stmt = "SELECT * FROM FACT_IMPRIM( 2, 
                                    '" . $mun . "',
                                    '" . $anio . "',
                                    '" . $mes . "',
                                    '" . $cliente . "'
    )";

    $response = array();
    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);


    while ($fila = ibase_fetch_row($result)) {
        $row['FECHASERVER'] = utf8_encode($fila[0]);
        $row['IDFACT'] = utf8_encode($fila[1]);
        $row['CODMUNIC'] = utf8_encode($fila[2]);
        $row['PREFIJO'] = utf8_encode($fila[3]);
        $row['LOGOIZQ'] = ($fila[4]);
        $row['LOGODER'] = ($fila[5]);
        $row['NRO'] = utf8_encode($fila[6]);
        $row['NROLLENO'] = utf8_encode($fila[7]);
        $row['FECHA'] = utf8_encode($fila[8]);
        $row['CODANO'] = utf8_encode($fila[9]);
        $row['NOMANO'] = utf8_encode($fila[10]);
        $row['CODMES'] = utf8_encode($fila[11]);
        $row['NOMMES'] = utf8_encode($fila[12]);
        $row['CODCLIENTE'] = utf8_encode($fila[13]);
        $row['NOMCLIENTE'] = utf8_encode($fila[14]);
        $row['DIRECCLIENTE'] = utf8_encode($fila[15]);
        $row['TELEFCLIENTE'] = utf8_encode($fila[16]);
        $row['TEXTO1FACT'] = utf8_encode($fila[17]);
        $row['NIT'] = utf8_encode($fila[18]);
        $row['NOMREPRESLEG'] = utf8_encode($fila[19]);
        $row['NOMDPTO'] = utf8_encode($fila[20]);
        $row['NOMMUNIC'] = utf8_encode($fila[21]);
        $row['NITMUNIC'] = utf8_encode($fila[22]);
        $row['NOTA1'] = utf8_encode($fila[23]);
        $row['NOTA2'] = ($fila[24]);
        $row['NOTA3'] = utf8_encode($fila[25]);
        $row['TEXTOENCAB'] = utf8_encode($fila[26]);
        $row['NOMACTIV'] = utf8_encode($fila[27]);
        $row['NOMCLASEACTIV'] = utf8_encode($fila[28]);
        $row['CODTARIFA'] = utf8_encode($fila[29]);
        $row['NROACUERDO'] = utf8_encode($fila[30]);
        $row['IMPUESTOCARGO'] = utf8_encode($fila[31]);
        $row['NOMIMPUCARGO'] = utf8_encode($fila[32]);
        $row['TOTALPOSIT'] = utf8_encode($fila[33]);
        $row['TOTALNEGAT'] = utf8_encode($fila[34]);
        $row['TOTAL'] = utf8_encode($fila[35]);
        $row['FECHALIMPAGO'] = utf8_encode($fila[36]);
        $row['VRREDONDEO'] = utf8_encode($fila[37]);
        $row['CODCONCEPTO'] = utf8_encode($fila[38]);
        $row['NOMCONCEPTO'] = utf8_encode($fila[39]);
        $row['VRCONCEPTO'] = utf8_encode($fila[40]);
        array_push($response, $row);
    }

    return $response;

}



