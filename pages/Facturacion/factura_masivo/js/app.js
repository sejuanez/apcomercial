// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            ajax: false,

            btn_cargando: false,
            btn_borrando: false,
            btn_cargar: false,
            btn_verificar: true,
            btn_verificando: false,
            btn_cancelar: false,
            btn_liquidar: false,
            btn_Re_liquidar: false,

            busqueda: "",

            selectMunicipio: [],
            valorMun: "",

            selectAño: [],
            valorAño: "",

            selectMes: [],
            valorMes: "",

            selectComercializador: [],
            valorComercializador: "",

            tablaConsumos: [],
            tablaExcluidos: [],
            arrayEexcel: [],
            camposTabla: [],
            arrayConsumos: [],

            liquidando: false,
            datos_liquidados: false,

            liquidados: "",

        },


        methods: {

            loadMunicipios: function () {

                var app = this;

                $.post('./request/getMunicipios.php', function (data) {
                    app.selectMunicipio = JSON.parse(data);
                });

            },

            onChangeMun: function (cod) {
                var app = this;
                app.valorMun = cod;
            },

            loadAño: function () {

                var app = this;

                $.post('./request/getAnio.php', function (data) {
                    app.selectAño = JSON.parse(data);
                });

            },

            onChangeAño: function (cod) {
                var app = this;
                app.valorAño = cod;
            },

            loadMes: function () {

                var app = this;

                $.post('./request/getMes.php', function (data) {
                    app.selectMes = JSON.parse(data);
                });

            },

            onChangeMes: function (cod) {
                var app = this;
                app.valorMes = cod;
            },

            cancelar: function () {

                var app = this;

                app.btn_cargando = false;
                app.btn_borrando = false;
                app.btn_cargar = false;
                app.btn_verificar = true;
                app.btn_verificando = false;
                app.btn_cancelar = false;
                app.btn_liquidar = false;
                app.btn_Re_liquidar = false;

                app.enableSelect2('.selectMunicipio')
                app.enableSelect2('.selectAño')
                app.enableSelect2('.selectMes')
                app.enableSelect2('.selectComercializador')

                app.tablaConsumos = [];
                app.tablaExcluidos = [];

            },

            verifica: function () {

                var app = this;


                if (
                    app.valorMun == "" ||
                    app.valorAño == "" ||
                    app.valorMes == ""
                ) {

                    alertify.error("Por favor selecciona todas las opciones")

                } else {

                    app.btn_verificar = false;
                    app.btn_verificando = true;


                    app.disableSelect2('.selectMunicipio')
                    app.disableSelect2('.selectAño')
                    app.disableSelect2('.selectMes')


                    $.post('./request/verificaLiquidados.php', {
                        mun: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes,
                    }, function (data) {

                        console.log(data)

                        var data = JSON.parse(data);

                        if (data[0].result == 'FACTURADO') {

                            app.btn_verificando = false;
                            app.btn_Re_liquidar = true;
                            app.btn_liquidar = false;
                            app.btn_cancelar = true;

                        } else if (data[0].result == 'LIQUIDADO') {

                            app.btn_verificando = false;
                            app.btn_Re_liquidar = false;
                            app.btn_liquidar = true;
                            app.btn_cancelar = true;

                        } else {

                            alertify.alert('Error', 'No existen registros para realizar una facturacion con esta busqueda')

                            app.btn_verificando = false;
                            app.btn_Re_liquidar = false;
                            app.btn_liquidar = false;
                            app.btn_cancelar = true;

                        }


                    });

                }

            },


            liquidarTodo: function (opcion) {
                var app = this;

                $.ajax({
                    url: './request/liquidar_todo.php',
                    type: 'POST',
                    data: {
                        mun: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes,
                        opcion: opcion
                    },

                }).done(function (response) {

                    console.log(response)

                    var liquidados = JSON.parse(response);


                    var msg;
                    opcion == 1 ? msg = "Factuacion" : msg = "Re-Facturacion";

                    if (liquidados[0].result !== null) {

                        alertify.alert(msg, 'Realizado con exito')

                    }

                    app.verifica();


                }).fail(function (error) {

                    console.log(error)

                });


            },


            disableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', true);
                $(class_select).select2();

            },

            enableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', false);
                $(class_select).select2();

            },


        },


        watch: {},

        mounted() {
            this.loadMunicipios();
            this.loadAño();
            this.loadMes();
        },

    })
;
