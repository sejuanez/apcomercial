<?php
define('MAX_SEGMENT_SIZE', 65535);
ini_set('max_execution_time', 0);
ini_set('memory_limit', '1024M');
session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");


$array = $_POST['array'];
$campos = $_POST['campos'];

$mun = $_POST['mun'];
$anio = $_POST['anio'];
$mes = $_POST['mes'];
$comer = $_POST['comer'];

$camposInsert = "id, COD_MUNIC, COD_ANO, COD_MES, COD_COMER, ";

$valoresinsert = "";

for ($i = 0; $i < count($campos); $i++) {

    if ($i == (count($campos) - 1)) {
        $camposInsert = $camposInsert . $campos[$i]['campo'] . "";
    } else {
        $camposInsert = $camposInsert . $campos[$i]['campo'] . ", ";
    }

}


//var_dump($campos);

for ($i = 0; $i < count($array); $i++) {

    $sql = 'SELECT GEN_ID( GEN_FACT_COMERC_AUX , 1 ) FROM RDB$DATABASE;';

    $result = ibase_query($conexion, $sql);

    while ($fila = ibase_fetch_row($result)) {
        $id = utf8_encode($fila[0]);
    }

    ibase_free_result($result);

    for ($j = 0; $j < count($array[$i]); $j++) {

        //$tipo = $tipo . $campos[$j]['campo'] . " -> " . $valoresinsert . $array[$i][$j] . " ";
        //$tipo = $campos[$j]['campo'];

        if ($campos[$j]['tipo'] == 1) {

            if ($j == (count($array[$i]) - 1)) {

                if ($array[$i][$j] == "") {
                    $valoresinsert = $valoresinsert . "'" . '' . "'";
                } else {
                    $valoresinsert = $valoresinsert . "'" . trim(($array[$i][$j])) . "'" . "";
                }

            } else {
                //$valoresinsert = $valoresinsert . $array[$i][$j] . ", ";

                if ($array[$i][$j] == "") {
                    $valoresinsert = $valoresinsert . "'" . '' . "'" . ", ";
                } else {
                    $valoresinsert = $valoresinsert . "'" . trim(($array[$i][$j])) . "'" . ", ";
                }

            }

        } else {

            if ($j == (count($array[$i]) - 1)) {

                if ($array[$i][$j] == "" || !(is_numeric($array[$i][$j]))) {
                    $valoresinsert = $valoresinsert . "'" . '0' . "'";
                } else {
                    $valoresinsert = $valoresinsert . "'" . trim(($array[$i][$j])) . "'" . "";
                }

            } else {
                //$valoresinsert = $valoresinsert . $array[$i][$j] . ", ";

                if ($array[$i][$j] == "" || !(is_numeric($array[$i][$j]))) {
                    $valoresinsert = $valoresinsert . "'" . '0 ' . "'" . ', ';
                } else {
                    $valoresinsert = $valoresinsert . "'" . trim(($array[$i][$j])) . "'" . ", ";
                }

            }

        }


    }

    $sql = "INSERT INTO FACT_COMERC_AUX (" . $camposInsert . ") VALUES ("
        . "'" . $id . "'" . ", "
        . "'" . $mun . "'" . ", "
        . "'" . $anio . "'" . ", "
        . "'" . $mes . "'" . ", "
        . "'" . $comer . "'" . ", "
        . $valoresinsert . ") ";

    $valoresinsert = "";


    $result = ibase_query($conexion, $sql);


}

$stmt = "SELECT * FROM CONSULTA_TARIFAS_FACT('" . $mun . "')";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$arrayErrores = array();

while ($fila = ibase_fetch_row($result)) {
    $row_array['valor'] = "Nom Servicio: " . utf8_encode($fila[0]) . "  -  Nom Estrato: " . utf8_encode($fila[1]);
    $row_array['error'] = "No se encuentra registrada en la homologacion de consumo";
    array_push($arrayErrores, $row_array);
}

if (count($arrayErrores) != 0) {

    $array = array("errores" => $arrayErrores);
    ibase_free_result($result);
    echo json_encode($array);

} else {

    $stmt = "SELECT * FROM FACTCOMERC_PASAR_IMPORT(
                                       '" . $mun . "',
                                       '" . $anio . "',
                                       '" . $mes . "',
                                       '" . $comer . "',
                                       null
                                   )";
    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);

    $return_arr = array();

    while ($fila = ibase_fetch_row($result)) {
        $row_array['codigo'] = utf8_encode($fila[0]);
        array_push($return_arr, $row_array);
    }

    $array = array("ok" => $return_arr);
    ibase_free_result($result);
    echo json_encode($array);

}






