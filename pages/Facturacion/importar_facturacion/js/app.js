// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);
    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,


        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,
        infoArchivo: false,

        selectMunicipio: [],
        valorMun: "",

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectComercializador: [],
        valorComercializador: "",

        tablaConsumos: [],
        arrayEexcel: [],
        camposTabla: [],
        arrayConsumos: [],
        arrayErrores: [],
        arrayTarifas: [],

        textErrores: "Errores",
        textConsumo: "Facturados",

        textVerificadosArchivo: "Verificados:",
        textErroresArchivo: "Errores:",
        textTotalArchivo: "Total:",


    },


    methods: {

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        loadTarifas: function () {

            var app = this;

            $.post('./request/getTarifas.php', {
                mun: app.valorMun
            }, function (data) {
                //console.log(data)
                app.arrayTarifas = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
        },

        loadComercializador: function () {

            var app = this;

            $.post('./request/getComercializador.php', function (data) {
                console.log(data)
                app.selectComercializador = JSON.parse(data);
            });

        },

        loadCamposTabla: function () {

            var app = this;

            $.post('./request/getCamposConsumo.php', {
                mun: app.valorMun,
                comer: app.valorComercializador,
            }, function (data) {
                //console.log(data)
                app.camposTabla = JSON.parse(data);

                if (app.camposTabla.length <= 0) {
                    alertify.error('Este municipio no cuenta con una configuracion de campos para importacion')
                } else {

                    app.verificar();
                }

            });

        },

        onChangeComercializador: function (cod) {
            var app = this;
            app.valorComercializador = cod;
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectComercializador')

            app.tablaConsumos = [];
            app.arrayErrores = [];
            app.arrayTarifas = [];
            app.arrayConsumos = [];

            app.textErrores = "Errores";
            app.textConsumo = "Facturados";
            app.infoArchivo = false;
        },

        verificar: function (msg) {

            var app = this;

            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == ""
            ) {

                alertify.error("Por favor selecciona todas las opciones")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                }, function (data) {

                    console.log(data)

                    var datos = jQuery.parseJSON(data);


                    if (datos['consumo'].length <= 0) {
                        alertify.success("No se ha importado un consumo para esta busqueda todavia")

                        app.btn_cargar = true;
                        app.loadTarifas();

                    } else {


                        if (msg != 1) {
                            alertify.warning("Existen registros de consumo para esta busqueda")
                        }

                        app.btn_borra = true;
                        app.tablaConsumos = datos['consumo'];
                        app.textConsumo = "Facturados: " + "(" + datos['cantidad'][0]['cantidad'] + ")";
                        app.infoArchivo = false;
                        app.btn_cargar = false;

                    }

                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar los facturados?",
                function () {
                    $.post('./request/deleteConsumo.php', {
                        mun: dato.mun,
                        anio: dato.anio,
                        mes: dato.mes,
                        comer: dato.comer,
                    }, function (data) {
                        //console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.textConsumo = "Facturados";
                            app.textErrores = "Errores";
                            app.tablaConsumos = [];
                            app.verificar()
                        } else {
                            alertify.error("Error al eliminar");
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        cargar: function () {

            $("#inputFileContainer").html("" +
                "<input type='file' id='archivo' name='archivo' class='' accept='.xlsx, .xls, .csv'" +
                "autocomplete='nope'>");

            $("#archivo").trigger("click");

            $('#archivo')[0].files[0] = "";

            $("#archivo").change(function () {

                console.log('upload file')

                app.subeExcel();

            });

        },

        leerExcel: function (excel) {

            var app = this;
            /* set up XMLHttpRequest */
            var url = excel;
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, true);
            oReq.responseType = "arraybuffer";

            oReq.onload = function (e) {
                var arraybuffer = oReq.response;

                /* convert data to binary string */
                var data = new Uint8Array(arraybuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");

                /* Call XLSX */
                var workbook = XLSX.read(bstr, {type: "binary"});

                /* DO SOMETHING WITH workbook HERE */
                var first_sheet_name = workbook.SheetNames[0];
                /* Get worksheet */
                var worksheet = workbook.Sheets[first_sheet_name];


                app.arrayEexcel = (XLSX.utils.sheet_to_json(worksheet, {header: 1}, {defval: ""}));

                app.buscaUsuariosDuplicados()


            }

            oReq.send();

        },

        subeExcel: function () {


            var app = this;
            var formData = new FormData($('.formGuardar')[0]);

            app.arrayErrores = [];
            app.arrayConsumos = [];
            app.arrayEexcel = [];

            //hacemos la petición ajax
            $.ajax({
                url: './uploadFile.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,

                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    app.leerExcel("./temp/" + data)

                },
                //si ha ocurrido un error
                error: function (FormData) {
                    ////console.log(data)
                }
            });

        },

        armarArray: function () {

            var app = this;

            app.arrayConsumos = [];

            for (var i in app.arrayEexcel) {
                var array = [];
                for (var j in app.camposTabla) {

                    if (i != 0) {

                        if ((app.arrayEexcel[i][app.camposTabla[j].codigo]) != null) {

                            array[j] = app.arrayEexcel[i][app.camposTabla[j].codigo];

                        } else {

                            array[j] = "";

                        }

                    }

                }
                app.arrayConsumos.push(array)
            }
            app.arrayConsumos.shift()
            //console.log(app.arrayConsumos)

            app.enviarArray()


        },

        buscaUsuariosDuplicados: function () {

            var app = this;


            //app.infoArchivo = true;
            /*

                        var indexCodCliente = 0;
                        var indexServicio = 0;
                        var indexEstrato = 0;

                        console.log(app.camposTabla)

                        for (i in app.camposTabla) {
                            if (app.camposTabla[i].campo.toUpperCase() == "COD_CLIENTE") {
                                indexCodCliente = app.camposTabla[i].codigo;
                            }
                            if (app.camposTabla[i].campo.toUpperCase() == "NOM_SERVICIO") {
                                indexServicio = app.camposTabla[i].codigo;
                            }
                            if (app.camposTabla[i].campo.toUpperCase() == "NOM_ESTRATO") {
                                indexEstrato = app.camposTabla[i].codigo;
                            }
                        }
                        console.log("Nom_ servicio : " + indexServicio)
                        console.log("Nom_ estrato : " + indexEstrato)

                        var tarifa = "";

                        for (j in app.arrayEexcel) {

                            if (j > 0) {

                                var estrato = 0;

                                if (!(app.arrayEexcel[j][indexEstrato] === null ||
                                    app.arrayEexcel[j][indexEstrato] === undefined ||
                                    app.arrayEexcel[j][indexEstrato] === "" ||
                                    app.arrayEexcel[j][indexEstrato] === " "
                                )) {
                                    estrato = app.arrayEexcel[j][indexEstrato];
                                }


                                try {
                                    tarifa = (app.arrayEexcel[j][indexServicio]).trim() + "" + (estrato).trim();
                                } catch (e) {
                                    tarifa = "";
                                }

                                if (!(app.contains(app.arrayTarifas, "tarifa", tarifa))) {

                                    app.arrayErrores.push(
                                        {
                                            valor: "Servicio: " + app.arrayEexcel[j][indexServicio] + " " + "Estrato: " + estrato,
                                            error: 'Tarifa no registrada en consumo homologacion'
                                        }
                                    )

                                }
                            }

                        }

                        app.textErrores = "Errores " + "(" + app.arrayErrores.length + ")";
                        app.textErroresArchivo = "Errores " + "(" + app.arrayErrores.length + ")";
                        app.textVerificadosArchivo = "Verificados " + "(" + app.arrayConsumos.length + ")";
                        app.textTotalArchivo = "Total " + "(" + (app.arrayEexcel.length - 1) + ")";
            */

            if (app.arrayErrores.length == 0) {
                app.armarArray();
            } else {
                console.log('array no enviado')
            }

        },

        contains: function (arr, key, val) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][key] === val) return true;
            }
            return false;
        },

        enviarArray: function () {

            var app = this;

            //console.log(app.arrayConsumos)

            app.ajax = true;
            //if (app.tablaConsumos.length == 0) {
            if (true) {

                $.ajax({
                    url: './request/insertConsumo.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        mun: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes,
                        comer: app.valorComercializador,
                        array: app.arrayConsumos,
                        campos: app.camposTabla
                    },

                }).done(function (data) {

                    console.log(data)
                    var result = JSON.parse(data);
                    app.ajax = false;
                    app.arrayErrores = result['errores'];

                    let auxError = -1;

                    try {
                        auxError = app.arrayErrores.length;
                    } catch (e) {
                        auxError = 0;
                    }

                    if (auxError == 0) {
                        alertify.success("Facturacion importada correctamente");
                        app.verificar(1)
                        app.arrayConsumos = [];
                    } else {
                        app.deleteWhenError()
                        alertify.alert('Error', 'Revisa la pestalla Errores')
                    }


                }).fail(function (data) {

                });
            } else {

                alertify.warning('ya se cargaron consumos para esta busqueda')

            }
        },

        deleteWhenError: function () {
            var app = this;

            $.post('./request/deleteConsumoAux.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {
                console.log(data)
            });
        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

        detalle: function () {
        }

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        this.loadComercializador();


    },

});
