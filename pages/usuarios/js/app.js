// ----------------------------------------------
// On Load page
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(".selectDepartamento").select2().change(function (e) {

        var idDepartamento = $(this).val();
        var departamento = this.options[this.selectedIndex].text;

        app.onChangeDepartamento(idDepartamento);

    });

    $(".selectMunicipio").select2().change(function (e) {

        var idMunicipio = $(this).val();
        var municipio = this.options[this.selectedIndex].text;

        app.onChangeMunicipio(idMunicipio, municipio);

    });

    $(".selectBDUsuario").select2().change(function (e) {

        var usuario = $(this).val();
        var nombreUsuario = this.options[this.selectedIndex].text;

        app.onChangeTipoUsuario(usuario, nombreUsuario);

    });


    $("#foto").change(function () {
        readURL(this);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imgUsuario').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

});
// ----------------------------------------------
// ----------------------------------------------


let app = new Vue({
    el: '#app',
    data: {


        selectBDUsuarios: [],
        valorBDUsuario: "",

        idDepartamento: "",
        selectDepartamentos: [],

        idMunicipio: "",
        selectMunicipios: [],

        tablaDepartamentos: [],


        usuario: "",
        nombreUsuario: "",

        password1: "",
        password2: "",


    },

    methods: {

        loadSelectDepartamento: function () {
            var app = this;
            $.get('./request/getSelectDepartamentos.php', function (data) {

                app.selectDepartamentos = JSON.parse(data);

            });
        },

        loadSelectUsuariosRegistrados: function () {
            var app = this;
            $.get('./request/getSelectUsuariosRegistrados.php', function (data) {
                console.log(data)
                app.selectBDUsuarios = JSON.parse(data);

            });


        },

        onChangeDepartamento: function (departamento) {

            var app = this;
            app.idDepartamento = departamento;
            app.idMunicipio = "";
            app.selectMunicipios = [];

            if (app.idDepartamento != "") {
                $.post('./request/getMunicipios.php', {Departamento: departamento}, function (data) {

                    console.log(data)
                    app.selectMunicipios = JSON.parse(data);

                });
            }

        },

        onChangeMunicipio: function (municipio) {

            var app = this;
            app.idMunicipio = municipio;

        },

        onChangeTipoUsuario: function (usuario, nombreUsuario) {

            var app = this;


            app.usuario = usuario;
            app.nombreUsuario = usuario;


            if (nombreUsuario == "") {

                $('#imgUsuario').attr('src', "./img/user.jpg");

            } else {

                $('#imgUsuario').attr('src', "./request/getFoto.php?usuario=" + usuario);

                // app.tablaDepartamentos = [];

                $.get('./request/getClaveUsuario.php?usuario=' + usuario, function (data) {

                    console.log(data)

                    var datos = JSON.parse(data);

                    try {
                        app.nombreUsuario = datos[0].NOMBRE;
                        app.password1 = datos[0].CLAVE;
                        app.password2 = datos[0].CLAVE;
                    }catch (e) {
                        
                    }


                });


                $.get('./request/getTablaDatos.php?usuario=' + usuario, function (data) {

                    console.log(data)
                    app.tablaDepartamentos = JSON.parse(data);

                });

            }

        },

        cargarMunicipioTabla: function () {

            var app = this;

            var codDep = app.idDepartamento;
            var codMun = app.idMunicipio;
            var nombreDep = $('#selectDepartamento option:selected').text();
            var nombreMun = $("#selectMunicipio>option:selected").html()


            var rowTabla =
                {
                    "codDep": codDep,
                    "nombreDep": nombreDep,
                    "codMun": codMun,
                    "nombreMun": nombreMun
                };

            if (!(codDep == "" || codMun == "")) {

                if (app.tablaDepartamentos.length > 0) {

                    var existe = true;
                    for (var i = 0; i < app.tablaDepartamentos.length; i++) {

                        var codDep = app.tablaDepartamentos[i].codDep;
                        var codMun = app.tablaDepartamentos[i].codMun;

                        if (rowTabla.codMun == codMun && rowTabla.codDep == codDep) {

                            existe = false;

                        }

                    }

                    if (existe) {

                        app.tablaDepartamentos.push(rowTabla);

                    }

                } else {

                    app.tablaDepartamentos.push(rowTabla);

                }
            }

        },

        eliminarRow: function (index) {

            var app = this;

            app.tablaDepartamentos.splice(index, 1);


        },

        guardar: function () {

            var app = this;

            var usuario = app.usuario;
            var nombre = app.nombreUsuario;

            var pass1 = app.password1;
            var pass2 = app.password2;

            var departamentos = app.tablaDepartamentos;

            if (usuario == "" ||
                nombre == "" ||
                pass1 == "" ||
                pass2 == ""
            ) {

                alertify.error("Por favor ingresa todos los campos");

                if (departamentos.length == 0) {

                    alertify.error("Agrega por lo menos un municipio a la tabla");

                }

                if (!(pass1 == pass2)) {

                    alertify.error("Las Contraseñas no coinciden");

                }


            } else {


                var formData = new FormData($('.formGuardar')[0]);

                formData.append('departamentos', departamentos);

                //hacemos la petición ajax
                $.ajax({
                    url: './uploadFile.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {


                        app.formDatos(data, "./request/insertUsuario.php");
                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });


            }


        },

        formDatos: function (data, url) {

            var app = this;
            var departamentos = JSON.stringify(app.tablaDepartamentos);
            var formData = new FormData($('.formGuardar')[0]);
            formData.append('url', data);
            formData.append('placa1', app.usuario);
            formData.append('array', departamentos)


            //hacemos la petición ajax
            $.ajax({
                url: url,
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    console.log(data)


                    if (data.indexOf("UNIQUE KEY constraint") > -1) {

                        alertify.warning("Ya se encuentra Registrado");

                    }


                    //app.search();


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        if (url == "./request/updateUsuario.php") {


                            app.resetCampos();

                        }


                        app.resetCampos();


                    } else {


                        alertify.error("Ha ocurrido un error");
                    }


                },
                //si ha ocurrido un error
                error: function (FormData) {
                    console.log(data)
                }
            });
        },

        actualizar: function () {


            var app = this;

            var usuario = app.usuario;
            var nombre = app.nombreUsuario;

            var pass1 = app.password1;
            var pass2 = app.password2;

            var departamentos = app.tablaDepartamentos;

            if (usuario == "" ||
                nombre == "" ||
                pass1 == "" ||
                pass2 == ""

            ) {

                alertify.error("Por favor ingresa todos los campos");

                if (departamentos.length == 0) {

                    alertify.error("Agrega por lo menos un municipio a la tabla");

                }

                if (!(pass1 == pass2)) {

                    alertify.error("Las Contraseñas no coinciden");

                }


            } else {


                var formData = new FormData($('.formGuardar')[0]);


                //hacemos la petición ajax
                $.ajax({
                    url: './uploadFile.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,
                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {
                        //console.log(data);
                        app.formDatos(data, "./request/updateUsuario.php");
                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(FormData)
                    }
                });
            }


        },

        eliminar: function () {

            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este item?",
                function () {
                    $.post('./request/eliminarUsuario.php', {usuario: app.usuario}, function (data) {
                        // console.log(data);
                        if (data == 1) {
                            alertify.success("Registro Eliminado.");

                            app.loadSelectUsuariosRegistrados();
                            app.resetCampos();


                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });


        },

        resetCampos: function () {

            var app = this;


            app.valorBDUsuario = "";

            app.tablaDepartamentos = [];
            app.usuario = "";
            app.nombreUsuario = "";
            app.password1 = "";
            app.password2 = "";

            app.idDepartamento = "";
            app.idMunicipio = "";

            $('#imgUsuario').attr('src', "./img/user.jpg");

            app.selectBDUsuarios = [];
            app.selectDepartamentos = [];
            app.selectMunicipios = [];

            app.loadSelectDepartamento();
            app.loadSelectUsuariosRegistrados();

        }


    },
    watch: {},
    mounted() {
        this.loadSelectDepartamento();
        this.loadSelectUsuariosRegistrados();

    },

});
