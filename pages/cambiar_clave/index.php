<?php
  session_start();

   if(!$_SESSION['user']/* && !$_SESSION['permiso']*/){//si no se ha inciciado sesion y se esta accediendo directamente del navegador
        echo
        "<script>
            window.location.href='../inicio/index.php';
        </script>";
        exit();
	} 
?>

<!DOCTYPE html>

<html>
<head>
	<meta charset="UTF-8">

	<title>Cambiar contraseña</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="css/estilo.css">

	<link rel='stylesheet' href='css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

  	<link rel='stylesheet' href='css/tablet.css' type='text/css' media='only screen and (min-width:481px) and (max-width:831px)' />

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../../css/ionicons/css/ionicons.min.css' type='text/css' />

	<link rel="stylesheet" type="text/css" href="../../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>

	
	<script type="text/javascript">
		$(function(){
			
			//var externa = "SI";
			$( "#alert" ).dialog({
      			modal: true,
				autoOpen: false,
				resizable:false,
				buttons: {
					Ok: function() {
						$("#alert p").html("");
						$( this ).dialog( "close" );
					}
				}
			});
			
			
			
			$("#guardando").hide();


			function limpiarFormulario(){
				
				$("input[type=password]").val("");

				
			};
			
			function validarFormulario(){
				if(($("#claveAnterior").val()!="" && $("#claveNueva1").val()!="" && $("#claveNueva2").val()!="") && ($("#claveNueva1").val() == $("#claveNueva2").val())){
					return true;
				}else{
					return false;
				}
			};//fin de la funcion validar formulario

			

			//listener del evento click del boton GUARDAR

			$("#guardar").click(function(e){
				e.preventDefault();
				$("#guardar").hide();
				$("#cancelar").hide();

				//$("#form").submit();

				if(validarFormulario()){
					$("#guardando").show();
					//$("#form").submit();
					$.ajax({//ajax guardar
							url:'request/guardar.php',
			                type:'POST',
			                dataType:'json',
			                data:{
			                	claveAnterior:$("#claveAnterior").val(),
			                	claveNueva:$("#claveNueva1").val()			                	
			                }
			                
						}).done(function(repuesta){
							if(repuesta.length>0){

								$("#alert p").html(repuesta[0].msg);
								$( "#alert" ).dialog( "open" );

								//if(repuesta[0].success){								
									
									limpiarFormulario();
									//window.location.href='../../init/cerrarSesion.php';
								//}
								
								
							}else{
								//alert("Error al guardar");
								$("#alert p").html("No se pudo completar la acción");
								$( "#alert" ).dialog( "open" );
								
							}

							$("#guardar").show();
							$("#cancelar").show();
							$("#guardando").hide();
						});

				}else{
					//alert("Debes diligenciar todos los campos marcados con asterisco (*)");
					$("#alert p").html("Las contraseñas no coinciden. Debes diligenciar todos los campos marcados con asterisco (*)");
					$( "#alert" ).dialog( "open" );
					
					$("#guardar").show();
					$("#cancelar").show();
				}
				
			});// fin del evento clic de guardar
			
			//listener del evento clic del boton cancelar
			$("#cancelar").click(function(e){
				e.preventDefault();
				limpiarFormulario();
			});//fin del evento clic del boton cancelar

			$("#guardando").click(function(e){
				e.preventDefault();
			});


			$("body").show();

		});
	</script>
</head>
<body style="display:none">

	<div id="alert" title="Mensaje">
        <p></p>
    </div>

	<header>
		<h3>Cambiar contraseña</h3>
		<nav>
			<ul id="menu">
				
				<li id="guardar"><a href="" ><span class="ion-android-done"></span><h6>Guardar</h6></a></li>
				<li id="guardando"><a href="" ><span class="ion-load-d"></span><h6>Guardando...</h6></a></li>
				<li id="cancelar"><a href="" ><span class="ion-android-close"></span><h6>Cancelar</h6></a></li>
					
			</ul>
		</nav>
		
	</header>
	

	<div id='contenido'>
		
		<h4><?php echo $_SESSION['user']?></h4>
		
		<form id='form' method='POST' action='request/guardar.php'>
			<label>Contraseña actual *</label>
			<input type='password' id='claveAnterior' name='claveAnterior' >

			<label>Contraseña nueva  *</label>
			<input type='password' id='claveNueva1' name='claveNueva1' maxlength='20'>

			<label>Confirmar contraseña *</label>
			<input type='password' id='claveNueva2' maxlength='20'>
		</form>	
	</div>

	<footer><p><span class="ion-ios-information-outline"></span><em>Los campos del formulario marcados con * son obligatorios.</em></p></footer>

</body>
</html>