// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipios").select2().change(function (e) {
        let usuario = $(".selectMunicipios").val();
        let nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeDepartamento(usuario, nombreUsuario);
    });

    $(".selectClientes").select2().change(function (e) {

        var usuario = $(".selectClientes").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeClientes(usuario, nombreUsuario);
        app.loadDatos();
    });
});
// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,
        municipio: "",
        codMunicipio: "",
        prefijo: "",
        numero: "",
        idFactura: "",
        valor: "",
        codigo: "",
        saldo: "",
        valorAbono: "",
        selectClientes: [],
        valorClientes: "",
        tablaClientes: [],

        selectMunicipios: [],
        valorMunicipio: "",

    },

    methods: {

        loadDepartamentos: function () {
            let app = this;
            $.get('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);
            });
        },
        onChangeDepartamento: function (cod, nombre) {
            let app = this;
            app.valorMunicipio = cod;
            app.loadClientes();
        },

        loadClientes: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                mun: app.valorMunicipio
            }, function (data) {
                app.selectClientes = JSON.parse(data);
            });
        },
        onchangeClientes: function (cod, nombre) {
            var app = this;
            app.valorClientes = cod;
            app.codigo = cod;

            // console.log(app.codigo)
        },
        loadDatos: function () {
            var app = this;
            $.post('./request/getDatos.php', {
                selectClientes: app.valorClientes,
            }, function (data) {

                app.tablaClientes = JSON.parse(data);
                // console.log(app.tablaClientes)

                app.codMunicipio = app.tablaClientes[0]['COD_MUNICIPIO'];
                app.valor = app.tablaClientes[0]['FAC_TOTAL'];
                app.municipio = app.tablaClientes[0]['MUNICIPIO'];
                app.numero = app.tablaClientes[0]['NRO'];
                app.prefijo = app.tablaClientes[0]['PREFIJO'];
                app.idFactura = app.tablaClientes[0]['ID_FAC'];

                app.saldo = app.tablaClientes[0]['SALDO'];
                // console.log("codigo municipio :", app.codMunicipio)
                // console.log("Municipio:", app.municipio)
                // console.log("Prefijo Municipio:", app.prefijo)
                // console.log("Numero De Factura: ", app.numero)
                // console.log("Valor Factura:", app.valor)
                //console.log("Saldo Actual:", app.saldo)
            });
        },
        guardar: function () {

            let app = this;

            if (app.saldo || app.saldo === 0 || app.saldo === "") {
                alertify.warning('Debe tener saldo pendiente para realizar un abono')
            } else {

                if (app.valorAbono == "" ||
                    app.valorClientes == "") {
                    alertify.error("Por favor ingresa todos los campos");
                } else {
                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/getDatosSalida.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codMunicipio: app.valorMunicipio,
                            valorAbono: app.valorAbono,
                            idFactura: app.idFactura,
                        },

                    }).done(function (data) {
                        console.log(data)
                        var respuesta = JSON.parse(data);
                        if (respuesta.length >= 1) {
                            alertify.success("Registro guardado Exitosamente");
                            app.loadClientes();

                        } else {
                            alertify.error("Ha ocurrido un error");


                        }

                    }).fail(function (data) {

                    });
                }

            }
        },
    },
    watch: {},
    mounted() {
        this.loadDepartamentos();
    },

});
