<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css">

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 69vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Abono A Facturas

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar()">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMunicipio">
                        <option value="">Municipio...</option>
                        <option v-for="item in selectMunicipios"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>
                </div>
            </div>


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">
                    <select class="selectClientes" id="selectClientes" name="selectClientes"
                            v-model="valorClientes">
                        <option value="">Clientes</option>
                        <option v-for="selectClientes in selectClientes"
                                :value="selectClientes.COD">
                            {{selectClientes.NOM}} -
                            {{selectClientes.COD}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="number" id="codigo" class="form-control" placeholder="Email address" required=""
                           disabled
                           autocomplete="nope"
                           v-model="codigo">
                    <label for="codigo">Codigo Cliente</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="number" id="valorAbono" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="valorAbono">
                    <label for="valorAbono">Valor Del Abono</label>
                </div>
            </div>


        </div>


    </div>

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="municipio" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="municipio">-->
    <!--                    <label for="municipio">Cod Municipio</label>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="municipio" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="municipio">-->
    <!--                    <label for="municipio">Municipio</label>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="prefijo" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="prefijo">-->
    <!--                    <label for="prefijo">Prefijo</label>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="numero" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="numero">-->
    <!--                    <label for="numero">Numero</label>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="valor" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="valor">-->
    <!--                    <label for="valor">Valor Factura</label>-->
    <!--                </div>-->
    <!--            </div>-->

    <!--            <div class="col-12 col-sm-3">-->
    <!--                <div class="form-label-group">-->
    <!--                    <input type="text" id="saldo" class="form-control" placeholder="Email address" required=""-->
    <!--                           disabled-->
    <!--                           autocomplete="nope"-->
    <!--                           v-model="saldo">-->
    <!--                    <label for="saldo">Saldo</label>-->
    <!--                </div>-->
    <!--            </div>-->


    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th>Municipio</th>
                        <th>Cod Municipio</th>
                        <th>Prefijo</th>
                        <th>ID Factura</th>
                        <th>Numero Factura</th>
                        <th>Valor Factura</th>
                        <th>Saldo Pendiente</th>


                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaClientes.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaClientes">
                        <td v-text="dato.MUNICIPIO"></td>
                        <td v-text="dato.COD_MUNICIPIO"></td>
                        <td v-text="dato.PREFIJO"></td>
                        <td v-text="dato.ID_FAC"></td>
                        <td v-text="dato.NRO"></td>
                        <td>$ {{parseInt(dato.FAC_TOTAL, 10).toLocaleString()}}</td>
                        <td>$ {{parseInt(dato.SALDO, 10).toLocaleString()}}</td>


                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
