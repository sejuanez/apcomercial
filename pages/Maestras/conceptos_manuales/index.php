<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera td {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 58vh;
            overflow-y: scroll
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Concepto Manual
            <!--
            <span id="btnBuscar" v-show="btnAnadir" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Nuevo
		    	</span>
              -->
        </p>
    </header>


    <div class="container" style="padding-bottom: 5px">
        <div v-if="ajax" class="loading">Loading&#8230;</div>
        <form class="form_guardar_departamento">
            <div class="row">


                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                                    v-model="selectMunicipio">
                                <option value="" disabled>Municipio</option>
                                <option v-for="municipio in municipios" :value="municipio.codigo"
                                        v-text="municipio.nombre">

                                </option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <select class="selectAnio" id="selectAnio" name="selectAnio"
                                    v-model="selectAnio">
                                <option value="" disabled>Año</option>
                                <option v-for="anio in anios" :value="anio.codigo" v-text="anio.nombre">

                                </option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <select class="selectMes" id="selectMes" name="selectMes"
                                    v-model="selectMes">
                                <option value="" disabled>Mes</option>
                                <option v-for="mes in meses" :value="mes.codigo">
                                    {{mes.nombre}}
                                </option>
                            </select>

                        </div>
                    </div>
                </div>


                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <input type="number" id="valor" class="form-control" aria-label="valor"
                                   v-model="cliente" placeholder="Cliente">
                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <select class="selectConcepto" id="selectConcepto" name="selectConcepto"
                                    v-model="selectConcepto">
                                <option value="" disabled>Concepto</option>
                                <option v-for="Concepto in conceptos" :value="Concepto.codigo">
                                    {{Concepto.nombre}}
                                </option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="col-12 col-sm-4">
                    <div class="form-group">
                        <div class="input-group input-group-sm">
                            <input type="number" id="valor" class="form-control" aria-label="valor"
                                   v-model="valor" placeholder="Valor">
                        </div>
                    </div>
                </div>


                <div class="col-12 col-sm-12 text-center">

                </div>


            </div>


        </form>


        <br>

        <div class="row">
            <div class="col-sm-3 "></div>
            <div class="col-sm-6 ">

                <div class="input-group float-right input-group-sm">
                    <div style="padding-right: 16px;">
                        <button type="button" v-on:click="guardar()" class="btn btn-secondary btn-label">
                            Guardar
                        </button>
                    </div>
                    <input type="text" placeholder="Buscar Registro" v-model="busqueda" class="form-control">
                    <span class="input-group-btn">
                        <button type="button" v-on:click="buscar()" class="btn btn-secondary">Buscar</button>
                    </span>
                </div>
            </div>
        </div>

        <br>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <td>Municipio</td>
                        <td>Año</td>
                        <td>Mes</td>
                        <td>Cliente</td>
                        <td>Concepto</td>
                        <td>Valor</td>
                        <td style="text-align: center;" width="100">Acción</td>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tabla.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tabla" class="text-center">
                        <td v-text="dato.NOM_MUNIC"></td>
                        <td v-text="dato.NOM_ANO"></td>
                        <td v-text="dato.NOM_MES"></td>
                        <td v-text="dato.COD_CLIENTE"></td>
                        <td v-text="dato.NOM_CONCEPTO"></td>
                        <td v-text="dato.VR"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verConceptoManual(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>


    </div>

    <div id="modal" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Concepto Manual </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <form>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="selectMunicipio"> Municipio </label>
                                        <select id="selectMunicipio" name="selectMunicipio"
                                                v-model="selectMunicipio_modal" disabled>
                                            <option value="" disabled>Seleccione...</option>
                                            <option v-for="municipio in municipios" :value="municipio.codigo">
                                                {{municipio.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal"> Año </label>
                                        <select class="selectAnio" id="selectAnio" name="selectAnio"
                                                v-model="selectAnio_modal" disabled>
                                            <option value="" disabled>Seleccione...</option>
                                            <option v-for="anio in anios" :value="anio.codigo">
                                                {{anio.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal"> Mes </label>
                                        <select class="selectMes" id="selectMes" name="selectMes"
                                                v-model="selectMes_modal" disabled>
                                            <option value="" disabled>Seleccione...</option>
                                            <option v-for="mes in meses" :value="mes.codigo">
                                                {{mes.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal"> Cliente </label>
                                        <input type="number" id="cliente" disabled class="form-control"
                                               aria-label="cliente"
                                               v-model="cliente_modal">
                                    </div>
                                </div>


                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal"> Concepto </label>
                                        <select class="selectConcepto" id="selectConcepto" name="selectConcepto"
                                                v-model="selectConcepto_modal" disabled>
                                            <option value="" disabled>Seleccione...</option>
                                            <option v-for="Concepto in conceptos" :value="Concepto.codigo">
                                                {{Concepto.nombre}}
                                            </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal"> Valor </label>
                                        <input type="number" id="valor" class="form-control" aria-label="valor"
                                               v-model="valor_modal">
                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>

                    </div>

                </form>

            </div>

        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
