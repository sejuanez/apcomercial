<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */
error_reporting(0);


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");

$ID = utf8_decode($_POST['buscar']);

$consulta = "SELECT C_COD, C_NOM, C_SUMA,C_ORDEN,C_EDITA,C_GENERA_INTERES FROM CONCEPTO";

if ($ID != 'null' && $ID != '') {
    $consulta .= " WHERE C_COD LIKE '%" . $ID . "%' OR ";
    $consulta .= " C_NOM LIKE '%" . $ID . "%'";
}
$consulta .= " ORDER BY C_COD";

$return_arr = array();

$result = ibase_query($conexion, $consulta);

while ($fila = ibase_fetch_row($result)) {

    $row_array['codigo'] = utf8_encode($fila[0]);
    $row_array['nombre'] = utf8_encode($fila[1]);
    $row_array['modo'] = (1 == $fila[2]) ? 'Suma' : 'Resta';
    $row_array['orden'] = utf8_encode($fila[3]);
    $row_array['editable'] = (1 == $fila[4]) ? 'Si' : 'No';
    $row_array['interes'] = (1 == $fila[5]) ? 'Si' : 'No';
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);



