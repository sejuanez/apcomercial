﻿// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $('#selectMunicipio').select2();
    $('#selectAnio').select2();
    $('#selectMes').select2();
    $('#selectConcepto').select2();

    $('#selectMunicipio').on("change", function () {
        app.selectMunicipio = $('#selectMunicipio').val();
    });
    $('#selectAnio').on("change", function () {
        app.selectAnio = $('#selectAnio').val();
    });
    $('#selectMes').on("change", function () {
        app.selectMes = $('#selectMes').val();
    });
    $('#selectConcepto').on("change", function () {
        app.selectConcepto = $('#selectConcepto').val();
    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            valor: "",
            selectMunicipio: "",
            selectAnio: "",
            selectConcepto: "",
            selectMes: "",


            busqueda: "",

            valor_modal: "",
            selectAnio_modal: "",
            selectMes_modal: "",
            cliente_modal: "",
            selectConcepto_modal: "",
            selectMunicipio_modal: "",

            tabla: [],

            municipios: [],
            anios: [],
            conceptos: [],
            meses: [],
            cliente: "",


            noResultados: false,

        },


        methods: {
            loadConceptosManual: function () {

                var app = this;
                $.get('./request/getConceptoManual.php', function (data) {
                    console.log(data);
                    app.tabla = JSON.parse(data);
                });
            },
            loadEmpresa: function () {

                var app = this;
                $.get('./request/getEmpresa.php', function (data) {
                    //console.log(data);
                    app.municipios = JSON.parse(data);
                });
            },
            loadAnios: function () {

                var app = this;
                $.get('./request/getAno.php', function (data) {
                    //console.log(data);
                    app.anios = JSON.parse(data);
                });
            },

            loadMes: function () {

                var app = this;
                $.get('./request/getMes.php', function (data) {
                    //console.log(data);
                    app.meses = JSON.parse(data);
                });
            },


            loadSConceptos: function () {

                var app = this;
                $.get('./request/getConcepto.php', function (data) {
                    //console.log(data);
                    app.conceptos = JSON.parse(data);
                });

            },

            verConceptoManual: function (dato) {

                this.selectAnio_modal = dato.COD_ANO;
                this.selectMes_modal = dato.COD_MES;
                this.cliente_modal = dato.COD_CLIENTE;
                this.selectConcepto_modal = dato.COD_CONCEPTO;
                this.valor_modal = dato.VR;
                this.selectMunicipio_modal = dato.COD_MUNIC;

                $('#modal').modal('show');
            },

            resetModal: function () {

                $('#modal').modal('hide');

                this.selectMunicipio_modal = "";
                this.selectAnio_modal = "";
                this.cliente_modal = "";
                this.selectMes_modal = "";
                this.selectConcepto_modal = "";
                this.valor_modal = "";

            },


            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getConceptoManual.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        //console.log(data);
                        var datos = jQuery.parseJSON(data);
                        app.tabla = datos;
                        app.busqueda = "";

                    });
                } else {

                    app.loadConceptosManual()

                }

            },


            actualizar: function () {

                if (this.valor_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;
                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateConceptoManual.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            muni: app.selectMunicipio_modal,
                            anio: app.selectAnio_modal,
                            mes: app.selectMes_modal,
                            cliente: app.cliente_modal,
                            concepto: app.selectConcepto_modal,
                            valor: app.valor_modal,
                        },

                    }).done(function (data) {


                        //console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {

                                alertify.warning("Ya existe este concepto manual");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                        //si ha ocurrido un error
                    }).fail(function (data) {
                        //console.log(data)
                    });

                }

            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar este concepto manual ?",
                    function () {
                        $.post('./request/eliminarConceptoManual.php', {
                            muni: dato.COD_MUNIC,
                            anio: dato.COD_ANO,
                            mes: dato.COD_MES,
                            cliente: dato.COD_CLIENTE,
                            concepto: dato.COD_CONCEPTO
                        }, function (data) {
                            //console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                alertify.error("Error al eliminar");
                                app.buscar();
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {

                if (this.selectMunicipio == "" ||
                    this.selectAnio == "" ||
                    this.selectConcepto == "" ||
                    this.valor == "" ||
                    this.cliente == "" ||
                    this.selectMes == "") {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertConceptoManual.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            muni: this.selectMunicipio,
                            anio: this.selectAnio,
                            concepto: this.selectConcepto,
                            valor: this.valor,
                            cliente: this.cliente,
                            mes: this.selectMes,
                        },

                    }).done(function (data) {
                        //console.log("respuesta")
                        //console.log(data)

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadConceptosManual();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este concepto manual ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        //console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.anio = "";
                app.valor = "";

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadConceptosManual();
            this.loadSConceptos();
            this.loadEmpresa();
            this.loadAnios();
            this.loadMes();


        }
        ,

    })
;
