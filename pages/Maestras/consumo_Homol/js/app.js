// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipios").select2().change(function (e) {

        var usuario = $(".selectMunicipios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios(usuario, nombreUsuario);
    });
    $(".selectTarifas").change(function (e) {

        var usuario = $(".selectTarifa").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeTarifa(usuario, nombreUsuario);

    });
    $(".selectZonas").change(function (e) {

        var usuario = $(".selectZona").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeZona(usuario, nombreUsuario);

    });

    $(".selectEstrato").change(function (e) {
        var usuario = $(".selectEstrato").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeEstrato(usuario, nombreUsuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,
        codigo: "",

        id: "",
        nombreServicio: "",
        zona: "",
        estrato: "",

        codMun: "",

        selectMunicipios: [],
        valorMunicipio: "",
        selectTarifas: [],
        valorTarifa: "",
        selectZonas: [],
        valorZona: "",
        valorEstrato: "",
        busqueda: "",

        tablaClientes: [],
        tablaDepartamentos: [],

        btnModal: true,


    },


    methods: {

        loadConsumo: function () {
            var app = this;

            console.log(app.busqueda)

            if (app.valorMunicipio == "") {
                alertify.warning('Por favor selecciona un municipio')
            } else {

                $.post('./request/getConsumo.php', {
                    mun: app.valorMunicipio,
                    busqueda: app.busqueda.toUpperCase()
                }, function (data) {
                    app.tablaClientes = JSON.parse(data);
                });
            }
            app.busqueda = "";
        },

        loadMunicipios: function () {
            var app = this;
            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);


            });
        },

        onchangeMunicipios: function (cod, nombre) {
            var app = this;
            app.valorMunicipio = cod;
            if (app.valorMunicipio == "") {
                app.tablaClientes = [];
            } else {
                app.loadConsumo();
            }
        },

        loadTarifas: function (opcion) {
            var app = this;

            $.ajax({
                url: './request/getTarifa.php',
                type: 'POST',
                async: opcion,
                // Form data
                //datos del formulario
                data: {
                    mun: app.valorMunicipio
                },

            }).done(function (data) {
                app.selectTarifas = JSON.parse(data);
            }).fail(function (data) {
                console.log(data)
            });

        },

        loadZonas: function (opcion) {
            var app = this;
            $.ajax({
                url: './request/getZona.php',
                type: 'POST',
                async: opcion,
                // Form data
                //datos del formulario
                data: {
                    mun: app.valorMunicipio
                },

            }).done(function (data) {
                app.selectZonas = JSON.parse(data);
            }).fail(function (data) {
                console.log(data)
            });

        },

        verClientes: function (dato) {

            var app = this;

            app.btnModal = false;

            app.loadTarifas(false)
            app.loadZonas(false)
            app.id = dato.ID;
            app.codMun = dato.MUNIC;
            app.valorTarifa = dato.COD_TARIFA_HOMOL;
            app.valorZona = dato.COD_ZONA_HOMOL;
            app.valorEstrato = dato.COD_ESTRATO_HOMOL;
            app.nombreServicio = dato.NOM_SERVICIO;
            app.zona = dato.NOM_ZONA;
            app.estrato = dato.NOM_ESTRATO;


            $('#addFucionario').modal('show');

        },

        nuevo: function () {
            var app = this;
            app.btnModal = true;
            app.codMun = app.valorMunicipio;
            app.resetCampos();
            app.loadTarifas(true);
            app.loadZonas(true);

            $('#addFucionario').modal('show');
        },

        actualizar: function () {

            if (this.valorMunicipios_modal == "" ||
                this.valorTarifa_modal == "" ||
                this.valorZona_modal == "" ||
                //this.valorEstrato == "" ||
                this.nombreServicio_modal == "" ||
                this.zona_modal == "" ||
                this.estrato_modal == "") {
                alertify.error("Por favor ingresa todos los campos");

            } else {
                var app = this;

                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateConsumo.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        mun: app.valorMunicipios,
                        id: app.id,
                        codMun: app.codMun,
                        codTarifa: app.valorTarifa,
                        codZona: app.valorZona,
                        codEstrato: app.valorEstrato,
                        nomServicio: app.nombreServicio,
                        nomZona: app.zona,
                        nomEstrato: app.estrato,
                    },

                }).done(function (data) {
                    console.log(data)

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.loadConsumo();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {

                });

            }


        },

        eliminar: function (dato) {
            console.log(dato)
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.MUNIC + " de la empresa ?",
                function () {
                    $.post('./request/deleteConsumo.php', {
                            id: dato.ID,
                        },
                        function (data) {

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");

                                app.loadConsumo();
                                app.resetCampos();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {
                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");
                                }
                            }
                        });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {
            var app = this;

            if (app.codMun == "" ||
                app.valorTarifa == "" ||
                app.valorZona == "" ||
                app.valorEstrato == "" ||
                app.nombreServicio == "") {
                alertify.error("Por favor seleccina una taifa, zona y estrato");
            } else {

                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertConsumo.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        mun: app.valorMunicipios,
                        codMun: app.codMun,
                        codTarifa: app.valorTarifa,
                        codZona: app.valorZona,
                        codEstrato: app.valorEstrato,
                        nomServicio: app.nombreServicio,
                        nomZona: app.zona,
                        nomEstrato: app.estrato,
                    },


                }).done(function (data) {

                    console.log(data)
                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.loadConsumo();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },

        resetCampos: function () {

            app = this;

            app.id = "";
            app.valorTarifa = "";
            app.valorZona = "";
            app.valorEstrato = "";
            app.nombreServicio = "";
            app.zona = "";
            app.estrato = "";
            $('#addFucionario').modal('hide');

        },
    },


    watch: {},

    mounted() {


        this.loadMunicipios();


    },

});
