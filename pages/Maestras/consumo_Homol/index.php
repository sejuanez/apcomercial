<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css">

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 70vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Homologacion De Tarifas

            <span class="float-right" v-if="valorMunicipio!=''"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="nuevo()">
					<i class="fa fa-save" aria-hidden="true"></i> Nuevo
		    	</span>

        </p>
    </header>

    <div v-if="ajax" class="loading">Loading&#8230;</div>

    <div class="container" style="padding-bottom: 5px; max-width: 95%;">
        <div class="row" style="padding-bottom: 5px;">

            <div class="col-sm-4"></div>

            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMunicipio">
                        <option value="">Municipio</option>
                        <option v-for="selectMunicipios in selectMunicipios"
                                :value="selectMunicipios.codigo">
                            {{selectMunicipios.codigo}} -
                            {{selectMunicipios.nombre}}
                        </option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row" v-if="valorMunicipio!=''" style="padding-bottom: 5px;">
            <div class="col-sm-3 "></div>
            <div class="col-sm-6 ">
                <div class="input-group float-right input-group-sm"><input type="text"
                                                                           v-on:keyup.enter="loadConsumo()"
                                                                           v-model="busqueda"
                                                                           placeholder="Filtrar por nombre de servicio, estrato, zona, tarifa"
                                                                           class="form-control"> <span
                            class="input-group-btn"><button type="button"
                                                            @click="loadConsumo()"
                                                            class="btn btn-secondary">Buscar</button></span>
                </div>
            </div>
        </div>

    </div>


    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="60">ID</th>
                        <th width="70">Municipio</th>
                        <th>Nombre Servicio</th>
                        <th width="">Nombre Estrato</th>
                        <th width="70">Zona</th>
                        <th width="70">Tarifa</th>
                        <th width="70">Zona</th>
                        <th width="70">Estrato</th>


                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaClientes.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaClientes">
                        <td width="60" v-text="dato.ID"></td>
                        <td width="70" v-text="dato.MUNIC"></td>
                        <td v-text="dato.NOM_SERVICIO"></td>
                        <td v-text="dato.NOM_ESTRATO"></td>
                        <td width="70" v-text="dato.NOM_ZONA"></td>
                        <td width="70" v-text="dato.COD_TARIFA_HOMOL"></td>
                        <td width="70" v-text="dato.COD_ZONA_HOMOL"></td>
                        <td width="70" v-text="dato.COD_ESTRATO_HOMOL"></td>

                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verClientes(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Tarifa
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="codMun" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           disabled
                                           v-model="codMun">
                                    <label for="codMun">Cod Municipio</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectTaarifas" id="selectTaarifas" name="selectTaarifas"
                                            v-model="valorTarifa">
                                        <option value="" disabled>Tarifa</option>
                                        <option v-for="item in selectTarifas"
                                                :value="item.cod">
                                            {{item.nom}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-2">
                                <div class="form-group input-group-sm">
                                    <select class="selectZona" id="selectZona" name="selectZona"
                                            v-model="valorZona">
                                        <option value="" disabled>Zona</option>
                                        <option value="NA">NA</option>
                                        <option v-for="item in selectZonas"
                                                :value="item.COD">
                                            {{item.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>


                            <div class="col-12 col-sm-3">
                                <div class="form-group input-group-sm">
                                    <select class="selectEstrato_modal" id="selectEstrato_modal"
                                            name="selectEstrato_modal"
                                            v-model="valorEstrato">
                                        <option value="">Nro Estrato</option>
                                        <option value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="nombreServicio" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="nombreServicio">
                                    <label for="nombreServicio">Nombre Servicio</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="zona" class="form-control" placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="zona">
                                    <label for="zona">Zona</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="estrato" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="estrato">
                                    <label for="estrato">Estrato</label>
                                </div>
                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">

                        <button type="button" v-if="!(btnModal)" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>

                        <button type="button" v-if="btnModal" class="btn btn-success btn-sm" @click="guardar()">
                            Guardar
                        </button>

                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetCampos()">
                            Cancelar
                        </button>

                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
