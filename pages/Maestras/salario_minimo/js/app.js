// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            anio: "",
            valor: "",

            busqueda: "",

            anio_actualizar: "",
            anio_modal: "",
            valor_modal: "",


            tablaSalario: [],

            noResultados: false,

        },


        methods: {
            loadSalario: function () {
                let app = this;
                $.post('./request/getSalario.php', {
                    buscar: app.busqueda,
                }, function (data) {
                    console.log(data);
                    app.tablaSalario = JSON.parse(data);
                });
            },


            verSalario: function (dato) {

                this.anio_actualizar = dato.anio;
                this.anio_modal = dato.anio;
                this.valor_modal = dato.valor;

                $('#addSalario').modal('show');
            },

            resetModal: function () {

                $('#addSalario').modal('hide');

                this.anio_modal = "";
                this.valor_modal = "";
                this.anio_actualizar = "";

            },


            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getSalario.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data);
                        var datos = jQuery.parseJSON(data);
                        app.tablaSalario = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadSalario()

                }

            },


            actualizar: function () {

                if (this.anio_modal == "" ||
                    this.valor_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;
                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateSalario.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            anio: app.anio_actualizar,
                            anio_nuevo: app.anio_modal,
                            valor_nuevo: app.valor_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {

                                alertify.warning("Ya existe un salario en este año");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el Salario del año " + dato.anio + " ?",
                    function () {
                        $.post('./request/eliminarSalario.php', {
                            anio: dato.anio,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                alertify.error("Error al eliminar");
                                app.buscar();
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (this.anio == "" ||
                    this.valor == "") {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertSalario.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            anio: this.anio,
                            valor: this.valor
                        },

                    }).done(function (data) {

                        console.log(data)

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadSalario();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este año ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.anio = "";
                app.valor = "";

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadSalario();


        }
        ,

    })
;
