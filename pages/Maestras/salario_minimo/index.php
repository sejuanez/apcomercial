<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }

        table tr.cabecera td {
            color: #fff;
            text-align: center;
        }

        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Salarios

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
		    	</span>

        </p>
    </header>

    <div v-if="ajax" class="loading">Loading&#8230;</div>
    <div class="container">

        <div class="row">

            <div class="col-sm-2"></div>

            <div class="col-12 col-sm-4">
                <div class="form-label-group">
                    <input type="text" id="anio" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="anio">
                    <label for="anio">Año</label>
                </div>
            </div>

            <div class="col-12 col-sm-4">
                <div class="form-label-group">
                    <input type="text" id="valor" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="valor">
                    <label for="valor">Valor</label>
                </div>
            </div>


        </div>


        <div class="row">
            <div class="col-sm-3 "></div>
            <div class="col-sm-6 ">
                <div class="input-group float-right input-group-sm"><input type="text"
                                                                           placeholder="Filtrar por año o valor"
                                                                           v-model="busqueda"
                                                                           class="form-control"> <span
                            class="input-group-btn"><button type="button"
                                                            v-on:click="buscar()"
                                                            class="btn btn-secondary">Buscar</button></span></div>
            </div>
        </div>

        <br>


    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <td>Año</td>
                        <td>Valor</td>
                        <td style="text-align: center;" width="100">Acción</td>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaSalario.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaSalario">
                        <td v-text="dato.anio"></td>
                        <td>$ {{parseInt(dato.valor, 10).toLocaleString()}}</td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verSalario(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <div id="addSalario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Salario
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <form>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal">Año </label>
                                        <input type="text" id="descripcion" name="descripcion_modal"
                                               class="form-control" required aria-label="descripcion_modal"
                                               v-model="anio_modal">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="descripcion_modal">Valor </label>
                                        <input type="text" id="descripcion" name="nombre_modal"
                                               class="form-control" required aria-label="nombre_modal"
                                               v-model="valor_modal">
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
