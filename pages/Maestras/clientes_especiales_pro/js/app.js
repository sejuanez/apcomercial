// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


    $(".selectDepartamentos").change(function (e) {

        var usuario = $(".selectDepartamentos").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeDepartamento(usuario, nombreUsuario);

        app.loadMunicipios();

    });
    $(".selectMunicipios").change(function (e) {

        var usuario = $(".selectMunicipios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios(usuario, nombreUsuario);
        app.loadSector();


    });
    $(".selectSector").change(function (e) {

        var usuario = $(".selectSector").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeSector(usuario, nombreUsuario);
        app.loadBarrios();


    });
    $(".selectActividad").change(function (e) {

        var usuario = $(".selectActividad").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


    });
    $(".selectBarrios").change(function (e) {

        var usuario = $(".selectBarrios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


    });


    $(".selectDepartamentos_modal").change(function (e) {

        var usuario = $(".selectDepartamentos_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeDepartamento_modal(usuario, nombreUsuario);

        app.loadMunicipios_modal();

    });
    $(".selectMunicipios_modal").change(function (e) {

        var usuario = $(".selectMunicipios_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios_modal(usuario, nombreUsuario);
        app.loadSector_modal();


    });


    $(".selectSector_modal").change(function (e) {
        var usuario = $(".selectSector_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeSector_modal(usuario, nombreUsuario);
        app.loadBarrios_modal();

    });
    $(".selectActividad_modal").change(function (e) {

        var usuario = $(".selectActividad_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


    });


    $(".selectBarrios_modal").change(function (e) {

        var usuario = $(".selectBarrios_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            ajax: false,
            vrmin: "",
            vrmax: "",
            ic: "",
            icm: "",


            direccion: "",
            codigo: "",
            nombre: "",
            identificacion: "",
            repLegal: "",
            telefono: "",
            nroClientes: "",
            textoFac: "",

            selectMunicipios: [],
            valorMunicipios: "",
            selectDepartamentos: [],
            valorDepartamentos: "",
            selectBarrios: [],
            valorBarrios: "",
            selectTipoIdentidad: [],
            valorTipoIdentidad: "",
            selectActividad: [],
            valorActividad: "",
            selectSector: [],
            valorSector: "",

            busqueda: "",
            codigo_actualizar: "",

            selectMunicipios_modal: "",
            valorMunicipios_modal: "",
            selectDepartamentos_modal: "",
            valorDepartamentos_modal: "",
            selectBarrios_modal: "",
            valorBarrios_modal: "",
            selectTipoIdentidad_modal: "",
            valorTipoIdentidad_modal: "",
            selectActividad_modal: "",
            valorActividad_modal: "",
            selectSector_modal: "",
            valorSector_modal: "",
            direccion_modal: "",
            codigo_modal: "",
            nombre_modal: "",
            identificacion_modal: "",
            repLegal_modal: "",
            telefono_modal: "",
            nroClientes_modal: "",
            textoFac_modal: "",


            tablaClientes: [],
            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadClientes: function () {
                var app = this;
                $.get('./request/getClientesEspeciales.php', function (data) {
                    app.tablaClientes = JSON.parse(data);
                });
            },

            loadDepartamentos: function () {
                var app = this;
                $.get('./request/getDepartamentos.php', function (data) {
                    app.selectDepartamentos = JSON.parse(data);

                });
            },

            loadMunicipios: function () {
                var app = this;
                $.post('./request/getMunicipios.php', {
                    selectDepartamentos: app.valorDepartamentos,
                }, function (data) {
                    app.selectMunicipios = JSON.parse(data);

                });
            },

            loadSector: function () {
                var app = this;

                $.post('./request/getSector.php', {
                        selectDepartamentos: app.valorDepartamentos,
                        selectMunicipios: app.valorMunicipios
                    },
                    function (data) {

                        app.selectSector = JSON.parse(data);

                    });
            },

            loadBarrios: function () {
                var app = this;
                $.post('./request/getBarrios.php', {
                        selectDepartamentos: app.valorDepartamentos,
                        selectMunicipios: app.valorMunicipios,
                        selectSector: app.valorSector,
                    },
                    function (data) {
                        app.selectBarrios = JSON.parse(data);

                    });
            },

            loadTipoIdentidad: function () {
                var app = this;
                $.get('./request/getTipoIdentidad.php', function (data) {

                    app.selectTipoIdentidad = JSON.parse(data);

                });
            },

            loadActividad: function () {
                var app = this;
                $.get('./request/getActividad.php', function (data) {

                    app.selectActividad = JSON.parse(data);

                });
            },


            loadDepartamentos_modal: function () {
                var app = this;
                $.get('./request/getDepartamentos_modal.php', function (data) {
                    app.selectDepartamentos_modal = JSON.parse(data);

                });
            },

            loadMunicipios_modal: function () {
                var app = this;
                $.post('./request/getMunicipios.php', {
                    selectDepartamentos: app.valorDepartamentos_modal,
                }, function (data) {
                    app.selectMunicipios_modal = JSON.parse(data);

                });
            },

            loadSector_modal: function () {
                var app = this;
                $.post('./request/getSector_modal.php', {

                        selectDepartamentos_modal: app.valorDepartamentos_modal,
                        selectMunicipios_modal: app.valorMunicipios_modal
                    },
                    function (data) {

                        app.selectSector_modal = JSON.parse(data);


                    });
            },

            loadBarrios_modal: function () {
                var app = this;
                $.post('./request/getBarrios_modal.php', {
                        selectDepartamentos_modal: app.valorDepartamentos_modal,
                        selectMunicipios_modal: app.valorMunicipios_modal,
                        selectSector_modal: app.valorSector_modal,
                    },
                    function (data) {
                        app.selectBarrios_modal = JSON.parse(data);


                    });
            },

            loadTipoIdentidad_modal: function () {
                var app = this;
                $.get('./request/getTipoIdentidad_modal.php', function (data) {

                    app.selectTipoIdentidad_modal = JSON.parse(data);

                });
            },

            loadActividad_modal: function () {
                var app = this;
                $.get('./request/getActividad_modal.php', function (data) {

                    app.selectActividad_modal = JSON.parse(data);


                });
            },


            onchangeDepartamento: function (cod, nombre) {
                var app = this;

                app.valorDepartamentos = cod;
            },

            onchangeMunicipios: function (cod, nombre) {

                var app = this;

                app.valorMunicipios = cod;
            },

            onchangeSector: function (cod, nombre) {

                var app = this;

                app.valorSector = cod;
            },


            onchangeDepartamento_modal: function (cod, nombre) {
                var app = this;

                app.valorDepartamentos_modal = cod;
            },

            onchangeMunicipios_modal: function (cod, nombre) {

                var app = this;

                app.valorMunicipios_modal = cod;
            },

            onchangeSector_modal: function (cod, nombre) {

                var app = this;

                app.valorSector_modal = cod;
            },


            verClientes: function (dato) {
                this.codigo_modal = dato.COD;
                this.nombre_modal = dato.NOM;
                this.valorTipoIdentidad_modal = dato.COD_TIPO_IDENT;
                this.identificacion_modal = dato.IDENT;
                this.repLegal_modal = dato.NOM_REPLEG;
                this.valorDepartamentos_modal = dato.COD_DPTO;


                this.loadMunicipios_modal()
                setTimeout(function () {
                    this.valorMunicipios_modal = dato.COD_MUNIC;
                    $("#selectMunicipios_modal").val(this.valorMunicipios_modal).trigger('change')
                }, 500);
                setTimeout(function () {
                    this.valorSector_modal = dato.SECT;
                    $("#selectSector_modal").val(this.valorSector_modal).trigger('change')
                    console.log(this.valorSector_modal)
                }, 600);
                setTimeout(function () {

                    this.valorBarrios_modal = dato.COD_BARRIO;
                    $("#selectBarrios_modal").val(this.valorBarrios_modal).trigger('change')
                    console.log(this.valorBarrios_modal)

                }, 900);
                setTimeout(function () {
                    this.valorActividad_modal = dato.COD_ACTIV;
                    $("#valorActividad_modal").val(this.valorActividad_modal).trigger('change')
                    console.log(this.valorActividad_modal)

                }, 1100);
                this.direccion_modal = dato.DIREC;
                this.telefono_modal = dato.TELEF;

                this.nroClientes_modal = dato.NRO_CLIENTE;
                this.textoFac_modal = dato.TEXTO1_FACT;


                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {

                        var datos = jQuery.parseJSON(data);
                        app.tablaClientes = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadClientes()

                }

            },

            actualizar: function () {

                if (this.codigo_modal == "" ||
                    this.nombre_modal == "" ||
                    this.valorTipoIdentidad_modal == "" ||
                    this.identificacion_modal == "" ||
                    this.repLegal_modal == "" ||
                    this.valorDepartamentos_modal == "" ||
                    this.valorMunicipios_modal == "" ||
                    this.valorSector_modal == "" ||
                    this.valorBarrios_modal == "" ||
                    this.direccion_modal == "" ||
                    this.telefono_modal == "" ||
                    this.valorActividad_modal == "" ||
                    this.nroClientes_modal == "" ||
                    this.textoFac_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateClientes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {

                            codigo_modal: this.codigo_modal,
                            nombre_modal: this.nombre_modal,
                            valorTipoIdentidad_modal: this.valorTipoIdentidad_modal,
                            identificacion_modal: this.identificacion_modal,
                            repLegal_modal: this.repLegal_modal,
                            valorDepartamentos_modal: this.valorDepartamentos_modal,
                            valorMunicipios_modal: this.valorMunicipios_modal,
                            valorSector_modal: this.valorSector_modal,
                            valorBarrios_modal: this.valorBarrios_modal,
                            direccion_modal: this.direccion_modal,
                            telefono_modal: this.telefono_modal,
                            valorActividad_modal: this.valorActividad_modal,
                            nroClientes_modal: this.nroClientes_modal,
                            textoFac_modal: this.textoFac_modal,
                        },

                    }).done(function (data) {
                        console.log(data)

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {

                    });

                }


            },

            eliminar: function (dato) {
                console.log(dato)
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.COD + " de la empresa ?",
                    function () {
                        $.post('./request/deleteClientes.php', {
                            codigo: dato.COD,

                        }, function (data) {
                            console.log(data)
                            app.codigo = "";

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {

                if (this.codigo == "" ||
                    this.nombre == "" ||
                    this.valorTipoIdentidad == "" ||
                    this.identificacion == "" ||
                    this.repLegal == "" ||
                    this.valorDepartamentos == "" ||
                    this.valorSector == "" ||
                    this.valorMunicipios == "" ||
                    this.valorBarrios == "" ||
                    this.direccion == "" ||
                    this.telefono == "" ||
                    this.valorActividad == "" ||
                    this.nroClientes == "" ||
                    this.textoFac == "") {
                    alertify.error("Por favor ingresa todos los campos");
                } else {
                    var app = this;
                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertClientes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo,
                            nombre: this.nombre,
                            valorTipoIdentidad: this.valorTipoIdentidad,
                            identificacion: this.identificacion,
                            repLegal: this.repLegal,
                            valorDepartamentos: this.valorDepartamentos,
                            valorMunicipios: this.valorMunicipios,
                            valorSector: this.valorSector,
                            valorBarrios: this.valorBarrios,
                            direccion: this.direccion,
                            telefono: this.telefono,
                            valorActividad: this.valorActividad,
                            nroClientes: this.nroClientes,
                            textoFac: this.textoFac,
                        },

                    }).done(function (data) {

                        console.log(data)
                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadClientes();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {

                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.vrmin = "";
                app.vrmax = "";
                app.ic = "";
                $("#select-ic").val("").trigger('change')
                app.icm = "";
                $("#select-icm").val("").trigger('change')
                app.valorActividad = "";
                $("#selectActividad").val("").trigger('change')


            }
        },


        watch: {}
        ,

        mounted() {

            //this.loadAnio();
            this.loadDepartamentos();
            this.loadTipoIdentidad();
            this.loadTipoIdentidad_modal();
            this.loadActividad();
            this.loadActividad_modal();


        }
        ,

    })
;
