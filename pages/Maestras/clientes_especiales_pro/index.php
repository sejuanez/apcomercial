<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 58vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Clientes Especiales

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar()">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
		    	</span>

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="buscar()">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">


        <div v-if="ajax" class="loading">Loading&#8230;</div>
        <div class="row">

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectDepartamentos" id="selectDepartamentos" name="selectDepartamentos"
                            v-model="valorDepartamentos">
                        <option value="">Departamentos</option>
                        <option v-for="selectDepartamentos in selectDepartamentos"
                                :value="selectDepartamentos.COD">
                            {{selectDepartamentos.NOM}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMunicipios">
                        <option value="">Municipio</option>
                        <option v-for="selectMunicipios in selectMunicipios"
                                :value="selectMunicipios.codigo">
                            {{selectMunicipios.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectSector" id="selectSector" name="selectSector"
                            v-model="valorSector">
                        <option value="">Sector</option>
                        <option v-for="selectSector in selectSector"
                                :value="selectSector.COD_SECT">
                            {{selectSector.NOM_SECT}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectBarrios" id="selectBarrios" name="selectBarrios"
                            v-model="valorBarrios">
                        <option value="">Barrios</option>
                        <option v-for="selectBarrios in selectBarrios"
                                :value="selectBarrios.COD">
                            {{selectBarrios.NOM}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="direccion" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="direccion">
                    <label for="direccion">Direción</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="codigo" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="codigo">
                    <label for="codigo">Codigo</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="nombre" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="nombre">
                    <label for="nombre">Nombre</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectTipoIdentidad" id="selectTipoIdentidad" name="selectTipoIdentidad"
                            v-model="valorTipoIdentidad">
                        <option value="">Tipo Identificación</option>
                        <option v-for="selectTipoIdentidad in selectTipoIdentidad"
                                :value="selectTipoIdentidad.COD">
                            {{selectTipoIdentidad.NOM}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="identificacion" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="identificacion">
                    <label for="identificacion">Identidicacion</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="repLegal" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="repLegal">
                    <label for="repLegal">Rep. Legal</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="telefono" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="telefono">
                    <label for="telefono">Telefono</label>
                </div>
            </div>

            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectActividad" id="selectActividad" name="selectActividad"
                            v-model="valorActividad">
                        <option value="">Actividad</option>
                        <option v-for="selectActividad in selectActividad"
                                :value="selectActividad.COD">
                            {{selectActividad.NOM}}
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="nroClientes" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="nroClientes">
                    <label for="nroClientes">Nro Clientes</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="textoFac" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="textoFac">
                    <label for="textoFac">Texto Facura</label>
                </div>
            </div>


            <div class="col-12 col-sm-3">

                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm"><input type="text"
                                                                               placeholder="Filtrar por codigo nombre"
                                                                               v-model="busqueda"
                                                                               class="form-control">
                        <span class="input-group-btn"><button type="button"
                                                              v-on:click="buscar()"
                                                              class="btn btn-secondary">Buscar</button></span>
                    </div>
                </div>

            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th>Identificación</th>
                        <th>Actividad</th>


                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaClientes.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaClientes">
                        <td v-text="dato.COD"></td>
                        <td v-text="dato.NOM"></td>
                        <td v-text="dato.IDENT"></td>
                        <td v-text="dato.COD_ACTIV"></td>

                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verClientes(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Tarifa
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectDepartamentos_modal" id="selectDepartamentos_modal"
                                            name="selectDepartamentos_modal"
                                            v-model="valorDepartamentos_modal">
                                        <option value="">Departamentos</option>
                                        <option v-for="selectDepartamentos_modal in selectDepartamentos"
                                                :value="selectDepartamentos_modal.COD">
                                            {{selectDepartamentos_modal.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectMunicipios_modal" id="selectMunicipios_modal"
                                            name="selectMunicipios_modal"
                                            v-model="valorMunicipios_modal">
                                        <option value="">Municipio</option>
                                        <option v-for="selectMunicipios_modal in selectMunicipios_modal"
                                                :value="selectMunicipios_modal.codigo">
                                            {{selectMunicipios_modal.nombre}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectSector_modal" id="selectSector_modal" name="selectSector_modal"
                                            v-model="valorSector_modal">
                                        <option value="">Sector</option>
                                        <option v-for="selectSector_modal in selectSector_modal"
                                                :value="selectSector_modal.COD_SECT">
                                            {{selectSector_modal.NOM_SECT}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectBarrios_modal" id="selectBarrios_modal"
                                            name="selectBarrios_modal"
                                            v-model="valorBarrios_modal">
                                        <option value="">Barrios</option>
                                        <option v-for="selectBarrios_modal in selectBarrios_modal"
                                                :value="selectBarrios_modal.COD">
                                            {{selectBarrios_modal.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="direccion_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="direccion_modal">
                                    <label for="direccion_modal">Direción</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="codigo_modal" class="form-control"
                                           disabled=""
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="codigo_modal">
                                    <label for="codigo_modal">Codigo</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="nombre_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="nombre_modal">
                                    <label for="nombre_modal">Nombre</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectTipoIdentidad_modal" id="selectTipoIdentidad_modal"
                                            name="selectTipoIdentidad_modal"
                                            v-model="valorTipoIdentidad_modal">
                                        <option value="">Tipo Identificación</option>
                                        <option v-for="selectTipoIdentidad_modal in selectTipoIdentidad_modal"
                                                :value="selectTipoIdentidad_modal.COD">
                                            {{selectTipoIdentidad_modal.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="identificacion_modal" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="identificacion_modal">
                                    <label for="identificacion_modal">Identidicacion</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="repLegal_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="repLegal_modal">
                                    <label for="repLegal_modal">Rep. Legal</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="telefono_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="telefono_modal">
                                    <label for="telefono_modal">Telefono</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-group input-group-sm">
                                    <select class="selectActividad_modal" id="selectActividad_modal"
                                            name="selectActividad_modal"
                                            v-model="valorActividad_modal">
                                        <option value="">Actividad</option>
                                        <option v-for="selectActividad_modal in selectActividad"
                                                :value="selectActividad_modal.COD">
                                            {{selectActividad_modal.NOM}}
                                        </option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="nroClientes_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="nroClientes_modal">
                                    <label for="nroClientes_modal">Nro Clientes</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-4">
                                <div class="form-label-group">
                                    <input type="text" id="textoFac_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="textoFac_modal">
                                    <label for="textoFac_modal">Texto Facura</label>
                                </div>
                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
