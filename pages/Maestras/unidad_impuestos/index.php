<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css">

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">
    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 75vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Unidad De Impuestos

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar()">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
		    	</span>

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="buscar()">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMunicipios">
                        <option value="">Municipio</option>
                        <option v-for="selectMunicipios in selectMunicipios"
                                :value="selectMunicipios.codigo">
                            {{selectMunicipios.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectAnio" id="selectAnio" name="selectAnio"
                            v-model="valorAnio">
                        <option value="">Año</option>
                        <option v-for="selectAnio in selectAnio"
                                :value="selectAnio.COD">
                            {{selectAnio.NOM}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">
                    <select class="selectMes" id="selectMes" name="selectMes"
                            v-model="valorMes">
                        <option value="">Mes</option>
                        <option v-for="selectMes in selectMes"
                                :value="selectMes.COD">
                            {{selectMes.NOM}}
                        </option>
                    </select>

                </div>
            </div>


            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="number" id="valorvUvt" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model="valorvUvt">
                    <label for="valorvUvt">Valor</label>
                </div>
            </div>

            <div class="checkbox">
                <input type="checkbox" id="anual" value=""
                       v-model=" anual">
                <label for="anual">Anual</label>

            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th>Municipio</th>
                        <th>Año</th>
                        <th>Mes</th>
                        <th>Valor</th>


                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaClientes.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaClientes">
                        <td v-text="dato.COD_EMPRESA"></td>
                        <td v-text="dato.COD_ANO"></td>
                        <td v-text="dato.COD_MES"></td>
                        <td v-text="dato.VR_UVT"></td>


                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verClientes(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Unidad de Impuesto
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="valorvUvt" class="form-control" placeholder="Email address"
                                           required=""
                                           disabled
                                           autocomplete="nope"
                                           v-model="valorMunicipios_modal">
                                    <label for="valorvUvt">Codigo Municipio</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group input-group-sm">
                                    <select class="selectAnio_modal" id="selectAnio_modal" name="selectAnio_modal"
                                            v-model="valorAnio_modal">
                                        <option value="" disabled>Año</option>
                                        <option v-for="selectAnio_modal in selectAnio_modal"
                                                :value="selectAnio_modal.COD">
                                            {{selectAnio_modal.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group input-group-sm">
                                    <select class="selectMes_modal" id="selectMes_modal" name="selectMes_modal"
                                            v-model="valorMes_modal">
                                        <option value="" disabled>Mes</option>
                                        <option v-for="selectMes_modal in selectMes_modal"
                                                :value="selectMes_modal.COD">
                                            {{selectMes_modal.NOM}}
                                        </option>
                                    </select>

                                </div>
                            </div>


                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="number" id="valorvUvt_modal" class="form-control"
                                           placeholder="Email address"
                                           required=""
                                           autocomplete="nope"
                                           v-model="valorvUvt_modal">
                                    <label for="valorvUvt_modal">Valor</label>
                                </div>
                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
