// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


    $(".selectMunicipios").select2().change(function (e) {

        var usuario = $(".selectMunicipios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios(usuario, nombreUsuario);

        app.resetCampos(11);
    });
    $(".selectAnio").select2().change(function (e) {

        var usuario = $(".selectAnio").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeAnio(usuario, nombreUsuario);

    });
    $(".selectMes").select2().change(function (e) {

        var usuario = $(".selectMes").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMes(usuario, nombreUsuario);

    });


    $(".selectMunicipios_modal").change(function (e) {

        var usuario = $(".selectMunicipios_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios_modal(usuario, nombreUsuario);
        app.resetCampos();
    });

    $(".selectAnio_modal").change(function (e) {
        var usuario = $(".selectAnio_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeAnio_modal(usuario, nombreUsuario);

    });

    $(".selectMes_modal").change(function (e) {
        var usuario = $(".selectMes_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMes_modal(usuario, nombreUsuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {


        ajax: false,
        codigo: "",

        valorvUvt: "",
        valorvUvt_modal: "",
        anual: "",
        anual_modal: "",

        codigo_actualizar: "",
        nombre_modal: "",
        codigo_modal: "",


        selectMunicipios: [],
        valorMunicipios: "",
        selectMunicipios_modal: [],
        valorMunicipios_modal: "",
        selectAnio: [],
        valorAnio: "",
        selectAnio_modal: [],
        valorAnio_modal: "",
        selectMes: [],
        valorMes: "",
        selectMes_modal: [],
        valorMes_modal: "",
        busqueda: "",
        codigoActualizar: "",

        tablaClientes: [],
        tablaDepartamentos: [],


        noResultados:
            false,


    },


    methods: {

        loadMunicipios: function () {
            var app = this;
            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);


            });
        },
        loadAnio: function () {
            var app = this;
            $.post('./request/getAnio.php', {}, function (data) {
                app.selectAnio = JSON.parse(data);

            });
        },
        loadMes: function () {
            var app = this;

            $.post('./request/getMes.php', {},
                function (data) {

                    app.selectMes = JSON.parse(data);

                });
        },
        onchangeMunicipios: function (cod, nombre) {


            var app = this;

            app.valorMunicipios = cod;
        },
        onchangeAnio: function (cod, nombre) {
            var app = this;

            app.valorAnio = cod;
        },
        onchangeMes: function (cod, nombre) {

            var app = this;
            app.valorMes = cod;
        },
        loadAnio_modal: function () {
            var app = this;


            $.ajax({
                url: './request/getAnio_modal.php',
                type: 'POST',
                async: false,
                data: {},

            }).done(function (response) {
                app.selectAnio_modal = JSON.parse(response);
                //return response;
            }).fail(function (error) {

                console.log(error)

            });


        },
        loadMes_modal: function () {
            var app = this;
            $.ajax({
                url: './request/getMes_modal.php',
                type: 'POST',
                async: false,
                data: {},

            }).done(function (response) {
                app.selectMes_modal = JSON.parse(response);
                //return response;
            }).fail(function (error) {

                console.log(error)

            });
        },
        onchangeMunicipios_modal: function (cod, nombre) {

            var app = this;

            app.valorMunicipios_modal = cod;
        },
        onchangeAnio_modal: function (cod, nombre) {
            var app = this;

            app.valorAnio_modal = cod;
        },
        onchangeMes_modal: function (cod, nombre) {

            var app = this;
            app.valorMes_modal = cod;
        },
        verClientes: function (dato) {

            console.log(dato)

            let app = this;
            app.valorvUvt_modal = dato.VR_UVT;
            app.valorMunicipios_modal = dato.COD_EMPRESA;


            app.loadAnio_modal()

            app.valorAnio_modal = dato.COD_ANO;
            //$("#selectAnio_modal").val(app.valorAnio_modal).trigger('change')

            app.loadMes_modal()

            app.valorMes_modal = dato.COD_MES;
            //$("#selectMes_modal").val(app.valorMes_modal).trigger('change')


            $('#addFucionario').modal('show');
        },
        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
        },
        buscar: function () {
            let app = this;

            if (app.valorMunicipios != "") {
                $.post('./request/getUnidadImpuestos.php', {
                    mun: app.valorMunicipios,
                    anio: app.valorAnio,
                    mes: app.valorMes,
                }, function (data) {

                    app.tablaClientes = jQuery.parseJSON(data);
                    app.busqueda = "";

                });
            } else {

                alertify.warning('Selecciona un municipio');

            }

        },
        guardar: function () {

            if (this.valorMunicipios == "" ||
                this.valorAnio == "" ||
                this.valorMes == "" ||
                this.valorvUvt == "") {
                alertify.error("Por favor ingresa todos los campos");
            } else {
                var app = this;
                var valor;
                if (app.anual == true) {
                    valor = 1;
                } else {
                    valor = 0;
                }
                console.log(valor)
                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertUnidadImpuestos.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        valorMunicipios: this.valorMunicipios,
                        valorAnio: this.valorAnio,
                        valorMes: this.valorMes,
                        valorvUvt: this.valorvUvt,
                        anual: valor
                    },


                }).done(function (data) {

                    console.log(data)
                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.buscar();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },
        actualizar: function () {

            if (this.valorMunicipios_modal == "" ||
                this.valorAnio_modal == "" ||
                this.valorMes_modal == "" ||
                this.valorvUvt_modal == "") {

                $('.formGuardar'
                ).addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {
                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateUnidadImpuestos.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        valorMunicipios_modal: this.valorMunicipios_modal,
                        valorAnio_modal: this.valorAnio_modal,
                        valorMes_modal: this.valorMes_modal,
                        valorvUvt_modal: this.valorvUvt_modal,

                    },

                }).done(function (data) {
                    console.log(data)

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.buscar();
                        app.resetModal();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {

                });

            }


        },
        eliminar: function (dato) {
            console.log(dato)
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.COD_EMPRESA + " de la empresa ?",
                function () {
                    $.post('./request/deleteUnidadImpuestos.php', {

                        valorAnio: dato.COD_ANO,
                        valorMunicipios: dato.COD_EMPRESA,
                        valorMes: dato.COD_MES,
                    }, function (data) {

                        app.valorAnio = "";
                        app.valorMunicipios = "";
                        app.valorMes = "";

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");

                            app.buscar();
                            app.resetModal();


                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {
                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                console.log(data)
                                alertify.error("Ha ocurrido un error");
                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },
        resetCampos: function (op) {

            app = this;

            if (op == 1) {
                $("#selectMunicipios").val("").trigger('change')
                app.valorMunicipios = "";
            }


            $("#selectAnio").val("").trigger('change')
            app.valorAnio = "";
            $("#selectMes").val("").trigger('change')
            app.valorMes = "";
            app.valorvUvt = "";


        }
    },


    watch:
        {},

    mounted() {


        this.loadMunicipios();
        this.loadAnio();
        this.loadMes();


    },

});
