// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeMunicipio(usuario, nombreUsuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });

    $(".selectTabla").select2().change(function (e) {
        var usuario = $(".selectTabla").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeTabla(usuario, nombreUsuario);
    });

    $(".selectCampos").select2().change(function (e) {
        var usuario = $(".selectCampos").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeCampo(usuario, nombreUsuario);
    });

    $(".selectTipoDato").select2().change(function (e) {
        var usuario = $(".selectTipoDato").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeTipoDato(usuario, nombreUsuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        nroColumna: "",

        valorMunicipio: "",
        selectMunicipios: [],

        selectTabla: [],
        valorTabla: "",

        selectCampos: [],
        valorCampo: "",

        selectTipoDato: [],
        valorTipoDato: "",

        busqueda: "",

        codigo_actualizar: "",
        codigo_modal: "",
        nombre_modal: "",

        selectComercializador: [],
        valorComercializador: "",

        tablaRegistros: [],

        noResultados: false,

    },


    methods: {

        loadComercializador: function () {

            var app = this;
            $.post('./request/getComercializador.php', function (data) {
                console.log(data)
                app.selectComercializador = JSON.parse(data);
            });

        },

        onChangeComercializador: function (cod) {
            var app = this;
            app.valorComercializador = cod;
            app.loadTabla()
            app.loadTipoDato()
        },

        loadTipoDato: function () {
            var app = this;
            $.get('./request/getTipoDato.php', function (data) {
                app.selectTipoDato = JSON.parse(data);
            });
        },

        onChangeTipoDato: function (cod, nombre) {
            var app = this;
            console.log(cod)
            app.valorTipoDato = cod;
        },

        loadMunicipios: function () {
            var app = this;
            $.get('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);
            });
        },

        onChangeMunicipio: function (cod, nombre) {
            var app = this;
            console.log(cod)
            app.valorMunicipio = cod;
            app.selectTabla = []
            app.selectCampos = []
            app.valorTabla = "";
            app.valorCampo = "";
            app.valorComercializador = "";
            $("#selectTabla").val("").trigger('change.select2')
            $("#selectCampos").val("").trigger('change.select2')
            $("#selectComercializador").val("").trigger('change.select2')

        },

        loadTabla: function () {
            var app = this;
            $.post('./request/getTablas.php', {
                mun: app.valorMunicipio,
                comer: app.valorComercializador
            }, function (data) {

                app.selectTabla = JSON.parse(data);

                if (app.selectTabla.length == 0) {
                    alertify.warning('El comercializador de este municipio no cuenta con tablas')
                } else {
                    app.loadCampos();
                }

            });
        },

        onChangeTabla: function (cod, nombre) {
            var app = this;
            console.log(cod);
            app.valorTabla = cod;
            app.loadCampos();
            app.loadRegistros();
        },

        loadRegistros: function () {
            var app = this;
            if (app.valorTabla != "") {
                $.post('./request/getRegistros.php', {
                    mun: app.valorMunicipio,
                    tabla: app.valorTabla,
                    comer: app.valorComercializador,
                }, function (data) {
                    console.log(data)
                    app.tablaRegistros = JSON.parse(data);
                });

            }
        },

        loadCampos: function () {
            var app = this;
            if (app.valorTabla != "") {
                $.post('./request/getCampos.php', {
                    mun: app.valorMunicipio,
                    tabla: app.valorTabla,
                    comer: app.valorComercializador
                }, function (data) {
                    app.selectCampos = JSON.parse(data);
                });
            }
        },

        onChangeCampo: function (cod, nombre) {
            var app = this;
            console.log(cod)
            app.valorCampo = cod;
        },

        verDepartamento: function (dato) {

            this.codigo_actualizar = dato.codigo;
            this.codigo_modal = dato.codigo;
            this.nombre_modal = dato.nombre;
            this.valorActividad = dato.empresa;

            $('#addFucionario').modal('show');
        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        actualizar: function () {

            if (this.valorActividad == "" ||
                this.codigo_modal == "" ||
                this.nombre_modal == "") {

                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        empresa: app.valorActividad,
                        codigo: app.codigo_modal,
                        nombre_nuevo: app.nombre_modal,
                    },

                }).done(function (data) {


                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.resetModal();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {
                    console.log(data)
                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            console.log(dato)

            console.log(app.valorTabla)
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.campo + " ?",
                function () {
                    $.post('./request/deleteMes.php', {
                        idCampo: dato.idCampo,
                    }, function (data) {

                        app.valorActividad = "";

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.loadRegistros();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {


                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {

            var app = this;


            if (app.valorCampo == "" ||
                app.nroColumna == "" ||
                app.valorTipoDato == ""
            ) {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertCampo.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        mun: app.valorMunicipio,
                        campo: app.valorCampo,
                        colum: app.nroColumna,
                        tipo: app.valorTipoDato,
                    },

                }).done(function (data) {

                    console.log(data)


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadRegistros();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {
                    console.log(data)
                });


            }
        },

        resetCampos: function () {

            app = this;

            app.nroColumna = "";
            app.valorCampo = "";
            app.valorTipoDato = "";
            $("#selectCampos").val("").trigger('change.select2')
            $("#selectTipoDato").val("").trigger('change.select2')


        }
    },


    watch: {},

    mounted() {


        this.loadMunicipios();
        this.loadComercializador();


    },

});
