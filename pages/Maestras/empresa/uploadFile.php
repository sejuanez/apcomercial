<?php


//comprobamos que sea una petición ajax
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

    //obtenemos el archivo a subir
    $file1 = $_FILES['foto1']['name'];
    $file2 = $_FILES['foto2']['name'];
    $codMun = $_POST['codMun'];

    $response = array();

    if ($file1 != "") {

        $extension1 = explode(".", $file1);
        $nameFile1 = $codMun . '_I.' . $extension1[1];
        $row['logo1'] = subirLogo($file1, $nameFile1, 'foto1');

    } else {

        $row['logo1'] = null;

    }

    if ($file2 != "") {

        $extension2 = explode(".", $file2);
        $nameFile2 = $codMun . '_D.' . $extension2[1];
        $row2['logo2'] = subirLogo($file2, $nameFile2, 'foto2');

    } else {

        $row2['logo2'] = null;

    }
    array_push($response, $row);
    array_push($response, $row2);

    echo json_encode($response);


} else {
    throw new Exception("Error Processing Request", 1);
}

function subirLogo($file, $nameFile, $foto)
{
    if (!is_dir("../../logosMunicipios/"))
        mkdir("../../logosMunicipios/", 0777);

    // comprobamos si el archivo ha subido
    if ($file && move_uploaded_file($_FILES[$foto]['tmp_name'], "../../logosMunicipios/" . $nameFile)) {
        return $nameFile;
    } else {
        return false;
    }
}





