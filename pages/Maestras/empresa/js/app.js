// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $("#logo2").change(function () {
        readURL2(this);
    });

    $(".selectDepartamentos").change(function (e) {

        var usuario = $(".selectDepartamentos").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onChangeDepartamento(usuario, nombreUsuario);

    });


});

jQuery(document).ready(function ($) {
    $("#logo1").change(function () {
        readURL(this);
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-logo1').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);


    }
}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#img-logo2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            codigo: "",
            nombre: "",
            prefijo: "",
            nit: "",
            dpf: "",
            tif: "",

            nota1: "",
            nota2: "",
            nota3: "",
            encabezado: "",
            dirLogo1: "",
            dirLogo2: "",

            url_logo1: '0',
            url_logo2: '0',

            seleccionado: "",

            busqueda: "",

            codigo_actualizar: "",
            codigo_modal: "",
            nombre_modal: "",

            editor: "",

            selectDepartamentos: [],
            valorDepartamento: "",

            tablaDepartamentos: [],

            noResultados: false,

            btnCancelar: false,
            btnActualizar: false,
            btnGuardar: false,
        },


        methods: {

            loadAnio: function () {


                var app = this;
                $.post('./request/getAno.php', {
                    buscar: app.busqueda
                }, function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            loadDepartamentos: function () {
                let app = this;
                $.get('./request/getDepartamentos.php', function (data) {
                    app.selectDepartamentos = JSON.parse(data);
                });
            },

            onChangeDepartamento: function (cod, nombre) {
                let app = this;
                app.valorDepartamento = cod;
            },

            nuevo: function () {
                var app = this;

                app.btnCancelar = true;
                app.btnGuardar = true;
                app.btnActualizar = false;

                app.resetModal()

                $('#addFucionario').modal('show');
                $('#img-logo1').attr('src', './img/user.jpg');
                $('#img-logo2').attr('src', './img/user.jpg');


            },

            showSoporte: function (dir) {

                var today = (new Date().getTime());

                if (dir == "" || dir == null) {
                    $('#imgSoporte').attr('src', './img/user.jpg');
                } else {
                    $('#imgSoporte').attr('src', dir + '?' + today);
                }
                $('#modalSoporte').modal('show');

            },

            creaLogo1: function () {


                let app = this;
                let formData = new FormData($('.formGuardar')[0]);
                formData.append('codMun', app.codigo);

                //hacemos la petición ajax
                $.ajax({
                    url: './uploadFile.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: formData,

                    //necesario para subir archivos via ajax
                    cache: false,
                    contentType: false,
                    processData: false,
                    async: false,
                    //mientras enviamos el archivo
                    beforeSend: function () {
                    },
                    success: function (data) {

                        console.log(data)

                    },
                    //si ha ocurrido un error
                    error: function (FormData) {
                        console.log(data)
                    }
                });

            },

            showLogo1: function (dir) {

                var today = (new Date().getTime());

                if (dir == "" || dir == null) {
                    $('#img-logo1').attr('src', './img/user.jpg');
                } else {
                    $('#img-logo1').attr('src', dir + '?' + today);
                }

            },
            showLogo2: function (dir) {

                var today = (new Date().getTime());

                if (dir == "" || dir == null) {
                    $('#img-logo2').attr('src', './img/user.jpg');
                } else {
                    $('#img-logo2').attr('src', dir + '?' + today);
                }

            },

            borrarLogo: function (opcion) {

                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item  ?",
                    function () {
                        $.post('./request/borraLogo.php', {
                            codigo: app.seleccionado,
                            opcion: opcion,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Logo Eliminado.");

                                if (opcion == 1) {
                                    $('#img-logo1').attr('src', '');
                                } else {
                                    $('#img-logo2').attr('src', '');
                                }

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });

            },

            verDepartamento: function (dato) {

                var app = this;

                app.btnCancelar = true;
                app.btnGuardar = false;
                app.btnActualizar = true;

                app.seleccionado = dato.codigo;

                app.codigo = dato.codigo;
                app.nombre = dato.nombre;
                app.prefijo = dato.pref;
                app.tif = dato.tif;
                app.dpf = dato.dpf;
                app.nit = dato.nit;
                app.valorDepartamento = dato.cod_dep;
                app.nota1 = dato.nota1;
                app.nota2 = dato.nota2;
                app.nota3 = dato.nota3;
                app.encabezado = dato.texto;
                app.dirLogo1 = dato.logo_izq;
                app.dirLogo2 = dato.logo_der;

                let array = app.encabezado.split("/");

                let text = "";

                for (let i = 0; i < array.length; i++) {
                    text += array[i] + "\n";
                }

                $('#encabezado').html(text);

                app.showLogo1(dato.logo_izq);
                app.showLogo2(dato.logo_der);


                $('#addFucionario').modal('show');
            },

            resetModal: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.prefijo = "";
                app.tif = "";
                app.dpf = "";
                app.nit = "";
                app.valorDepartamento = "";
                app.nota1 = "";
                app.nota2 = "";
                app.nota3 = "";
                app.encabezado = "";
                $('#encabezado').html("");

                let $el = $('#logo1');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();

                let $el2 = $('#logo2');
                $el2.wrap('<form>').closest('form').get(0).reset();
                $el2.unwrap();

                $('#addFucionario').modal('hide');

            },

            buscar: function () {
                var app = this;


                if (app.busqueda != "") {
                    $.post('./request/getAno.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;
                        app.busqueda = "";

                    });
                } else {

                    app.loadAnio()

                }

            },


            actualizar: function () {

                let app = this;

                let foto1 = "";
                let foto2 = "";

                try {

                    foto1 = app.getExtension(document.getElementById('logo1').files[0].name);
                    foto2 = app.getExtension(document.getElementById('logo2').files[0].name);

                } catch (e) {

                }


                let encabezado = ($('#encabezado').val().replace(/\n/g, "/"));


                if (app.codigo == "" ||
                    app.nombre == "" ||
                    app.prefijo == "" ||
                    app.tif == "" ||
                    app.dpf == "" ||
                    app.nit == "" ||
                    app.valorDepartamento == "" ||
                    app.nota1 == "" ||
                    app.nota2 == "" ||
                    app.nota3 == "" ||
                    encabezado == ""
                ) {
                    $('.formGuardar').addClass('was-validated');

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: app.codigo,
                            nombre: app.nombre,
                            prefijo: app.prefijo,
                            tif: app.tif,
                            dpf: app.dpf,
                            nit: app.nit,
                            valorDepartamento: app.valorDepartamento,
                            nota1: app.nota1,
                            nota2: app.nota2,
                            nota3: app.nota3,
                            editor: encabezado,
                            ext1: foto1,
                            ext2: foto2,
                            dir1: app.dirLogo1,
                            dir2: app.dirLogo2,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");
                            app.creaLogo1();
                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codigo: dato.codigo,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            getExtension: function (filename) {
                return filename.slice((filename.lastIndexOf(".") - 1 >>> 0) + 2);
            },


            guardar: function () {

                let app = this;

                let foto1 = "";
                let foto2 = "";

                try {

                    foto1 = app.getExtension(document.getElementById('logo1').files[0].name);
                    foto2 = app.getExtension(document.getElementById('logo2').files[0].name);

                } catch (e) {

                }

                console.log(foto1)

                let encabezado = ($('#encabezado').val().replace(/\n/g, "/"));


                if (app.codigo == "" ||
                    app.nombre == "" ||
                    app.prefijo == "" ||
                    app.tif == "" ||
                    app.dpf == "" ||
                    app.nit == "" ||
                    app.valorDepartamento == "" ||
                    app.nota1 == "" ||
                    app.nota2 == "" ||
                    app.nota3 == "" ||
                    encabezado == ""
                ) {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: app.codigo,
                            nombre: app.nombre,
                            prefijo: app.prefijo,
                            tif: app.tif,
                            dpf: app.dpf,
                            nit: app.nit,
                            valorDepartamento: app.valorDepartamento,
                            nota1: app.nota1,
                            nota2: app.nota2,
                            nota3: app.nota3,
                            editor: encabezado,
                            logo1: foto1,
                            logo2: foto2,
                        },

                    }).done(function (data) {

                            console.log(data)

                            if (data == 1) {
                                app.creaLogo1();
                                alertify.success("Registro guardado Exitosamente");
                                app.resetModal();
                            } else {
                                if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                    alertify.warning("Este codigo ya se encuentra registrado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }

                        }
                    ).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.prefijo = "";
                app.tif = "";
                app.dpf = "";
                app.nit = "";
                app.valorDepartamento = "";
                app.nota1 = "";
                app.nota2 = "";
                app.nota3 = "";
                app.encabezado = "";

                $('#encabezado').html(app.encabezado);

                $('#addFucionario').modal('hide');
            }
        },


        watch: {}
        ,

        mounted() {

            this.loadDepartamentos();
            this.loadAnio();


        }
        ,

    })
;

