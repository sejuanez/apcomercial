<?php

define('MAX_SEGMENT_SIZE', 65535);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");


$codigo = utf8_decode($_POST['codigo']);
$nombre = utf8_decode($_POST['nombre']);
$prefijo = utf8_decode($_POST['prefijo']);
$tif = utf8_decode($_POST['tif']);
$dpf = utf8_decode($_POST['dpf']);
$nit = utf8_decode($_POST['nit']);
$valorDepartamento = utf8_decode($_POST['valorDepartamento']);
$nota1 = utf8_decode($_POST['nota1']);
$nota2 = utf8_decode($_POST['nota2']);
$nota3 = utf8_decode($_POST['nota3']);
$editor = utf8_decode($_POST['editor']);
$logo1 = utf8_decode($_POST['logo1']);
$logo2 = utf8_decode($_POST['logo2']);

if ($logo1 != "") {
    $logo1 = '../../logosMunicipios/' . $codigo . '_I.' . $logo1;
} else {
    $logo1 = null;
}
if ($logo2 != "") {
    $logo2 = '../../logosMunicipios/' . $codigo . '_D.' . $logo2;
} else {
    $logo2 = null;
}

$stmt = "EXECUTE PROCEDURE EMP_CREA('" . $codigo . " ',
                                    '" . $nombre . "',
                                    '" . $nit . "',
                                    '" . $prefijo . "',
                                    '" . $dpf . "',
                                    '" . $tif . "',
                                    '" . $valorDepartamento . "',
                                    '" . $nota1 . "',
                                    '" . $nota2 . "',
                                    '" . $nota3 . "',
                                    '" . $editor . "',                                  
                                     null,
                                    '" . $logo1 . "',                                  
                                    '" . $logo2 . "'                                  
                                     )";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


echo $result;
