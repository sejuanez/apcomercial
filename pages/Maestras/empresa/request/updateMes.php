<?php


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");


$codigo = utf8_decode($_POST['codigo']);
$nombre = utf8_decode($_POST['nombre']);
$prefijo = utf8_decode($_POST['prefijo']);
$tif = utf8_decode($_POST['tif']);
$dpf = utf8_decode($_POST['dpf']);
$nit = utf8_decode($_POST['nit']);
$valorDepartamento = utf8_decode($_POST['valorDepartamento']);
$nota1 = utf8_decode($_POST['nota1']);
$nota2 = utf8_decode($_POST['nota2']);
$nota3 = utf8_decode($_POST['nota3']);
$editor = utf8_decode($_POST['editor']);
$ext1 = utf8_decode($_POST['ext1']);
$ext2 = utf8_decode($_POST['ext2']);
$dir1 = utf8_decode($_POST['dir1']);
$dir2 = utf8_decode($_POST['dir2']);

/*
echo $ext1;
echo '<br>';
echo $ext2;
echo '<br>';
*/

if ($dir1 == "" && $ext1 == "") {
    $ext1 = "";
} else if ($dir1 != "" && $ext1 == "") {
    $ext1 = $dir1;
} else if ($dir1 == "" && $ext1 != "") {
    $ext1 = '../../logosMunicipios/' . $codigo . "_I." . $ext1;
} else if ($dir1 != "" && $ext1 != "") {
    $ext1 = '../../logosMunicipios/' . $codigo . "_I." . $ext1;
}

if ($dir2 == "" && $ext2 == "") {
    $ext2 = "";
} else if ($dir2 != "" && $ext2 == "") {
    $ext2 = $dir2;
} else if ($dir2 == "" && $ext2 != "") {
    $ext2 = '../../logosMunicipios/' . $codigo . "_D." . $ext2;
} else if ($dir2 != "" && $ext2 != "") {
    $ext2 = '../../logosMunicipios/' . $codigo . "_D." . $ext2;
}

/*
echo $ext1 . '<br>';
echo $ext2;
echo '<br>';
*/

$stmt = "EXECUTE PROCEDURE EMP_MODIF('" . $codigo . " ',
                                    '" . $nombre . "',
                                    '" . $nit . "',
                                    '" . $prefijo . "',
                                    '" . $dpf . "',
                                    '" . $tif . "',
                                    '" . $valorDepartamento . "',
                                    '" . $nota1 . "',
                                    '" . $nota2 . "',
                                    '" . $nota3 . "',
                                    '" . $editor . "',                                  
                                     null,
                                    '" . $ext1 . "',                                  
                                    '" . $ext2 . "'                                  
                                     )";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


echo $result;

