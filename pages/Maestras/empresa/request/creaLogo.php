<?php


define('MAX_SEGMENT_SIZE', 65535);


function blob_create($data)
{
    if (strlen($data) == 0)
        return false;
    $handle = ibase_blob_create();
    $len = strlen($data);
    for ($pos = 0; $pos < $len; $pos += MAX_SEGMENT_SIZE) {
        $buflen = ($pos + MAX_SEGMENT_SIZE > $len) ? ($len - $pos) : MAX_SEGMENT_SIZE;
        $buf = substr($data, $pos, $buflen);
        ibase_blob_add($handle, $buf);
    }
    return ibase_blob_close($handle);
}

include("../../../../init/gestion.php");
// include("gestion.php");

$codigo = $_POST['codigo'];
$url = $_POST['url'];


if ($url == '0') {
    $foto = blob_create(file_get_contents("../img/user.png"));
} else {
    $foto = blob_create(file_get_contents("../temp/" . $url));
}


$stmt = "EXECUTE PROCEDURE EMP_CREAR_LOGOIZQ(
                                              '" . $codigo . "',
                                              '" . $foto . "',                                                  
                                              null)";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


echo $result;

