<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">


    <style type="text/css">

        .content {
            display: flex;
            align-items: center;
            flex-wrap: wrap;
        }

        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 74vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Municipios


            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="nuevo();">
					<i class="fa fa-save" aria-hidden="true"></i> Nuevo
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-3"></div>

            <div class="col-12 col-sm-6">

                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm"><input type="text"
                                                                               placeholder="Filtrar por codigo nombre"
                                                                               v-model="busqueda"
                                                                               class="form-control"> <span
                                class="input-group-btn"><button type="button"
                                                                v-on:click="buscar()"
                                                                class="btn btn-secondary">Buscar</button></span>
                    </div>
                </div>

            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">


                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th>Codigo</th>
                        <th>Nombre</th>
                        <th width="150">NIT</th>
                        <th width="100">TIF</th>
                        <th width="100">Factura</th>
                        <th>Departamento</th>
                        <th width="100">Logo Izq</th>
                        <th width="100">Logo Der</th>
                        <th style=" text-align: center;
                        " width="100">Acción
                        </th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaDepartamentos.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaDepartamentos">
                        <td class="text-center" v-text="dato.codigo"></td>
                        <td class="text-center" v-text="dato.nombre"></td>
                        <td class="text-center" v-text="dato.nit" width="150"></td>
                        <td class="text-center" v-text="dato.tif" width="100"></td>
                        <td class="text-center" v-text="dato.nro_fac" width="100"></td>
                        <td class="text-center" v-text="dato.nom_dep"></td>
                        <td style="text-align: center;" width="100">
                            <i class="fa fa-file-picture-o" style="cursor: pointer; font-size: 1.5em"
                               @click="showSoporte(dato.logo_izq)"></i>
                        </td>
                        <td style="text-align: center;" width="100">
                            <i class="fa fa-file-picture-o" style="cursor: pointer; font-size: 1.5em"
                               @click="showSoporte(dato.logo_der)"></i>
                        </td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verDepartamento(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>

    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Municipios
                        <small>( Los campos con <code>*</code> son obligatorios )</small>
                    </h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">


                        <div class="row">

                            <div class="container">

                                <br>
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="tab-principal" data-toggle="tab" href="#home">
                                            Datos de la Empresa
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu1">Notas</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu2">Logos</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#menu3">Texto Encabezado</a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">

                                    <div id="home" class="container tab-pane active"><br>

                                        <div class="row">

                                            <div class="col-3">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_direccion">Codigo<code>*</code></label>
                                                    <input type="text" id="propietario_direccion"
                                                           name="propietario_direccion" class="form-control" required
                                                           aria-label="propietario_direccion"
                                                           v-model="codigo" maxlength="40">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_telefono">Nombre<code>*</code></label>
                                                    <input type="text" id="propietario_telefono"
                                                           name="propietario_telefono"
                                                           autocomplete="nope"
                                                           class="form-control" required
                                                           aria-label="propietario_telefono"
                                                           v-model="nombre" maxlength="15">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>

                                            <div class="col-3">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_direccion">Prefijo<code>*</code></label>
                                                    <input type="text" id="propietario_direccion"
                                                           name="propietario_direccion" class="form-control" required
                                                           aria-label="propietario_direccion"
                                                           v-model="prefijo" maxlength="40">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_direccion">Dias de
                                                        plazo<code>*</code></label>
                                                    <input type="text" id="propietario_direccion"
                                                           name="propietario_direccion" class="form-control" required
                                                           aria-label="propietario_direccion"
                                                           v-model="dpf" maxlength="40">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>


                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_direccion">Tasa de interes
                                                        factura<code>*</code></label>
                                                    <input type="text" id="propietario_direccion"
                                                           name="propietario_direccion" class="form-control" required
                                                           aria-label="propietario_direccion"
                                                           v-model="tif" maxlength="40">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="form-group input-group-sm">
                                                    <label for="propietario_direccion">NIT<code>*</code></label>
                                                    <input type="text" id="propietario_direccion"
                                                           name="propietario_direccion" class="form-control" required
                                                           aria-label="propietario_direccion"
                                                           v-model="nit" maxlength="40">
                                                    <div class="invalid-feedback">Este campo es requerido</div>

                                                </div>
                                            </div>

                                            <div class="col-12 col-sm-6">
                                                <div class="form-group input-group-sm">

                                                    <label for="propietario_direccion">Departamento<code>*</code></label><br>
                                                    <select class="selectDepartamentos"
                                                            id="selectDepartamentos"
                                                            name="selectDepartamentos"
                                                            v-model="valorDepartamento">
                                                        <option value="">Departamento...</option>
                                                        <option v-for="selectDepartamento in selectDepartamentos"
                                                                :value="selectDepartamento.codigo">
                                                            {{selectDepartamento.nombre}}
                                                        </option>
                                                    </select>

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="menu1" class="container tab-pane fade"><br>

                                        <div class="form-group">
                                            <label for="comment">Nota 1:</label>
                                            <textarea class="form-control" rows="3" v-model="nota1"
                                                      id="comment"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="comment">Nota 2:</label>
                                            <textarea class="form-control" rows="3" v-model="nota2"
                                                      id="comment"></textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="comment">Nota 3:</label>
                                            <textarea class="form-control" rows="3" v-model="nota3"
                                                      id="comment"></textarea>
                                        </div>

                                    </div>

                                    <div id="menu2" class="container tab-pane fade"><br>

                                        <div class="row">

                                            <div class="col-2"></div>

                                            <div class="col-6" v-if="true">
                                                <div class="form-group">
                                                    <label for="foto">Logo Izquierdo <code>*</code></label>
                                                    <input type="file" class="" id="logo1"
                                                           accept=" image/jpg, image/jpeg"
                                                           name="foto1" required/>
                                                    <div class="invalid-feedback">Este campo es requerido</div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-3">

                                                    </div>

                                                </div>


                                            </div>
                                            <div class="col-2 content">
                                                <img id="img-logo1" src="img/user.jpg" alt="" width="100%">
                                            </div>


                                        </div>

                                        <br>

                                        <div class="row">

                                            <div class="col-2"></div>

                                            <div class="col-6" v-if="true">
                                                <div class="form-group">
                                                    <label for="foto">Logo Derecho <code>*</code></label>
                                                    <input type="file" class="" id="logo2"
                                                           accept=" image/jpg, image/jpeg"
                                                           name="foto2" required/>
                                                    <div class="invalid-feedback">Este campo es requerido</div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-3">

                                                    </div>


                                                </div>


                                            </div>
                                            <div class="col-2 content">
                                                <img id="img-logo2" src="img/user.jpg" alt="" width="100%">
                                            </div>


                                        </div>


                                    </div>

                                    <div id="menu3" class="container tab-pane fade"><br>

                                        <div class="form-group">

                                            <label for="comment">Encabezado</label>
                                            <textarea class="form-control" rows="6"
                                                      id="encabezado"></textarea>
                                        </div>

                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="btnCancelar" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="btnActualizar" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>

                        <button type="button" v-if="btnGuardar" class="btn btn-primary btn-sm" @click="guardar()">
                            Guardar
                        </button>

                    </div>

                </form>

            </div>


        </div>
    </div>

    <div id="modalSoporte" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Soporte</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <img id="imgSoporte" style="max-height: 60vh; max-width: 80vh">

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>


            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src='js/tinymce/tinymce.min.js'></script>
<script src='js/tinymce/jquery.tinymce.min.js'></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
