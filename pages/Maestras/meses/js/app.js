// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            codigo: "",
            nombre: "",

            busqueda: "",

            codigo_actualizar: "",
            codigo_modal: "",
            nombre_modal: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadMeses: function () {


                var app = this;
                $.get('./request/getMeses.php', function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            verDepartamento: function (dato) {

                this.codigo_actualizar = dato.codigo;
                this.codigo_modal = dato.codigo;
                this.nombre_modal = dato.nombre;


                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getMeses2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadMeses()

                }

            },

            actualizar: function () {

                if (this.codigo_modal == "" ||
                    this.nombre_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;

                    console.log(app.nombre_modal)
                    console.log(app.codigo_modal)

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: app.codigo_actualizar,
                            codigo_nuevo: app.codigo_modal,
                            nombre_nuevo: app.nombre_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codigo: dato.codigo,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                alertify.error("Error al eliminar");
                                app.buscar();
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (this.codigo == "" ||
                    this.nombre == "") {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo,
                            nombre: this.nombre
                        },

                    }).done(function (data) {

                        console.log(data)

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadMeses();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadMeses();


        }
        ,

    })
;
