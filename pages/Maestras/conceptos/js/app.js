// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            codigo: "",
            nombre: "",
            modo: "",
            orden: "",
            editable: "",
            interes: "",

            busqueda: "",

            codigo_actualizar: "",
            codigo_modal: "",
            nombre_modal: "",
            modo_modal: "",
            orden_modal: "",
            editable_modal: "",
            interes_modal: "",


            tablaConcepto: [],
            noResultados: false,

        },


        methods: {
            loadConcepto: function () {


                var app = this;
                $.get('./request/getConcepto.php', function (data) {
                    console.log(data);
                    app.tablaConcepto = JSON.parse(data);
                });
            },


            verConcepto: function (dato) {

                this.codigo_actualizar = dato.codigo;
                this.codigo_modal = dato.codigo;
                this.nombre_modal = dato.nombre;
                this.modo_modal = (dato.modo == "Suma" ? "1" : "0");
                this.orden_modal = dato.orden;
                this.editable_modal = (dato.editable == "Si" ? "1" : "0");
                this.interes_modal = (dato.interes == "Si" ? "1" : "0");

                $('#addConcepto').modal('show');
            },

            resetModal: function () {

                $('#addConcepto').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";
                this.modo_modal = "";
                this.orden_modal = "";
                this.editable_modal = "";
                this.interes_modal = "";
            },


            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getConcepto.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data);
                        var datos = jQuery.parseJSON(data);
                        app.tablaConcepto = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadConcepto()

                }

            },


            actualizar: function () {

                if (this.codigo_modal == "" ||
                    this.nombre_modal == "" ||
                    this.modo_modal == "" ||
                    this.orden_modal == "" ||
                    this.editable_modal == "" ||
                    this.interes_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;
                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateConcepto.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: app.codigo_actualizar.toUpperCase(),
                            codigo_nuevo: app.codigo_modal.toUpperCase(),
                            nombre_nuevo: app.nombre_modal,
                            modo_nuevo: app.modo_modal,
                            orden_nuevo: app.orden_modal,
                            editable_nuevo: app.editable_modal,
                            interes_nuevo: app.interes_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {

                                alertify.warning("Ya existe un concepto en este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el Concepto " + dato.codigo + " ?",
                    function () {
                        $.post('./request/eliminarConcepto.php', {
                            codigo: dato.codigo,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                alertify.error("Error al eliminar");
                                app.buscar();
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (this.codigo == "" ||
                    this.nombre == "") {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertConcepto.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo.toUpperCase(),
                            nombre: this.nombre.toUpperCase(),
                            modo: this.modo,
                            orden: this.orden,
                            editable: this.editable,
                            interes: this.interes
                        },

                    }).done(function (data) {

                        console.log(data)

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadConcepto();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este consepto ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.modo = "";
                app.orden = "";
                app.editable = "";
                app.interes = "";

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadConcepto();

        }
        ,

    })
;
