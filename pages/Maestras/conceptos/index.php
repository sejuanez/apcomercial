﻿<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Conceptos</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera td {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 60vh;
            overflow-y: scroll
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>


</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Conceptos
            <!--
            <span id="btnBuscar" v-show="btnAnadir" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Nuevo
		    	</span>
              -->
        </p>
    </header>

    <div class="container" style="padding-bottom: 5px">


        <div class="row">

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <input type="text" id="codigo" class="form-control" aria-label="codigo" placeholder="Codigo"
                               v-model="codigo">
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <input type="text" id="nombre" class="form-control" aria-label="nombre" placeholder="Nombre"
                               v-model="nombre">
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <select class="form-control" id="select-modo" aria-label="modo" v-model="modo">
                            <option value="" disabled>Modo</option>
                            <option value="1">Suma</option>
                            <option value="0">Resta</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <input type="number" id="orden" class="form-control" aria-label="orden" placeholder="Orden"
                               v-model="orden">
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <select class="form-control" id="select-editable" aria-label="editable" v-model="editable">
                            <option value="" disabled>Editable</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <select class="form-control" id="select-interes" aria-label="interes" v-model="interes">
                            <option value="" disabled>Generar Intereses</option>
                            <option value="1">Si</option>
                            <option value="0">No</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group">
                    <div class="col-sm-2 ">
                        <button type="button"
                                v-on:click="guardar()"
                                class="btn btn-secondary btn-label">
                            Guardar
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-4">

                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm">
                        <input type="text" placeholder="Filtrar por codigo nombre" v-model="busqueda"
                               class="form-control">
                        <span class="input-group-btn">
                            <button type="button" v-on:click="buscar()" class="btn btn-secondary">Buscar</button>
                        </span>
                    </div>
                </div>

            </div>

        </div>


    </div>


    <div>

        <div class="row">
            <div class="col-12">
                <table class="table table-sm">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <td>Codigo</td>
                        <td width="500">Nombre</td>
                        <td>Modo</td>

                        <td>Generar Interes</td>
                        <td style="text-align: center;" width="100">Acción</td>
                    </tr>
                    </thead>
                    <tbody class="scroll" id="detalle_gastos">
                    <tr v-if="tablaConcepto.length == 0">
                        <td class="text-center fondoGris" colspan="7">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaConcepto">
                        <td v-text="dato.codigo" class="text-center"></td>
                        <td width="500" v-text="dato.nombre"></td>
                        <td v-text="dato.modo" class="text-center"></td>
                        <td v-text="dato.interes" class="text-center"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verConcepto(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>

    <div id="addConcepto" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Concepto
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <form>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="codigo_modal">Codigo </label>
                                        <input type="text" id="descripcion" name="codigo_modal"
                                               class="form-control" required aria-label="codigo_modal"
                                               v-model="codigo_modal">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="nombre_modal">Nombre </label>
                                        <input type="text" id="descripcion" name="nombre_modal"
                                               class="form-control" required aria-label="nombre_modal"
                                               v-model="nombre_modal">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="nombre_modal">Modo</label>
                                        <select class="form-control" id="select-modo" aria-label="nombre_modal"
                                                v-model="modo_modal">
                                            <option value="" disabled></option>
                                            <option value="1">Suma</option>
                                            <option value="0">Resta</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group input-group-sm">
                                        <label for="interes_modal">Generar Interes </label>
                                        <select class="form-control" id="select-interes" aria-label="interes"
                                                v-model="interes_modal">
                                            <option value="" disabled></option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                </div>

                            </form>

                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
