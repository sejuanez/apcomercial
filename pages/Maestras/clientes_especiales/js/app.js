// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipios").select2().change(function (e) {

        var usuario = $(".selectMunicipios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios(usuario, nombreUsuario);
        app.loadTarifa();
        app.loadZona();
        app.resetCampos();
    });
    $(".selectTarifa").select2().change(function (e) {

        var usuario = $(".selectTarifa").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeTarifa(usuario, nombreUsuario);

    });
    $(".selectZona").select2().change(function (e) {

        var usuario = $(".selectZona").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeZona(usuario, nombreUsuario);

    });

    $(".selectMunicipios_modal").change(function (e) {

        var usuario = $(".selectMunicipios_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeMunicipios_modal(usuario, nombreUsuario);
        app.loadTarifa_modal();
        app.loadZona_modal();
        app.resetCampos();
    });

    $(".selectTarifa_modal").change(function (e) {
        var usuario = $(".selectTarifa_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeTarifa_modal(usuario, nombreUsuario);

    });

    $(".selectZona_modal").change(function (e) {
        var usuario = $(".selectZona_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeZona_modal(usuario, nombreUsuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,
        codigo: "",

        tEspecial: "",
        tEspecial_modal: "",

        codigo_actualizar: "",
        nombre_modal: "",
        codigo_modal: "",


        selectMunicipios: [],
        selectMunicipios_modal: [],
        valorMunicipios: "",
        valorMunicipios_modal: "",
        selectTarifa: [],
        selectTarifa_modal: [],
        valorTarifa: "",
        valorTarifa_modal: "",
        selectZona: [],
        selectZona_modal: [],
        valorZona: "",
        valorZona_modal: "",
        busqueda: "",
        codigoActualizar: "",

        tablaClientes: [],
        tablaDepartamentos: [],


        noResultados:
            false,


    },


    methods: {

        loadClientes: function () {
            var app = this;

            $.post('./request/getClientesEspeciales.php', {
                mun: app.valorMunicipios,
                buscar: "",
            }, function (data) {
                console.log(data)
                app.tablaClientes = JSON.parse(data);
            });
        },
        loadMunicipios: function () {
            var app = this;
            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);


            });
        },
        loadTarifa: function () {
            var app = this;
            $.post('./request/getTarifa.php', {
                selectMunicipios: app.valorMunicipios,
            }, function (data) {
                app.selectTarifa = JSON.parse(data);

            });
        },
        loadZona: function () {
            var app = this;

            $.post('./request/getZona.php', {

                    selectMunicipios: app.valorMunicipios
                },
                function (data) {

                    app.selectZona = JSON.parse(data);

                });
        },
        onchangeMunicipios: function (cod, nombre) {


            var app = this;

            app.valorMunicipios = cod;
        },
        onchangeTarifa: function (cod, nombre) {
            var app = this;

            app.valorTarifa = cod;
        },
        onchangeZona: function (cod, nombre) {

            var app = this;
            app.valorZona = cod;
        },
        loadMunicipios_modal: function () {
            var app = this;
            $.post('./request/getMunicipios_modal.php', function (data) {
                app.selectMunicipios_modal = JSON.parse(data);
            });
        },
        loadTarifa_modal: function () {

            let app = this;

            $.ajax({
                url: './request/getTarifa_modal.php',
                type: 'POST',
                async: false,
                data: {
                    valorMunicipios_modal: app.valorMunicipios_modal,
                },

            }).done(function (response) {
                app.selectTarifa_modal = JSON.parse(response);

                //return response;
            }).fail(function (error) {

                console.log(error)

            });


        },
        loadZona_modal: function () {
            let app = this;

            $.ajax({
                url: './request/getZona_modal.php',
                type: 'POST',
                async: false,
                data: {
                    selectMunicipios_modal: app.valorMunicipios_modal
                },

            }).done(function (response) {
                app.selectZona_modal = JSON.parse(response);

                //return response;
            }).fail(function (error) {

                console.log(error)

            });
        },
        onchangeMunicipios_modal: function (cod, nombre) {

            var app = this;

            app.valorMunicipios_modal = cod;
        },
        onchangeTarifa_modal: function (cod, nombre) {
            var app = this;

            app.valorTarifa_modal = cod;
        },
        onchangeZona_modal: function (cod, nombre) {

            var app = this;
            app.valorZona_modal = cod;
        },
        verClientes: function (dato) {

            let app = this;

            app.codigo_modal = dato.COD_CLIENTE;

            dato.TARIFAESPEC == 1 ? app.tEspecial_modal = true : app.tEspecial_modal = false;


            app.valorMunicipios_modal = dato.COD_EMPRESA;


            app.loadTarifa_modal();
            $("#selectTarifa_modal").val(app.valorTarifa_modal).trigger('change')
            app.valorTarifa_modal = dato.COD_TARIFA;

            app.loadZona_modal();
            $("#selectZona_modal").val(app.valorZona_modal).trigger('change')
            app.valorZona_modal = dato.COD_ZONA;


            $('#addFucionario').modal('show');
        },
        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
        },
        buscar: function () {
            let app = this;

            if (app.busqueda != "") {
                $.post('./request/getClientesEspeciales.php', {
                    buscar: app.busqueda.toUpperCase(),
                    mun: app.valorMunicipios
                }, function (data) {
                    console.log(data)

                    app.tablaClientes = jQuery.parseJSON(data);

                    app.busqueda = "";

                });
            } else {

                app.loadClientes()

            }

        },
        actualizar: function () {

            if (this.valorMunicipios_modal == "" ||
                this.codigo_modal == "" ||
                this.valorTarifa_modal == "" ||
                this.valorZona_modal == "") {
                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {
                var app = this;
                var valor;

                if (app.tEspecial_modal == true) {
                    valor = 1;
                } else {
                    valor = 0;
                }


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateClientes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        valorMunicipios_modal: this.valorMunicipios_modal,
                        codigo_modal: this.codigo_modal,
                        valorTarifa_modal: this.valorTarifa_modal,
                        valorZona_modal: this.valorZona_modal,
                        tEspecial_modal: valor,


                    },

                }).done(function (data) {
                    console.log(data)

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.resetModal();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {

                });

            }


        },
        eliminar: function (dato) {
            console.log(dato)
            let app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el cliente especial " + dato.COD_CLIENTE + "?",
                function () {
                    $.post('./request/deleteClientes.php', {
                        codigo: dato.COD_CLIENTE,
                        valorMunicipios: dato.COD_EMPRESA,
                    }, function (data) {

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");

                            app.resetModal();
                            app.buscar();


                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {
                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");
                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },
        guardar: function () {

            let app = this;

            if (app.valorMunicipios == "" ||
                app.codigo == "" ||
                app.valorTarifa == "" ||
                app.valorZona == "") {
                alertify.error("Por favor ingresa todos los campos");
            } else {

                let valor;

                if (app.tEspecial == true) {
                    valor = 1;
                } else {
                    valor = 0;
                }
                $.ajax({
                    url: './request/insertClientes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        valorMunicipios: app.valorMunicipios,
                        codigo: app.codigo,
                        valorTarifa: app.valorTarifa,
                        valorZona: app.valorZona,
                        tEspecial: valor
                    },


                }).done(function (data) {

                    console.log(data)
                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadClientes();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },
        resetCampos: function (op) {

            app = this;

            if (op == 1) {
                app.valorMunicipios = "";
                $("#selectMunicipios").val("").trigger('change')
            }

            $("#selectZona").val("").trigger('change')
            app.valorZona = "";
            $("#selectTarifa").val("").trigger('change')
            app.valorTarifa = "";


        }
    },


    watch:
        {},

    mounted() {


        this.loadMunicipios();


    },

});
