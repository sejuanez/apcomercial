﻿<?php
// Web Service

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            		window.location.href='../index.php';
        	</script>";
    exit();
}

include("../../../init/gestion.php");


$output = (isset($_REQUEST['o'])) ? strtolower($_REQUEST['o']) : 'xml';

//$usuarios=Database::execQuery("SELECT * FROM empresa;");

$stmt = "SELECT * FROM empresa";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();

if ($output == 'json') {

    header('Content-type: application/json');
    while ($fila = ibase_fetch_row($result)) {
        $row_array['codigo'] = utf8_encode($fila[0]);
        $row_array['nombre'] = utf8_encode($fila[1]);
        $row_array['prefijo'] = utf8_encode($fila[2]);
        $row_array['nit'] = utf8_encode($fila[5]);
        array_push($return_arr, $row_array);
    }

// $array = array("result"=>$return_arr);

    echo json_encode($return_arr);

}