// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectActividad").select2().change(function (e) {

        var usuario = $(".selectActividad").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onchangeActividad(usuario, nombreUsuario);

    });

    $(".selectActividad2").change(function (e) {

        var usuario = $(".selectActividad2").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            codigo: "",
            numero: "",
            fecha: "",
            activo: "",

            valorActividad: "",
            selectActividades: [],

            busqueda: "",

            codigo_actualizar: "",
            codigo_mun_actualizar: "",
            numero_modal: "",
            fecha_modal: "",
            activo_modal: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadAnio: function () {


                var app = this;
                $.get('./request/getAno.php', function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            loadClaseActividades: function () {
                var app = this;
                $.get('./request/getClaseAcividades.php', function (data) {
                    app.selectActividades = JSON.parse(data);
                });
            },

            onchangeActividad: function (cod, nombre) {
                var app = this;
                app.valorActividad = cod;
                console.log(cod)
            },

            verDepartamento: function (dato) {

                this.codigo_actualizar = dato.codigo;
                this.codigo_mun_actualizar = dato.codigo_mun;

                this.numero_modal = dato.numero;
                this.fecha_modal = dato.fecha;
                this.activo_modal = dato.activo;

                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadAnio()

                }

            },

            actualizar: function () {

                if (this.numero_modal == "" ||
                    this.fecha_modal == "" ||
                    this.activo_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: app.codigo_actualizar,
                            codigo_mun: app.codigo_mun_actualizar,
                            numero: app.numero_modal,
                            fecha: app.fecha_modal,
                            activo: app.activo_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar la actividad " + dato.codigo + " del municipio " + dato.codigo_mun + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codigo_mun: dato.codigo_mun,
                            codigo: dato.codigo,
                        }, function (data) {

                            console.log(data)

                            app.valorActividad = "";

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {

                console.log(this.valorActividad)
                console.log(this.activo)

                if (this.codigo == "" ||
                    this.nombre == "" ||
                    this.fecha == "" ||
                    this.activo == "" ||
                    $(".selectActividad").val() == null ||
                    $(".selectActividad").val() == ""

                ) {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codigo: this.codigo,
                            numero: this.numero,
                            fecha: this.fecha,
                            activo: this.activo,
                            codigo_mun: $(".selectActividad").val(),
                        },

                    }).done(function (data) {

                        console.log(data)


                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadAnio();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.numero = "";
                app.fecha = "";
                app.activo = "";
                app.valorActividad = "";
                $("#selectActividad").val("").trigger('change')


            }
        },


        watch: {}
        ,

        mounted() {

            this.loadAnio();
            this.loadClaseActividades();


        }
        ,

    })
;
