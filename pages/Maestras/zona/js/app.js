// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectActividad").select2().change(function (e) {

        var usuario = $(".selectActividad").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onchangeActividad(usuario, nombreUsuario);

    });

    $(".selectActividad2").change(function (e) {

        var usuario = $(".selectActividad2").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        codigo: "",
        nombre: "",

        valorActividad: "",
        selectActividades: [],

        busqueda: "",

        codigo_actualizar: "",
        codigo_modal: "",
        nombre_modal: "",


        tablaDepartamentos: [],

        noResultados: false,

    },


    methods: {

        loadAnio: function () {


            var app = this;
            $.get('./request/getAno.php', function (data) {
                app.tablaDepartamentos = JSON.parse(data);
            });
        },

        loadClaseActividades: function () {
            var app = this;
            $.get('./request/getClaseAcividades.php', function (data) {
                app.selectActividades = JSON.parse(data);
            });
        },

        onchangeActividad: function (cod, nombre) {
            var app = this;
            console.log(cod)
            app.valorActividad = cod;
        },

        verDepartamento: function (dato) {

            this.codigo_actualizar = dato.codigo;
            this.codigo_modal = dato.codigo;
            this.nombre_modal = dato.nombre;
            this.valorActividad = dato.empresa;

            $('#addFucionario').modal('show');
        },

        resetModal: function () {
            $('#addFucionario').modal('hide');

            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        buscar: function () {
            var app = this;

            if (app.busqueda != "") {
                $.post('./request/getAno2.php', {
                    buscar: app.busqueda.toUpperCase()
                }, function (data) {
                    console.log(data)
                    var datos = jQuery.parseJSON(data);
                    app.tablaDepartamentos = datos;

                    app.busqueda = "";

                });
            } else {

                app.loadAnio()

            }

        },

        actualizar: function () {

            if (this.valorActividad == "" ||
                this.codigo_modal == "" ||
                this.nombre_modal == "") {

                $('.formGuardar').addClass('was-validated');
                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        empresa: app.valorActividad,
                        codigo: app.codigo_modal,
                        nombre_nuevo: app.nombre_modal,
                    },

                }).done(function (data) {


                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.resetModal();
                        app.buscar();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {
                    console.log(data)
                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " de la empresa " + dato.empresa + " ?",
                function () {
                    $.post('./request/deleteMes.php', {
                        codigo: dato.codigo,
                        empresa: dato.empresa,
                    }, function (data) {

                        app.valorActividad = "";

                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {


                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {

            console.log($(".selectActividad").val())

            if (this.codigo == "" ||
                this.nombre == "" ||
                $(".selectActividad").val() == null) {

                alertify.error("Por favor ingresa todos los campos");

            } else {

                var app = this;


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertMes.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        codigo: this.codigo,
                        nombre: this.nombre,
                        codigo_clas: $(".selectActividad").val(),
                    },

                }).done(function (data) {

                    console.log(data)


                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadAnio();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {
                    console.log(data)
                });


            }
        },

        resetCampos: function () {

            app = this;

            app.codigo = "";
            app.nombre = "";
            app.valorActividad = "";


        }
    },


    watch: {},

    mounted() {

        this.loadAnio();
        this.loadClaseActividades();


    },

});
