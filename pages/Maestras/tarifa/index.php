<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">

    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 69vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Tarifa

            <span class="float-right" v-if="valorMunicipio!=''"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="nuevo()">
					<i class="fa fa-save" aria-hidden="true"></i> Nuevo
		    	</span>
        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row" style="padding-bottom: 5px;">
            <div class="col-sm-4"></div>
            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMunicipio">
                        <option value="">Municipio...</option>
                        <option v-for="item in selectMunicipios"
                                :value="item.codigo">
                            {{item.nombre}}
                        </option>
                    </select>
                </div>
            </div>
        </div>


        <div class="row" style="padding-bottom: 5px;">
            <div class="col-sm-4"></div>
            <div class="col-12 col-sm-4">
                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm"><input type="text"
                                                                               placeholder="Filtrar por codigo nombre"
                                                                               v-model="busqueda"
                                                                               class="form-control"> <span
                                class="input-group-btn"><button type="button"
                                                                v-on:click="loadTarifas()"
                                                                class="btn btn-secondary">Buscar</button></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container" style="max-width: 100%;">
        <div class="row">
            <div class="col-12 my-tbody">
                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="100">Municipio</th>
                        <th width="50">Código</th>
                        <th>Nombre</th>
                        <th width="100">Vr Min</th>
                        <th width="100">Vr Max</th>
                        <th width="80">U Men</th>
                        <th width="80">U May</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaTarifas.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaTarifas">
                        <td v-text="dato.empresa" width="100"></td>
                        <td v-text="dato.codigo" width="50"></td>
                        <td v-text="dato.nombre"></td>
                        <td v-text="dato.vrMin" width="100"></td>
                        <td v-text="dato.vrMax" width="100"></td>
                        <td v-text="dato.NOM_impCargo" width="80"></td>
                        <td v-text="dato.NOM_impCargoM" width="80"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verTarifa(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addTarifa" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Tarifa
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">


                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="codMun" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           disabled
                                           v-model="codMun">
                                    <label for="codMun">Cod Municipio</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="codTar" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="codTarifa">
                                    <label for="codTar">Cod Tarifa</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6">
                                <div class="form-label-group">
                                    <input type="text" id="nomTarifa" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="nomTarifa">
                                    <label for="nomTarifa">Nombre Tarifa</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="vrMin" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="vrMin">
                                    <label for="vrMin">Vr Min</label>
                                </div>
                            </div>


                            <div class="col-12 col-sm-3">
                                <div class="form-label-group">
                                    <input type="text" id="vrMax" class="form-control"
                                           placeholder="Email address" required=""
                                           autocomplete="nope"
                                           v-model="vrMax">
                                    <label for="vrMax">Vr Max</label>
                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group input-group-sm">

                                    <select class="select1" id="select1" name="selectActividad"
                                            v-model="valorIC">
                                        <option value="">IC...</option>
                                        <option v-for="selectActividad in selectIC"
                                                :value="selectActividad.codigo">
                                            {{selectActividad.nombre}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <div class="col-12 col-sm-3">
                                <div class="form-group input-group-sm">
                                    <select class="select2" id="select2" name="selectActividad"
                                            v-model="valorICM">
                                        <option value="">ICM...</option>
                                        <option v-for="selectActividad in selectIC"
                                                :value="selectActividad.codigo">
                                            {{selectActividad.nombre}}
                                        </option>
                                    </select>

                                </div>
                            </div>


                        </div>


                    </div>

                    <div class="modal-footer">

                        <button type="button" v-if="!(btnModal)" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>

                        <button type="button" v-if="btnModal" class="btn btn-success btn-sm" @click="guardar()">
                            Guardar
                        </button>

                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetCampos()">
                            Cancelar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
