// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipios").select2().change(function (e) {
        let usuario = $(".selectMunicipios").val();
        let nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeDepartamento(usuario, nombreUsuario);
    });


    $(".select-ic").select2().change(function (e) {
        let usuario = $(".select-ic").val();
        let nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeIC(usuario, nombreUsuario);
    });

    $(".select-icm").select2().change(function (e) {
        let usuario = $(".select-icm").val();
        let nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeICM(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        codigo: "",
        nombre: "",
        vrmin: "",
        vrmax: "",
        ic: "",
        icm: "",

        valorMunicipio: "",
        selectMunicipios: [],
        selectIC: [],
        selectICM: [],

        busqueda: "",

        codMun: "",
        codTarifa: "",
        nomTarifa: "",
        vrMin: "",
        vrMax: "",
        valorIC: "",
        valorICM: "",


        tablaTarifas: [],

        btnModal: true,

    },


    methods: {

        nuevo: function () {
            let app = this;
            app.btnModal = true;
            app.codMun = app.valorMunicipio;
            app.resetCampos();

            $("#codTar").prop('disabled', false);
            $('#addTarifa').modal('show');
        },

        loadTarifas: function () {
            let app = this;
            if (app.valorMunicipio === "") {
                app.tablaTarifas = [];
                alertify.warning('Por favor selecciona un municipio')
            } else {

                $.post('./request/getTarifas.php', {
                    mun: app.valorMunicipio,
                    busqueda: app.busqueda.toUpperCase()
                }, function (data) {
                    app.tablaTarifas = JSON.parse(data);
                });
            }
            app.busqueda = "";

        },

        loadDepartamentos: function () {
            let app = this;
            $.get('./request/getMunicipios.php', function (data) {
                app.selectMunicipios = JSON.parse(data);
            });
        },
        onChangeDepartamento: function (cod, nombre) {
            let app = this;
            app.valorMunicipio = cod;
            app.loadTarifas();
        },

        loadIC: function () {
            let app = this;
            $.get('./request/getIC.php', function (data) {
                app.selectIC = JSON.parse(data);
                app.selectICM = JSON.parse(data);
            });
        },

        onchangeIC: function (cod, nombre) {
            let app = this;
            app.ic = cod;
        },

        onchangeICM: function (cod, nombre) {
            let app = this;
            app.icm = cod;
        },

        verTarifa: function (dato) {
            let app = this;
            app.btnModal = false;
            app.codigo_actualizar = dato.codigo;
            app.codMun = dato.empresa;
            app.codTarifa = dato.codigo;
            app.nomTarifa = dato.nombre;
            app.vrMin = dato.vrMin;
            app.vrMax = dato.vrMax;
            app.valorIC = dato.impCargo;
            app.valorICM = dato.impCargoM;

            $("#codTar").prop('disabled', true);
            $('#addTarifa').modal('show');
        },

        resetModal: function () {

            $('#addFucionario').modal('hide');
            this.codigo_modal = "";
            this.nombre_modal = "";
            this.codigo_actualizar = "";

        },

        actualizar: function () {

            let app = this;

            if (app.codMun === "" ||
                app.codTarifa === "" ||
                app.nomTarifa === "" ||
                app.vrMax === "" ||
                app.vrMin === "" ||
                app.valorIC === "" ||
                app.valorICM === "") {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/updateTarifa.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        empresa: app.codMun,
                        codigo: app.codTarifa,
                        nombre: app.nomTarifa,
                        min: app.vrMin,
                        max: app.vrMax,
                        ic: app.valorIC,
                        icm: app.valorICM
                    },

                }).done(function (data) {


                    if (data == 1) {
                        alertify.success("Registro Actualizado Exitosamente");

                        app.buscar();
                        app.resetCampos();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Ya existe un mes con este codigo");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }


                    //si ha ocurrido un error
                }).fail(function (data) {

                });

            }


        },

        eliminar: function (dato) {
            let app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " de la empresa " + dato.empresa + " ?",
                function () {
                    $.post('./request/deleteTarifa.php', {
                        codigo: dato.codigo,
                        empresa: dato.empresa,
                    }, function (data) {


                        if (data == 1) {
                            alertify.success("Registro Eliminado.");
                            app.resetModal();
                            app.loadTarifas();

                        } else {
                            if (data.indexOf("Foreign key references are present for the record") > -1) {


                                alertify.warning("Este item esta siendo utilizado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        guardar: function () {
            let app = this;

            if (app.codTarifa === "" ||
                app.nomTarifa === "" ||
                app.vrMin === "" ||
                app.vrMax === "" ||
                app.valorIC === "" ||
                app.valorICM === "" ||
                app.valorMunicipio === "") {

                alertify.error("Por favor ingresa todos los campos");

            } else {


                //hacemos la petición ajax
                $.ajax({
                    url: './request/insertTarifa.php',
                    type: 'POST',
                    // Form data
                    //datos del formulario
                    data: {
                        empresa: app.valorMunicipio,
                        codigo: app.codTarifa,
                        nombre: app.nomTarifa,
                        vrmin: app.vrMin,
                        vrnax: app.vrMax,
                        ic: app.valorIC,
                        icm: app.valorICM,
                    },

                }).done(function (data) {

                    console.log(data);

                    if (data == 1) {
                        alertify.success("Registro guardado Exitosamente");

                        app.resetCampos();
                        app.loadTarifas();

                    } else {
                        if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                            alertify.warning("Este codigo ya se encuentra registrado");

                        } else {
                            alertify.error("Ha ocurrido un error");

                        }
                    }

                }).fail(function (data) {

                });


            }
        },

        resetCampos: function () {

            let app = this;

            app.codTarifa = "";
            app.nomTarifa = "";
            app.vrMin = "";
            app.vrMax = "";
            app.valorIC = "";
            app.valorICM = "";

            $('#addTarifa').modal('hide');


        }
    },


    watch: {},

    mounted() {
        this.loadDepartamentos();
        this.loadIC();

    },

});
