// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });


    $(".selectActividades").select2().change(function (e) {

        var usuario = $(".selectActividades").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad(usuario, nombreUsuario);

        app.selectZona = []
        app.selectTarifa = []

        app.valorZona = "";
        app.valorTarifa = "";

        app.loadFechaAcuerdo();
        app.loadZona();
        app.loadTarifa();
        // app.loadAnio();


    });

    $(".selectActividades_modal").select2().change(function (e) {

        var usuario = $(".selectActividades_modal").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeActividad_modal(usuario, nombreUsuario);

        app.selectZona_modal = []
        app.selectTarifa_modal = []

        app.valorZona_modal = "";
        app.valorTarifa_modal = "";

        app.loadFechaAcuerdo();
        app.loadZona_modal();
        app.loadTarifa_modal();
        // app.loadAnio();


    });


    $(".selectTarifa").select2().change(function (e) {

        var codigo = $(".selectTarifa").val();

        app.valorTarifa = codigo;

    });

    $(".selectTarifa_modal").change(function (e) {

        var codigo = $(".selectTarifa_modal").val();
        app.valorTarifa_modal = codigo;

    });

    $(".selectZona").select2().change(function (e) {

        var codigo = $(".selectZona").val();
        app.valorZona = codigo;

    });

    $(".selectZona_modal").change(function (e) {

        var codigo = $(".selectZona_modal").val();
        app.valorZona_modal = codigo;

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {

        ajax: false,

        nombre: "",
        codacuerdo: "",
        codzona: "",
        codtarifa: "",


        fecha: "",
        numero: "",

        total: "",
        acuerdo: "",
        valorActividad: "",
        selectActividades: [],
        selectZona: [],
        valorZona: "",
        selectTarifa: [],
        valorTarifa: "",
        busqueda: "",

        tablaDepartamentos: [],
        tablaParametros: [],
        noResultados: false,
        consumo: "",
        consumoLimite: "",
        CMK: "",
        CMEP: "",
        CMY: "",
        CMAP: "",


        mun_modal: "",
        acuerdo_modal: "",
        total_modal: "",
        selectZona_modal: [],
        valorZona_modal: "",
        selectTarifa_modal: [],
        valorTarifa_modal: "",
        consumo_modal: "",
        consumo_modal_fin: "",
        CMK_modal: "",
        CMEP_modal: "",
        CMY_modal: "",
        CMAP_modal: "",

        codigoAcuerdoActivo: "",
    },

    methods: {

        loadFechaAcuerdo: function () {
            var app = this;
            $.post('./request/getFechaAcuerdo.php', {
                    valorActividad: app.valorActividad,
                },
                function (data) {

                    app.tablaDepartamentos = JSON.parse(data);
                    app.fecha = app.tablaDepartamentos[0]['fechaAcuerdo'];
                    app.numero = app.tablaDepartamentos[0]['numAcuerdo'];
                    app.codigoAcuerdoActivo = app.tablaDepartamentos[0]['codAcuerdo'];
                    app.total = app.fecha + "/" + app.numero;

                    console.log(app.tablaDepartamentos)
                }
            )
            ;
        },
        loadTarifa: function () {
            var app = this;
            $.post('./request/getTarifa.php', {
                    valorActividad: app.valorActividad,
                },
                function (data) {
                    app.selectTarifa = JSON.parse(data);

                }
            )
            ;
        },
        loadZona: function () {
            var app = this;


            $.post('./request/getZona.php', {
                    valorActividad: app.valorActividad,
                },
                function (data) {
                    app.selectZona = JSON.parse(data);
                    console.log(data)

                }
            )
            ;
        },
        loadClaseActividades: function () {
            var app = this;
            $.get('./request/getClaseAcividades.php', function (data) {
                app.selectActividades = JSON.parse(data);


            });
        },
        onchangeActividad: function (cod, nombre) {
            var app = this;

            app.valorActividad = cod;
        },
        verParametros: function (dato) {
            //this.total_modal = dato.COD_CLIENTE;
            var app = this;
            app.mun_modal = dato.empresa;
            app.acuerdo_modal = dato.codAcuerdo;
            app.total_modal = dato.fechaAcuerdo + "/ " + dato.numAcuerdo;
            app.codigoAcuerdoActivo = dato.codAcuerdo;
            app.valorZona_modal = dato.codZona;
            app.valorTarifa_modal = dato.codTarifa;
            app.consumo_modal = dato.consumoBase;
            app.consumo_modal_fin = dato.consumoBaseFin;
            app.CMK_modal = dato.CMK;
            app.CMEP_modal = dato.CMEP;
            app.CMY_modal = dato.CMY;
            app.CMAP_modal = dato.CMAP;
            $('#modal').modal('show');
        },
        buscar: function () {
            var app = this;

            console.log(app.valorActividad)

            if (app.valorActividad != "") {
                $.post('./request/getAno.php', {
                    valorActividad: app.valorActividad,
                    codigoAcuerdoActivo: app.codigoAcuerdoActivo,
                    valorZona: app.valorZona,
                    valorTarifa: app.valorTarifa,

                }, function (data) {
                    app.tablaParametros = JSON.parse(data);
                });

            } else {
                alertify.warning('Por favor seleccione un municipio')
            }


        },
        actualizar: function () {

            var app = this;

            if (app.mun_modal == "" ||
                app.valorZona_modal == "" ||
                app.valorTarifa_modal == "" ||
                app.consumo_modal == "" ||
                app.consumo_modal_fin == "" ||
                app.CMK_modal == "" ||
                app.CMEP_modal == "" ||
                app.CMY_modal == "" ||
                app.CMAP_modal == "") {

                alertify.error("Por favor ingresa todos los campos");

            } else {

                if (app.comparaConsumos(app.consumo_modal, app.consumo_modal_fin)) {

                    alertify.error("El consumo base no puede ser mayor al consumo base limite");

                } else {

                    console.log(app.mun_modal)
                    console.log(app.acuerdo_modal)
                    console.log(app.valorZona_modal)
                    console.log(app.valorTarifa_modal)

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateParametros.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario¿
                        data: {
                            mun: app.mun_modal,
                            acuerdo: app.acuerdo_modal,
                            zona: app.valorZona_modal,
                            tarifa: app.valorTarifa_modal,
                            consumo: app.consumo_modal,
                            consumoFin: app.consumo_modal_fin,
                            cmk: app.CMK_modal,
                            cmep: app.CMEP_modal,
                            cmy: app.CMY_modal,
                            cmap: app.CMAP_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {

                            alertify.error("Ha ocurrido un error");


                        }

                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }
            }
        },
        eliminar: function (dato) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar este parametro?",
                function () {
                    $.post('./request/deletParametros.php', {
                            valorActividad: dato.empresa,
                            codigoAcuerdoActivo: dato.codAcuerdo,
                            zona: dato.codZona,
                            tarifa: dato.codTarifa,
                        },

                        function (dato) {


                            if (dato == 1) {
                                alertify.success("Registro Eliminado.");

                                app.buscar();


                            } else {
                                if (dato.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },
        guardar: function () {

            var app = this;


            if (app.valorActividad == "" ||
                app.valorZona == "" ||
                app.valorTarifa == "" ||
                app.consumo == "" ||
                app.consumoLimite == "" ||
                app.CMK == "" ||
                app.CMEP == "" ||
                app.CMY == "" ||
                app.CMAP == "") {
                alertify.error("Por favor ingresa todos los campos");


            } else {

                if (app.comparaConsumos(app.consumo, app.consumoLimite)) {

                    alertify.error("El consumo base no puede ser mayor al consumo base limite");

                } else {

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertParametros.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            valorActividad: app.valorActividad,
                            codigoAcuerdoActivo: app.codigoAcuerdoActivo,
                            valorZona: app.valorZona,
                            valorTarifa: app.valorTarifa,
                            consumo: app.consumo,
                            consumoLimite: app.consumoLimite,
                            CMK: app.CMK,
                            CMEP: app.CMEP,
                            CMY: app.CMY,
                            CMAP: app.CMAP,
                        },
                    }).done(function (data) {

                        console.log(data)
                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");
                            app.buscar();
                            app.resetCampos2();


                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {

                    });

                }
            }
        },
        resetCampos: function () {

            var app = this;

            //app.total = "";
            app.consumo = "";
            app.consumoLimite = "";
            app.CMK = "";
            app.CMEP = "";
            app.CMY = "";
            app.CMAP = "";
            app.valorZona = "";
            app.total = "";
            $("#selectZona").val("").trigger('change.select2')
            app.valorTarifa = "";
            $("#selectTarifa").val("").trigger('change.select2')
            app.valorActividad = "";
            $("#selectActividades").val('').trigger('change.select2')


        },
        resetCampos2: function () {

            var app = this;

            //app.total = "";
            app.consumo = "";
            app.consumoLimite = "";
            app.CMK = "";
            app.CMEP = "";
            app.CMY = "";
            app.CMAP = "";
            app.valorZona = "";


        },
        resetModal: function () {
            $('#modal').modal('hide');
        },

        comparaConsumos: function (base, fin) {

            return base > fin ? true : false;

        },
    },


    watch: {},

    mounted() {

        // this.loadAnio();
        this.loadClaseActividades();


    },

});
