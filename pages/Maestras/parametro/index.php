<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 59vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">
            Parametros

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar()">
					<i class="fa fa-save" aria-hidden="true"></i> Guardar
		    	</span>

            <span class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" v-on:click="buscar()">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectActividades" id="selectActividades" name="selectActividades"
                            v-model="valorActividad">
                        <option value="">Municipio</option>
                        <option v-for="selectActividades in selectActividades"
                                :value="selectActividades.codigo">
                            {{selectActividades.codigo}} - {{selectActividades.nombre}}
                        </option>
                    </select>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="total" class="form-control" placeholder="Email address"
                           required=""
                           readonly=""
                           autocomplete="nope"
                           v-model="total">
                    <label for="total">Fecha Y Numero Del Acuerdo</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-group input-group-sm">
                    <select class="selectZona" id="selectZona" name="selectZona"
                            v-model="valorZona">
                        <option value="">Zona</option>
                        <option v-for="item in selectZona"
                                :value="item.COD">
                            {{item.NOM}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">
                    <select class="selectTarifa" id="selectTarifa" name="selectTarifa"
                            v-model="valorTarifa">
                        <option value="">Tarifa</option>
                        <option v-for="item2 in selectTarifa"
                                :value="item2.codTarifa">
                            {{item2.codTarifa}} - {{item2.nomTarifa}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="consumo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="consumo">
                    <label for="consumo">Consumo Base</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="consumo_limite" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="consumoLimite">
                    <label for="consumo_limite">Consumo Base Limite</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="CMK" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="CMK">
                    <label for="CMK">CMK</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="CMEP" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="CMEP">
                    <label for="CMEP">CMEP</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="CMY" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="CMY">
                    <label for="CMY">CMY</label>
                </div>
            </div>

            <div class="col-12 col-sm-2">
                <div class="form-label-group">
                    <input type="text" id="CMAP" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="CMAP">
                    <label for="CMAP">CMAP</label>
                </div>
            </div>

        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="100">Empresa</th>
                        <th width="100">Acuerdo</th>
                        <th>Fecha Acuerdo</th>
                        <th>Zona</th>
                        <th>Tarifa</th>
                        <th>C. Base</th>
                        <th>C. Base Fin</th>
                        <th>CMK</th>
                        <th>CMEP</th>
                        <th>CMY</th>
                        <th>CMAP</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaParametros.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaParametros">
                        <td v-text="dato.empresa" width="100"></td>
                        <td v-text="dato.numAcuerdo" width="100"></td>
                        <td v-text="dato.fechaAcuerdo"></td>
                        <td v-text="dato.codZona"></td>
                        <td v-text="dato.codTarifa"></td>
                        <td v-text="dato.consumoBase"></td>
                        <td v-text="dato.consumoBaseFin"></td>
                        <td v-text="dato.CMK"></td>
                        <td v-text="dato.CMEP"></td>
                        <td v-text="dato.CMY"></td>
                        <td v-text="dato.CMAP"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verParametros(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="modal" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg ">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel">Parametros </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">

                            <form>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="mun_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               disabled
                                               v-model="mun_modal">
                                        <label for="mun_modal">Municipio</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="total_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               readonly=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="total_modal">
                                        <label for="total_modal">Fecha Y Numero Del Acuerdo</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-2">
                                    <div class="form-label-group">
                                        <input type="text" id="valorZona_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               disabled
                                               v-model="valorZona_modal">
                                        <label for="valorZona_modal">Zona</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-2">
                                    <div class="form-label-group">
                                        <input type="text" id="valorTarifa_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               disabled
                                               v-model="valorTarifa_modal">
                                        <label for="valorTarifa_modal">Tarifa</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="consumo_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="consumo_modal">
                                        <label for="consumo_modal">Consumo Base</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="consumo_modal_fin" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="consumo_modal_fin">
                                        <label for="consumo_modal_fin">Consumo Base Fin</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="CMK_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="CMK_modal">
                                        <label for="CMK_modal">CMK</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="CMEP_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="CMEP_modal">
                                        <label for="CMEP_modal">CMEP</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="CMY_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="CMY_modal">
                                        <label for="CMY_modal">CMY</label>
                                    </div>
                                </div>

                                <div class="col-12 col-sm-4">
                                    <div class="form-label-group">
                                        <input type="text" id="CMAP_modal" class="form-control"
                                               placeholder="Email address"
                                               required=""
                                               autofocus=""
                                               autocomplete="nope"
                                               v-model="CMAP_modal">
                                        <label for="CMAP_modal">CMAP</label>
                                    </div>
                                </div>

                            </form>

                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>

                    </div>

                </form>

            </div>

        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
