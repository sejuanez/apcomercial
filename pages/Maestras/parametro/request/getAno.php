<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");

$valorActividad = $_POST['valorActividad'];
$codigoAcuerdoActivo = $_POST['codigoAcuerdoActivo'];
$valorZona = $_POST['valorZona'];
$valorTarifa = $_POST['valorTarifa'];
$usuario = $_SESSION['user'];

if ($valorZona != "" && $valorTarifa != "") {
    $stmt = "Select * from PARAM_BUSCA('2','" . $valorActividad . "','" . $codigoAcuerdoActivo . "','" . $valorZona . "','" . $valorTarifa . "', '" . $usuario . "')";
}

if ($valorZona == "" && $valorTarifa == "") {
    $stmt = "Select * from PARAM_BUSCA('3','" . $valorActividad . "','" . $codigoAcuerdoActivo . "','" . $valorZona . "','" . $valorTarifa . "', '" . $usuario . "')";
}

if ($valorZona != "" && $valorTarifa == "") {
    $stmt = "Select * from PARAM_BUSCA('4','" . $valorActividad . "','" . $codigoAcuerdoActivo . "','" . $valorZona . "','" . $valorTarifa . "', '" . $usuario . "')";
}

if ($valorZona == "" && $valorTarifa != "") {
    $stmt = "Select * from PARAM_BUSCA('5','" . $valorActividad . "','" . $codigoAcuerdoActivo . "','" . $valorZona . "','" . $valorTarifa . "', '" . $usuario . "')";
}


$query = ibase_prepare($stmt);
$result = ibase_execute($query);


$return_arr = array();
while ($fila = ibase_fetch_row($result)) {
    $row_array['empresa'] = utf8_encode($fila[0]);
    $row_array['codAcuerdo'] = utf8_encode($fila[1]);
    $row_array['numAcuerdo'] = utf8_encode($fila[2]);
    $row_array['fechaAcuerdo'] = utf8_encode($fila[3]);
    $row_array['codZona'] = utf8_encode($fila[4]);
    $row_array['codTarifa'] = utf8_encode($fila[6]);
    $row_array['consumoBase'] = utf8_encode($fila[8]);
    $row_array['consumoBaseFin'] = utf8_encode($fila[9]);
    $row_array['CMK'] = utf8_encode($fila[10]);
    $row_array['CMEP'] = utf8_encode($fila[11]);
    $row_array['CMY'] = utf8_encode($fila[12]);
    $row_array['CMAP'] = utf8_encode($fila[13]);
    array_push($return_arr, $row_array);
}

// $array = array("result"=>$return_arr);

echo json_encode($return_arr);
// echo json_encode($array);



