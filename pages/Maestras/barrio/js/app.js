// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectDepartamentos").select2().change(function (e) {
        var usuario = $(".selectDepartamentos").val();
        app.onChangeDep(usuario);
    });

    $(".selectMunicipios").select2().change(function (e) {
        var usuario = $(".selectMunicipios").val();
        app.onChangeMun(usuario);
    });

    $(".SelectSectores").select2().change(function (e) {
        var usuario = $(".SelectSectores").val();
        app.onChengeSec(usuario);
    });

    $(".SelectZonas").select2().change(function (e) {
        var usuario = $(".SelectZonas").val();
        app.onChangeZona(usuario);
    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            ajax: false,
            codigo: "",
            nombre: "",
            vrmin: "",
            vrmax: "",
            ic: "",
            icm: "",

            valorActividad: "",

            selectDepartamentos: [],
            valorDep: "",

            selectMunicipios: [],
            valorMun: "",

            SelectSectores: [],
            valorSec: "",

            SelectZonas: [],
            valorZona: "",

            selectZona_modal: [],
            valorZona_modal: "",

            busqueda: "",

            cod_dep_modal: "",
            cod_mun_modal: "",
            cod_sec_modal: "",
            cod_zona_modal: "",
            cod_modal: "",
            nom_modal: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadAnio: function () {


                var app = this;
                $.get('./request/getAno.php', function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            loadDepartamentos: function () {

                var app = this;
                $.get('./request/getDepartamento.php', function (data) {
                    app.selectDepartamentos = JSON.parse(data);
                });

            },


            onChangeDep: function (cod) {
                var app = this;

                app.selectMunicipios = [];
                app.valorDep = cod;
                app.loadMunicipios(cod);

                $("#selectMunicipios").val("").trigger('change')
                app.selectMunicipios = [];
                $("#SelectSectores").val("").trigger('change')
                app.SelectSectores = [];
                $("#SelectZonas").val("").trigger('change')
                app.SelectZonas = [];

            },

            loadMunicipios: function (codDep) {

                var app = this;

                $.post('./request/getMunicipios.php', {
                    cod_dep: codDep,
                }, function (data) {

                    app.selectMunicipios = JSON.parse(data);
                });

            },

            onChangeMun: function (cod) {
                var app = this;

                app.valorMun = cod;
                $("#SelectSectores").val("").trigger('change')
                app.SelectSectores = [];
                $("#SelectZonas").val("").trigger('change')
                app.SelectZonas = [];
                app.tablaDepartamentos = [];
                app.buscar();
                app.loadSectores(cod);
                app.loadZonas(cod);
            },

            loadSectores: function (codDep) {

                var app = this;

                $.post('./request/getSectores.php', {
                    cod_dep: app.valorDep,
                    cod_mun: app.valorMun,
                }, function (data) {

                    app.SelectSectores = JSON.parse(data);
                });

            },

            onChengeSec: function (cod) {
                var app = this;

                app.valorSec = cod;
            },

            loadZonas: function (codDep) {

                $.post('./request/getZonas.php', {
                    cod_mun: app.valorMun,
                }, function (data) {

                    app.SelectZonas = JSON.parse(data);
                });

            },
            loadZonas_modal: function (codMun) {

                let app = this;

                $.ajax({
                    url: './request/getZonas.php',
                    type: 'POST',
                    async: false,
                    data: {
                        cod_mun: codMun,
                    },

                }).done(function (response) {
                    app.selectZona_modal = JSON.parse(response);
                }).fail(function (error) {
                    console.log(error)
                });


            },

            onChangeZona: function (cod) {
                var app = this;

                app.valorZona = cod;
            },

            verDepartamento: function (dato) {

                console.log(dato)
                let app = this;

                app.cod_dep_modal = dato.cod_dep;
                app.cod_mun_modal = dato.cod_mun;
                app.cod_sec_modal = dato.cod_sec;

                app.loadZonas_modal(dato.cod_mun);
                app.valorZona_modal = dato.zona;

                app.cod_modal = dato.cod;
                app.nom_modal = dato.nom;

                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function (msg) {
                let app = this;

                console.log(app.valorMunicipios)
                if (app.valorMun != "") {
                    $.post('./request/getAno2.php', {
                        municipio: app.valorMun,
                        buscar: app.busqueda.toUpperCase(),
                    }, function (data) {

                        console.log(data)
                        app.tablaDepartamentos = jQuery.parseJSON(data);


                        app.busqueda = "";

                    });
                } else {

                    if (msg) {
                        alertify.warning('Selecciona un departamento y un municipio')
                    }
                }

            },

            actualizar: function () {

                if (this.cod_dep_modal == "" ||
                    this.cod_mun_modal == "" ||
                    this.cod_sec_modal == "" ||
                    this.cod_modal == "" ||
                    this.nom_modal == "" ||
                    this.valorZona_modal == ""
                ) {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            cod_dep: app.cod_dep_modal,
                            cod_mun: app.cod_mun_modal,
                            cod_sec: app.cod_sec_modal,
                            cod: app.cod_modal,
                            nom: app.nom_modal,
                            cod_zona: app.valorZona_modal
                        },

                    }).done(function (data) {


                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {

                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " de la empresa " + dato.empresa + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            cod_dep: dato.cod_dep,
                            cod_mun: dato.cod_mun,
                            cod_sec: dato.cod_sec,
                            cod: dato.cod,
                        }, function (data) {


                            app.valorActividad = "";

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (this.valorDep == "" ||
                    this.valorMun == "" ||
                    this.valorSec == "" ||
                    this.valorZona == "" ||
                    this.codigo == "" ||
                    this.nombre == ""
                ) {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            cod_dep: this.valorDep,
                            cod_mun: this.valorMun,
                            cod_sec: this.valorSec,
                            cod_zona: this.valorZona,
                            codigo: this.codigo,
                            nombre: this.nombre
                        },

                    }).done(function (data) {


                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadAnio();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {

                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.vrmin = "";
                app.vrmax = "";
                app.ic = "";
                $("#select-ic").val("").trigger('change')
                app.icm = "";
                $("#select-icm").val("").trigger('change')
                app.valorActividad = "";
                $("#selectActividad").val("").trigger('change')


            }
        },


        watch: {}
        ,

        mounted() {

            //this.loadAnio();
            this.loadDepartamentos();


        }
        ,

    })
;
