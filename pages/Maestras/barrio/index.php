<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 65vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Barrio

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 130px;" @click="guardar();">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Nuevo
		    	</span>

        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">
        <div v-if="ajax" class="loading">Loading&#8230;</div>

        <div class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectDepartamentos" id="selectDepartamentos" name="selectDepartamentos"
                            v-model="valorDep">
                        <option value="">Departamentos...</option>
                        <option v-for="selectActividad in selectDepartamentos"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipios" id="selectMunicipios" name="selectMunicipios"
                            v-model="valorMun">
                        <option value="">Municipios...</option>
                        <option v-for="selectActividad in selectMunicipios"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="SelectSectores" id="SelectSectores" name="SelectSectores"
                            v-model="valorSec">
                        <option value="">Sectores...</option>
                        <option v-for="selectActividad in SelectSectores"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="SelectZonas" id="SelectZonas" name="SelectZonas"
                            v-model="valorZona">
                        <option value="">Zonas...</option>
                        <option v-for="selectActividad in SelectZonas"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-sm-3"></div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="codigo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="codigo">
                    <label for="codigo">Codigo</label>
                </div>
            </div>


            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="nombre" class="form-control" placeholder="Email address" required=""
                           autocomplete="nope"
                           v-model=" nombre">
                    <label for="nombre">Nombre</label>
                </div>
            </div>


        </div>

        <div class="row">

            <div class="col-sm-3"></div>

            <div class="col-12 col-sm-6">

                <div class="col-sm-12 ">
                    <div class="input-group float-right input-group-sm"><input type="text"
                                                                               placeholder="Filtrar por codigo nombre"
                                                                               v-model="busqueda"
                                                                               class="form-control"> <span
                                class="input-group-btn"><button type="button"
                                                                v-on:click="buscar(true)"
                                                                class="btn btn-secondary">Buscar</button></span>
                    </div>
                </div>

            </div>


        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="50">Código</th>
                        <th>Nombre</th>
                        <th width="100">Cod Dep</th>
                        <th width="100">Cod Mun</th>
                        <th width="100">Cod Sec</th>
                        <th width="100">Cod Zona</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaDepartamentos.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaDepartamentos">
                        <td v-text="dato.cod" width="50"></td>
                        <td v-text="dato.nom"></td>
                        <td v-text="dato.cod_dep" width="100"></td>
                        <td v-text="dato.cod_mun" width="100"></td>
                        <td v-text="dato.cod_sec" width="100"></td>
                        <td v-text="dato.zona == 'U'?'URBANO':'RURAL'" width="100"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-pencil" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="verDepartamento(dato)"></i>
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Barrio
                    </h6>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form enctype="multipart/form-data" class="formGuardar">

                    <div class="modal-body">

                        <div class="row">


                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="descripcion_modal">Cod Departamento</label>
                                    <input type="text" id="descripcion" name="descripcion_modal" readonly
                                           class="form-control" required aria-label="descripcion_modal"
                                           v-model="cod_dep_modal">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Cod Mununicipio </label>
                                    <input type="text" id="descripcion" name="nombre_modal" readonly
                                           class="form-control" required aria-label="nombre_modal"
                                           v-model="cod_mun_modal">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Cod Sector </label>
                                    <input type="text" id="descripcion" name="nombre_modal" readonly
                                           class="form-control" required aria-label="nombre_modal"
                                           v-model="cod_sec_modal">
                                </div>
                            </div>

                            <div class="col-3">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Cod Bario </label>
                                    <input type="text" id="descripcion" name="nombre_modal" readonly
                                           class="form-control" required aria-label="nombre_modal"
                                           v-model="cod_modal">
                                </div>
                            </div>


                            <div class="col-7">
                                <div class="form-group input-group-sm">
                                    <label for="nombre_modal">Barrio</label> <br>
                                    <input type="text" id="descripcion" name="nombre_modal"
                                           class="form-control" required aria-label="nombre_modal"
                                           v-model="nom_modal">
                                </div>
                            </div>


                            <div class="col-12 col-sm-5">
                                <div class="form-group input-group-sm">

                                    <label for="nombre_modal">Zona</label> <br>
                                    <select class="select1" id="select1" name="selectActividad"
                                            v-model="valorZona_modal">
                                        <option value="">Zona...</option>
                                        <option v-for="selectActividad in selectZona_modal"
                                                :value="selectActividad.codigo">
                                            {{selectActividad.nombre}}
                                        </option>
                                    </select>

                                </div>
                            </div>

                            <!--
                                                                                 <div class="col-12 col-sm-3">
                                                                                     <div class="form-group input-group-sm">

                                                                                         <label for="nombre_modal">Unidad Mayor</label> <br>
                                                                                         <select class="select2" id="select2" name="selectActividad"
                                                                                                 v-model="ICM_modal">
                                                                                             <option value="">ICM...</option>
                                                                                             <option v-for="selectActividad in selectIC"
                                                                                                     :value="selectActividad.codigo">
                                                                                                 {{selectActividad.nombre}}
                                                                                             </option>
                                                                                         </select>

                                                                                     </div>
                                                                                 </div>
                                                     -->

                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                            Cancelar
                        </button>

                        <button type="button" v-if="true" class="btn btn-primary btn-sm" @click="actualizar()">
                            Actualizar
                        </button>


                    </div>

                </form>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
