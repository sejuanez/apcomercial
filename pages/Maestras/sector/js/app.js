// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectDepartamentos").select2().change(function (e) {

        var usuario = $(".selectDepartamentos").val();
        var nombreUsuario = this.options[this.selectedIndex].text;


        app.onChangeDepartamento(usuario, nombreUsuario);

    });

    $(".selectMunicipios").select2().change(function (e) {

        var usuario = $(".selectMunicipios").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeMunicipio(usuario, nombreUsuario);

    });


});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {
            ajax: false,
            codigo: "",
            nombre: "",
            fecha: "",
            activo: "",


            selectDepartamentos: [],
            valorDepartamento: "",
            selectMunicipios: [],
            valorMunicipio: "",


            busqueda: "",

            codigo_sector: "",
            nombre_sector: "",
            codigo_dep: "",
            codigo_mun: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadTabla: function () {


                var app = this;
                $.get('./request/getAno.php', function (data) {
                    console.log(data)
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            loadDepartamentos: function () {
                var app = this;
                $.get('./request/getDepartamentos.php', function (data) {
                    console.log(data)
                    app.selectDepartamentos = JSON.parse(data);
                });
            },

            loadMunicipios: function (codMun) {

                console.log(codMun)

                var app = this;
                $.get('./request/getMunicipios.php', {
                    buscar: codMun,
                }, function (data) {
                    app.selectMunicipios = JSON.parse(data);
                    console.log(data)
                });
            },

            onChangeDepartamento: function (cod, nombre) {
                var app = this;
                app.valorDepartamento = cod;

                app.loadMunicipios(cod)

            },

            onChangeMunicipio: function (cod, nombre) {
                var app = this;
                app.valorMunicipio = cod;
            },

            verDepartamento: function (dato) {

                this.codigo_sector = dato.codSec;
                this.nombre_sector = dato.nomSec;

                this.codigo_dep = dato.codDep;
                this.codigo_mun = dato.codMun;

                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.codigo_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadTabla()

                }

            },

            actualizar: function () {

                if (this.codigo_dep == "" ||
                    this.codigo_mun == "" ||
                    this.codigo_sector == "" ||
                    this.nombre_sector == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codSec: app.codigo_sector,
                            nomSec: app.nombre_sector,
                            codDep: app.codigo_dep,
                            codMun: app.codigo_mun,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el sector " + dato.nomSec + "?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codDep: dato.codDep,
                            codMun: dato.codMun,
                            codSec: dato.codSec,
                        }, function (data) {

                            console.log(data)

                            app.valorActividad = "";

                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                if (data.indexOf("Foreign key references are present for the record") > -1) {


                                    alertify.warning("Este item esta siendo utilizado");

                                } else {
                                    alertify.error("Ha ocurrido un error");

                                }
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (this.codigo == "" ||
                    this.nombre == "" ||
                    this.valorDepartamento == "" ||
                    this.valorMunicipio == ""


                ) {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            codDep: this.valorDepartamento,
                            codMun: this.valorMunicipio,
                            codSec: this.codigo,
                            nomSec: this.nombre
                        },

                    }).done(function (data) {

                        console.log(data)


                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadTabla();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.valorDepartamento = "";
                app.valorMunicipio = "";
                $("#selectDepartamentos").val("").trigger('change')
                $("#selectMunicipios").val("").trigger('change')

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadTabla();
            this.loadDepartamentos();


        }
        ,

    })
;
