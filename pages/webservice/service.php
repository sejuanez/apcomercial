<?php
// Web Service
    error_reporting(0);
    include("../../init/gestion.php");


    $array = file_get_contents('php://input');
    $array = (json_decode($array, true));

    /*
    $array = [

        [
            "procedimiento" => 'ANO_CREA',
            "valores" => [
                '1' => '20220',
                '2' => '2020',
                '3' => 'null'
            ]
        ],

        [
            "procedimiento" => 'ANO_CREA',
            "valores" => [
                '1' => '20221',
                '2' => '2021',
                '3' => 'null'
            ]
        ],

    ];
    */

    $trans = ibase_trans(IBASE_DEFAULT);

//var_dump($array);

    $return_arr = array();
    $sql = "";

    $primerCampo = "";
    $segundoCampo = "";
    $flang = true;

    $error_arr = array();

    foreach ($array as $json) {

        $sql = "EXECUTE PROCEDURE ";

        $sql .= $json['procedimiento'];
        $sql .= " (";

        for ($i = 1; $i <= count($json['valores']); $i++) {

            if ($i == 1) {
                $primerCampo = utf8_decode($json['valores'][$i]);
            }

            if ($i == count($json['valores'])) {
                $sql .= "'" . utf8_decode($json['valores'][$i]) . "'";
                $sql .= ");";
            } else {
                $sql .= "'" . utf8_decode($json['valores'][$i]) . "'";
                $sql .= ",";
            }
        }


        $result = ibase_query($trans, $sql);

        if ($result == false) {
            $flang = false;

            $row_array_err['error'] = "" . str_replace('"', '', ibase_errmsg()) . "";
            array_push($error_arr, $row_array_err);

        }

        $row_array['resultado'] = array(
            "sync" => $result,
            "campo1" => $primerCampo,
            "campo2" => $segundoCampo,
        );
        array_push($return_arr, $row_array);


    }
    if (!$flang) {
        ibase_rollback($trans);
        echo json_encode($error_arr);
    } else {

        ibase_commit($trans);
        echo 'true';

    }


    //echo 'Items Registrados: ' . count($return_arr);

