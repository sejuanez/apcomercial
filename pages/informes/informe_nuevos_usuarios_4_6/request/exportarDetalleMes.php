<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];


$stmt = "Select * from INF_NUEVOS_USUARIOS(           '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "'                                                                                                   
                                                      )";

$nombre = $mun . "_" . $anio;

header("Content-Disposition: filename=Informe_Nuevos_Usuario_" . $nombre . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='4'>Informe Nuevos Usuarios - $nombre</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Cod Cliente</th>" .
    "<th>Nom Cliente</th>" .
    "<th>Tarifa</th>" .
    "<th>Vr Facturado</th>" .
    "</tr>";


$facturado = 0;
$clientes = 0;


while ($fila = ibase_fetch_row($result)) {


    $facturado += intval($fila[3]);
    $clientes++;


    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "</tr>";

}


$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='1'> <strong>TOTALES</strong> </td>" .
    "<td style='text-align: center' > " . $clientes . 'Usuarios' . "</td>" .
    "<td>" . $facturado . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

