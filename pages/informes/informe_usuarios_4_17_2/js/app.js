// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });

    $(".selectClientes").select2().change(function (e) {
        var usuario = $(".selectClientes").val();
        var nombreUsuario = this.options[this.selectedIndex].text;
        app.onchangeClientes(usuario, nombreUsuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        tablaDetalle: [],

        totalClientes: 0,
        totalFacturado: 0,
        totalLiquidados: 0,
        totalLiquidado: 0,
        TotalRecaudados: 0,
        totalRecaudado: 0,
        totalGenerada: 0,
        totalRecuperada: 0,

        facturadoModal: 0,
        liquidadoModal: 0,
        recaudadoModal: 0,
        generadaModal: 0,
        recuperadaModal: 0,

        checkGen: false,
        checkEsp: false,

        selectClientes: [],
        valorCliente: "",
    },


    methods: {


        loadClientes: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                mun: app.valorMun
            }, function (data) {
                app.selectClientes = JSON.parse(data);
            });
        },

        onchangeClientes: function (cod, nombre) {
            var app = this;
            app.valorCliente = cod;
            app.verificar();
            // console.log(app.codigo)
        },

        modalDetalle: function (opcion) {

            if (opcion) {
                $('#modalDetalle').modal('show');
            } else {
                $('#modalDetalle').modal('hide');
            }

        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
            app.loadClientes();
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            let app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;
            app.tablaDetalle = [];
            app.enableSelect2('.selectMunicipio');
            app.enableSelect2('.selectClientes');

            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;


            app.btn_verificar = false;
            app.btn_verificando = true;


            app.disableSelect2('.selectMunicipio');
            app.disableSelect2('.selectClientes');


            $.post('./request/getDetalleMes.php', {
                mun: app.valorMun,
                cliente: app.valorCliente,
            }, function (data) {

                try {

                    app.tablaDetalle = JSON.parse(data);

                    /*
                                        let auxVrFac = 0;
                                        let auxVrLiq = 0;
                                        let auxVrRec = 0;
                                        let auxGen = 0;
                                        let auxRec = 0;

                                        for (i in app.tablaDetalle) {

                                            auxVrFac += parseFloat(app.tablaDetalle[i].vr_fact);
                                            auxVrLiq += parseFloat(app.tablaDetalle[i].vr_liq);
                                            auxVrRec += parseFloat(app.tablaDetalle[i].vr_rec);
                                            auxGen += parseFloat(app.tablaDetalle[i].generada);
                                            auxRec += parseFloat(app.tablaDetalle[i].recuperada);

                                        }

                                        console.log(auxVrFac)
                                        console.log(auxVrRec)


                                        if (auxVrFac >= auxVrRec) {

                                            var saldo = auxVrFac - auxVrRec;
                                            app.generadaModal = (saldo);
                                            app.recuperadaModal = 0;

                                        } else {
                                            var saldo2 = auxVrRec - auxVrFac;
                                            app.generadaModal = 0;
                                            app.recuperadaModal = saldo2;
                                        }

                                        app.totalClientes = (app.tablaDetalle.length).toLocaleString();
                                        app.facturadoModal = "$ " + parseInt(auxVrFac, 10).toLocaleString();
                                        app.liquidadoModal = "$ " + parseInt(auxVrLiq, 10).toLocaleString();
                                        app.recaudadoModal = "$ " + parseInt(auxVrRec, 10).toLocaleString();
                                        app.generadaModal = "$ " + app.generadaModal.toLocaleString();
                                        app.recuperadaModal = "$ " + app.recuperadaModal.toLocaleString();
                    */
                } catch (e) {

                    console.log(e)

                }


                $("#btnExportarComer a").attr("href",
                    "request/exportarDetalleMes.php?mun=" + app.valorMun +
                    "&cliente=" + app.valorCliente
                );


                app.btn_cancelar = true;
                app.btn_verificar = false;
                app.btn_cargando = false;
                app.btn_verificando = false;

            });


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },


    },


    watch: {},

    mounted() {

        this.loadMunicipios();

    },

});
