// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        tablaDetalle: [],

        totalClientes: 0,
        totalFacturado: 0,
        totalLiquidados: 0,
        totalLiquidado: 0,
        TotalRecaudados: 0,
        totalRecaudado: 0,
        totalGenerada: 0,
        totalRecuperada: 0,

        facturadoModal: 0,
        liquidadoModal: 0,
        recaudadoModal: 0,
        generadaModal: 0,
        recuperadaModal: 0,

        checkGen: false,
        checkEsp: false,

        labels: [],
        datasets: [],
        colors: [
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#0008C5',
            '#FF6347',
            '#FFFF00',
            '#00034A',
            '#8A2BE2',
            '#6ce600',
            '#FF1493',
            '#FF8C00',
            '#9932CC',
            '#ADFF2F',
            '#FF0000',
            '#FFD700',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#8A2BE2',
            '#FFFF00',
            '#9932CC',
            '#ADFF2F',
            '#FF8C00',
            '#FF1493',
            '#008000',
            '#FFD700',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FF0000',
            '#800080',
            '#00FF7F',
            '#FF6347',
            '#FFFF00',
            '#8A2BE2',
            '#FF8C00',
            '#9932CC',
            '#FF1493',
            '#ADFF2F',
            '#008000',
            '#800000',
            '#6B8E23',
            '#FF4500',
            '#FFD700',
            '#FF0000'],

    },


    methods: {


        check: function (opcion) {
            var app = this;

            if (opcion === 1) {

                if (app.checkEsp) {
                    app.checkGen = true;
                    app.checkEsp = false;
                } else {
                    app.checkGen = false;
                }

            } else {

                if (app.checkGen) {
                    app.checkEsp = true;
                    app.checkGen = false;
                } else {
                    app.checkEsp = true;
                }


            }

        },

        modalDetalle: function (opcion) {

            if (opcion) {
                $('#modalDetalle').modal('show');
            } else {
                $('#modalDetalle').modal('hide');
            }

        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            let app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;
            app.tablaDetalle = [];

            app.enableSelect2('.selectMunicipio');
            app.enableSelect2('.selectAño');
            app.enableSelect2('.selectAñoFin');
            app.enableSelect2('.selectMes');
            app.enableSelect2('.selectMesFin');
            app.enableSelect2('.selectComercializador');

            $('#checkbox').prop('disabled', false);
            $('#checkbox2').prop('disabled', false);

            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ ";

            let ctx = document.getElementById('myChart');
            let myChart = new Chart(ctx, {type: 'bar', data: null, options: null});
            myChart.destroy();

            let ctx2 = document.getElementById('myChart2');
            let myChart2 = new Chart(ctx2, {type: 'bar', data: null, options: null});
            myChart2.destroy();

        },

        verificar: function (msg) {

            var app = this;


            if (
                app.valorMun === "" ||
                app.valorAño === "" ||
                app.valorAñoFin === "" ||
                app.valorMes === "" ||
                app.valorMesFin === ""
            ) {

                alertify.error("Selecciona un Municipio y un rango de fechas")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio');
                app.disableSelect2('.selectAño');
                app.disableSelect2('.selectAñoFin');
                app.disableSelect2('.selectMes');
                app.disableSelect2('.selectMesFin');
                app.disableSelect2('.selectComercializador');
                $('#checkbox').prop('disabled', true);
                $('#checkbox2').prop('disabled', true);


                $.post('./request/getDetalleMes.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    anioFin: app.valorAñoFin,
                    mes: app.valorMes,
                    mesFin: app.valorMesFin,
                    gen: app.checkGen,
                    esp: app.checkEsp,
                }, function (data) {


                    //console.log(data);

                    try {

                        app.tablaDetalle = JSON.parse(data);
                        app.calcularGrafico();

                        let auxVrFac = 0;
                        let auxVrLiq = 0;
                        let auxVrRec = 0;
                        let auxGen = 0;
                        let auxRec = 0;

                        for (i in app.tablaDetalle) {

                            auxVrFac += parseFloat(app.tablaDetalle[i].vr_fact);
                            auxVrLiq += parseFloat(app.tablaDetalle[i].vr_liq);
                            auxVrRec += parseFloat(app.tablaDetalle[i].vr_rec);
                            auxGen += parseFloat(app.tablaDetalle[i].generada);
                            auxRec += parseFloat(app.tablaDetalle[i].recuperada);

                        }

                        app.totalClientes = (app.tablaDetalle.length).toLocaleString();
                        app.facturadoModal = "$ " + parseInt(auxVrFac, 10).toLocaleString();
                        app.liquidadoModal = "$ " + parseInt(auxVrLiq, 10).toLocaleString();
                        app.recaudadoModal = "$ " + parseInt(auxVrRec, 10).toLocaleString();
                        app.generadaModal = "$ " + auxGen.toLocaleString();
                        app.recuperadaModal = "$ " + auxRec.toLocaleString();

                    } catch (e) {

                        console.log(e)

                    }


                    $("#btnExportarComer a").attr("href",
                        "request/exportarDetalleMes.php?mun=" + app.valorMun +
                        "&anio=" + app.valorAño +
                        "&anioFin=" + app.valorAñoFin +
                        "&mes=" + app.valorMes +
                        "&mesFin=" + app.valorMesFin +
                        "&gen=" + app.checkGen +
                        "&esp=" + app.checkEsp
                    );


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

        calcularGrafico: function () {

            let app = this;

            let labels = [];
            let datasets = [];

            let labels2 = [];
            let datasets2 = [];


            let data = [];
            let data2 = [];

            for (let i in app.tablaDetalle) {

                labels.push(app.tablaDetalle[i].mes);

                let fac = parseInt(app.tablaDetalle[i].vr_fact, 10);
                let cart = parseInt(app.tablaDetalle[i].generada, 10);

                let fac2 = parseInt(app.tablaDetalle[i].vr_rec, 10);
                let cart2 = parseInt(app.tablaDetalle[i].recuperada, 10);


                let eficiencia = 0;
                let eficiencia2 = 0;

                eficiencia = (cart / fac) * 100;
                if (!eficiencia) {
                    eficiencia = 0;
                }

                eficiencia2 = (cart2 / fac2) * 100;
                if (!eficiencia2) {
                    eficiencia2 = 0;
                }

                data.push(eficiencia);
                data2.push(eficiencia2);


            }

            datasets.push({
                'label': 'Eficiencia',
                'data': data,
                'backgroundColor': app.colors
            });

            datasets2.push({
                'label': 'Eficiencia',
                'data': data2,
                'backgroundColor': app.colors
            });


            app.graficar(labels, datasets);
            app.graficar2(labels, datasets2);

        },

        graficar: function (labels, datasets) {


            document.getElementById("char1").innerHTML = "";
            document.getElementById("char1").innerHTML = "<canvas style='margin-top: 35px;' id='myChart'></canvas>";

            var ctx = document.getElementById('myChart');

            var myChart = new Chart(ctx, {
                showTooltips: false,
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: datasets

                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    aspectRatio: 1,
                    tooltips: {
                        enabled: true
                    },
                    legend: {
                        display: false
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 1)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = parseInt(dataset.data[index], 10) + ' %';
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                });
                            });
                        }
                    },

                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                max: 110,
                                callback: function (value) {
                                    return value + "%"
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Porcentaje"
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            }
                        }]
                    }
                }
            });

        },
        graficar2: function (labels, datasets) {

            document.getElementById("char2").innerHTML = "";
            document.getElementById("char2").innerHTML = "<canvas style='margin-top: 35px;' id='myChart2'></canvas>";

            var ctx = document.getElementById('myChart2');

            var myChart = new Chart(ctx, {
                showTooltips: false,
                type: 'bar',
                data: {
                    labels: labels,
                    datasets: datasets

                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    aspectRatio: 1,
                    tooltips: {
                        enabled: true
                    },
                    legend: {
                        display: false
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 1)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = parseInt(dataset.data[index], 10) + ' %';
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);

                                });
                            });
                        }
                    },

                    scales: {
                        yAxes: [{
                            ticks: {
                                min: 0,
                                max: 100,
                                callback: function (value) {
                                    return value + "%"
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: "Porcentaje"
                            }
                        }]
                    }
                }
            });

        }

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        // this.modalDetalle(true)

    },

});
