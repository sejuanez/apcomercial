<?php
/*
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
*/
ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$mes = $_GET['mes'];
$mesFin = $_GET['mesFin'];
$gen = $_GET['gen'];
$esp = $_GET['esp'];

$nombre = "";

if ($gen == 'false' && $esp == 'false') {

    if ($anio == "" && $mes == "" && $anioFin == "" && $mesFin == "") {

        $nombre = $mun . "_Historico";

        $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      null,
                                                      null,
                                                      null,
                                                      null
                                                      )";


    } else if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

        $nombre = $mun . "_" . $anio . "_" . $mes;

        $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '2' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

    } else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

        $nombre = $mun . "_" . $anio . "_" . $mes . "_HASTA_" . $anioFin . "_" . $mesFin;

        $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '2' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mesFin . "' 
                                                      )";

    }

} else {

    if ($esp == 'true') {

        $nombre = "Especiales_";

        if ($anio == "" && $mes == "" && $anioFin == "" && $mesFin == "") {

            $nombre .= $mun . "_Historico";


            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '3' , 
                                                      null,
                                                      null,
                                                      null,
                                                      null
                                                      )";


        } else if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

            $nombre .= $mun . "_" . $anio . "_" . $mes;

            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '4' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

        } else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

            $nombre .= $mun . "_" . $anio . "_" . $mes . "_HASTA_" . $anioFin . "_" . $mesFin;

            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '4' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mesFin . "' 
                                                      )";

        }

        //
    } else {

        $nombre = "General_";

        if ($anio == "" && $mes == "" && $anioFin == "" && $mesFin == "") {

            $nombre .= $mun . "_Historico";

            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '5' , 
                                                      null,
                                                      null,
                                                      null,
                                                      null
                                                      )";


        } else if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

            $nombre .= $mun . "_" . $anio . "_" . $mes;

            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '6' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

        } else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

            $nombre .= $mun . "_" . $anio . "_" . $mes . "_HASTA_" . $anioFin . "_" . $mesFin;

            $stmt = "Select * from INF_RECAUDO_CORRIENTE_CARTERA( 
                                                      '" . $mun . "', 
                                                      '6' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mesFin . "' 
                                                      )";

        }

    }


}

header("Content-Disposition: filename=Recaudo(Corriente y Cartera)" . $nombre . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='6'>Recaudo (Corriente y Cartera) - $nombre</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Total Facturado</th>" .
    "<th>Total Recaudado</th>" .
    "<th>Recaudo Corriente</th>" .
    "<th>Recaudo Cartera</th>" .
    "</tr>";


$facturado = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {


    $facturado += intval($fila[3]);
    $recaudado += intval($fila[4]);
    $generada += intval($fila[5]);
    $recuperada += intval($fila[6]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "</tr>";

}


$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

