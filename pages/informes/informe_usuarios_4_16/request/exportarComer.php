<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
ini_set('max_execution_time', 0);

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$mes = $_GET['mes'];
$mesFin = $_GET['mesFin'];

$fecha = "";

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Expires: 0");

if ($anio != "" && $mes != "" && $anioFin == "" && $mesFin == "") {

    $fecha = $anio . "_" . $mes;

    $stmt = "Select * from INF_CARTERA_MESES( '5' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mes . "'
                                                      )";

} else if ($anio != "" && $mes != "" && $anioFin != "" && $mesFin != "") {

    $fecha = $anio . "_" . $mes . "_Hasta_" . $anioFin . "_" . $mesFin;

    $stmt = "Select * from INF_CARTERA_MESES( '5' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "', 
                                                      '" . $mes . "',                                                  
                                                      '" . $mesFin . "' 
                                                      )";

}

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();

header("Content-Disposition: filename=Facturacion_Clientes_Especiales" . $mun . "_" . $fecha . " .xls");

$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='8'>Informe Facturacion Clientes Especiales - $mun - $fecha</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Us Fact</th>" .
    "<th>Facturado</th>" .
    "</tr>";

$usFact = 0;
$facturado = 0;
$usRec = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {

    $usFact += intval($fila[3]);
    $facturado += intval($fila[4]);
    $usRec += intval($fila[5]);
    $recaudado += intval($fila[8]);
    $generada += intval($fila[9]);
    $recuperada += intval($fila[10]);


    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $usFact . "</td>" .
    "<td>" . $facturado . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

