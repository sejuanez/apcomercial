// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        tablaDetalle: [],

        totalClientes: 0,
        totalFacturado: 0,
        totalLiquidados: 0,
        totalLiquidado: 0,
        TotalRecaudados: 0,
        totalRecaudado: 0,
        totalGenerada: 0,
        totalRecuperada: 0,

        facturadoModal: 0,
        liquidadoModal: 0,
        recaudadoModal: 0,
        generadaModal: 0,
        recuperadaModal: 0,


    },


    methods: {


        modalDetalle: function (opcion) {

            if (opcion) {
                $('#modalDetalle').modal('show');
            } else {
                $('#modalDetalle').modal('hide');
            }

        },

        /*
        getUsuarios: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                    mun: app.valorMun,
                    busqueda: app.buscarUsuario,
                },

                function (data) {
                    var data = JSON.parse(data);
                    app.tablaClientes = data;
                }
            )
            ;
        },

        seleccionarCliente: function (data) {
            var app = this;
            app.idUsuario = data.codigo;
            app.nombreUsuario = data.codigo + " -  " + data.nombre;
            app.buscarUsuario = "";
            $('#modalMateriales').modal('hide');
        },

        loadClientes() {

            var app = this;

            app.selectUsuarios = [];

            $.post('./request/getClientes.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {

                //console.log(data)

                var usuarios = JSON.parse(data);

                var uPropios = usuarios['propio'];
                var uComer = uPropios.concat(usuarios['comer'])

                app.selectUsuarios = uComer;

            });

        },
        */

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectAñoFin')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectMesFin')
            app.enableSelect2('.selectComercializador')


            app.tablaConsumos = [];
            app.tablaDetalle = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;


            if (
                app.valorMun === "" ||
                app.valorAño === "" ||
                app.valorMes === ""
            ) {

                alertify.error("Municipio, año y mes son obligatorios")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio');
                app.disableSelect2('.selectAño');
                app.disableSelect2('.selectAñoFin');
                app.disableSelect2('.selectMes');
                app.disableSelect2('.selectMesFin');
                app.disableSelect2('.selectComercializador');


                $.post('./request/getDetalleMes.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {


                    console.log(data);

                    try {

                        app.tablaDetalle = JSON.parse(data);


                        let auxVrFac = 0;

                        for (let i in app.tablaDetalle) {

                            auxVrFac += parseFloat(app.tablaDetalle[i].vr_fact);

                        }

                        app.totalClientes = (app.tablaDetalle.length).toLocaleString();
                        app.facturadoModal = "$ " + parseInt(auxVrFac, 10).toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarDetalleMes.php?mun=" + app.valorMun +
                            "&anio=" + app.valorAño +
                            "&mes=" + app.valorMes
                        );


                    } catch (e) {

                        console.log(e)

                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },


    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        // this.modalDetalle(true)

    },

});
