<?php
/*
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}
*/
ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$mes = $_GET['mes'];
$gen = $_GET['gen'];
$esp = $_GET['esp'];

$nombre = "";

if ($gen == 'false' && $esp == 'false') {


    if ($mes == "") {

        $nombre = "todos_" . $mun . "_" . $anio;

        $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '2' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      null                                                            
                                                      )";

        //
    } else {

        $nombre = "todos_" . $mun . "_" . $anio . "_" . $mes;

        $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '1' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "'
                                                      )";

    }

} else {

    if ($gen == 'true') {

        if ($mes == "") {

            $nombre = "general_" . $mun . "_" . $anio;

            $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '4' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      null                                                   
                                                      )";

            //
        } else {

            $nombre = "general_" . $mun . "_" . $anio . "_" . $mes;

            $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '3' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "'                                                  
                                                      )";

        }

    } else {

        if ($mes == "") {

            $nombre = "especiales_" . $mun . "_" . $anio;

            $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '6' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      null                                                   
                                                      )";

            //
        } else {

            $nombre = "especiales_" . $mun . "_" . $anio . "_" . $mes;

            $stmt = "Select * from INF_CARTERA_MESES_ESP_AND_GEN( '5' , 
                                                      '" . $mun . "', 
                                                      '" . $anio . "', 
                                                      '" . $mes . "'                                                 
                                                      )";

        }

    }

}

header("Content-Disposition: filename=Informe_Por_Usuarios_" . $nombre . ".xls");

$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='9'>Informe por usuarios - $nombre</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Cod Cliente</th>" .
    "<th>Nom Cliente</th>" .
    "<th>Tarifa</th>" .
    "<th>Vr Facturado</th>" .
    "<th>Vr Recaudado</th>" .
    "<th>Cartera Generada</th>" .
    "<th>Cartera Recuperada</th>" .
    "</tr>";


$facturado = 0;
$recaudado = 0;
$generada = 0;
$recuperada = 0;

while ($fila = ibase_fetch_row($result)) {


    $facturado += intval($fila[3]);
    $recaudado += intval($fila[4]);
    $generada += intval($fila[5]);
    $recuperada += intval($fila[6]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[2]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "<td>" . ($fila[8]) . "</td>" .
        "<td>" . ($fila[9]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "</tr>";

}


$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='5'> <strong>TOTALES</strong> </td>" .
    "<td>" . $facturado . "</td>" .
    "<td>" . $recaudado . "</td>" .
    "<td>" . $generada . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

