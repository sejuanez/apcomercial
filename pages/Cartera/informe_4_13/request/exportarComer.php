<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$anioFin = $_GET['anioFin'];
$esp = $_GET['esp'];
$gen = $_GET['gen'];

$userReporte = "";
$fechaReporte = "";

if ($gen == 'false' && $esp == 'false') {

    $userReporte = 'Todos';

    if ($anio != "" && $anioFin == "") {

        $fechaReporte = $anio;

        $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES(
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";

    } else if ($anio != "" && $anioFin != "") {

        $fechaReporte = $anio . "_Hasta_" . $anioFin;

        $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES( 
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "'
                                                      )";

    }


} else {

    if ($gen == 'true') {

        $userReporte = 'Generales';

        if ($anio != "" && $anioFin == "") {

            $fechaReporte = $anio;

            $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES(   
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";

        } else if ($anio != "" && $anioFin != "") {

            $fechaReporte = $anio . "_Hasta_" . $anioFin;

            $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES( 
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "'
                                                      )";

        }


    } else {

        $userReporte = 'Especiales';

        if ($anio != "" && $anioFin == "") {

            $fechaReporte = $anio;

            $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES( 
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";

        } else if ($anio != "" && $anioFin != "") {

            $fechaReporte = $anio . "_Hasta_" . $anioFin;

            $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES( 
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '1' , 
                                                      '" . $anio . "', 
                                                      '" . $anioFin . "'
                                                      )";

        }

    }
}


header("Content-Disposition: filename=Reporte_Cartera_por_Edades_y_Recuperacion_" . $userReporte . "_" . $mun . "_" . $fechaReporte . ".xls");


$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='7'>Reporte Cartera por Edades y Recuperacion $comerReporte $mun</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Cartera</th>" .
    "<th>Corriente</th>" .
    "<th>Car < 30</th>" .
    "<th>Car < 60</th>" .
    "<th>Car > 90</th>" .
    "<th>Car Recuperada</th>" .
    "</tr>";

$cartera = 0;
$corriente = 0;
$car30 = 0;
$car60 = 0;
$car90 = 0;
$recuperada = 0;


while ($fila = ibase_fetch_row($result)) {

    $cartera += intval($fila[2]);
    $corriente += intval($fila[3]);
    $car30 += intval($fila[4]);
    $car60 += intval($fila[5]);
    $car90 += intval($fila[6]);
    $recuperada += intval($fila[7]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . intval($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center'> <strong>TOTALES</strong> </td>" .
    "<td>" . $cartera . "</td>" .
    "<td>" . $corriente . "</td>" .
    "<td>" . $car30 . "</td>" .
    "<td>" . $car60 . "</td>" .
    "<td>" . $car90 . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

