<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

ini_set('max_execution_time', 0);

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");

header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");

$mun = $_GET['mun'];
$anio = $_GET['anio'];
$esp = $_GET['esp'];
$gen = $_GET['gen'];

$userReporte = "";

if ($gen == 'false' && $esp == 'false') {

    $userReporte = 'Todos';

    $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES(
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '2' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";


} else {

    if ($gen == 'true') {

        $userReporte = 'General';

        $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES(
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '2' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";


    } else {

        $userReporte = 'Especiales';

        $stmt = "Select * from INF_ACUMULADO_CARTERA_EDADES(
                                                      '" . $mun . "', 
                                                      '1' , 
                                                      '2' , 
                                                      '" . $anio . "', 
                                                      '" . $anio . "'
                                                      )";

    }
}

header("Content-Disposition: filename=Reporte_Detallado_Cartera_por_Edades_y_Recuperacion_(" . $userReporte . ")_" . $mun . "_" . $anio . ".xls");


$query = ibase_prepare($stmt);
$result = ibase_execute($query);

$return_arr = array();


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='8'>Reporte Cartera por Edades y Recuperacion $mun / $anio</th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>Año</th>" .
    "<th>Mes</th>" .
    "<th>Cartera</th>" .
    "<th>Corriente</th>" .
    "<th>Car < 30</th>" .
    "<th>Car < 60</th>" .
    "<th>Car > 90</th>" .
    "<th>Car Recuperada</th>" .
    "</tr>";

$cartera = 0;
$corriente = 0;
$car30 = 0;
$car60 = 0;
$car90 = 0;
$recuperada = 0;


while ($fila = ibase_fetch_row($result)) {

    $cartera += intval($fila[2]);
    $corriente += intval($fila[3]);
    $car30 += intval($fila[4]);
    $car60 += intval($fila[5]);
    $car90 += intval($fila[6]);
    $recuperada += intval($fila[7]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . ($fila[0]) . "</td>" .
        "<td>" . ($fila[1]) . "</td>" .
        "<td>" . intval($fila[2]) . "</td>" .
        "<td>" . intval($fila[3]) . "</td>" .
        "<td>" . intval($fila[4]) . "</td>" .
        "<td>" . intval($fila[5]) . "</td>" .
        "<td>" . intval($fila[6]) . "</td>" .
        "<td>" . intval($fila[7]) . "</td>" .
        "</tr>";

}

$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td>" . $cartera . "</td>" .
    "<td>" . $corriente . "</td>" .
    "<td>" . $car30 . "</td>" .
    "<td>" . $car60 . "</td>" .
    "<td>" . $car90 . "</td>" .
    "<td>" . $recuperada . "</td>" .
    "</tr>";

$tabla .= "</table>";

echo $tabla;

