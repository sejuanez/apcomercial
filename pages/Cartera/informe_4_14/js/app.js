// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        usuarioBuscar: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        totalFacturado: 0,
        totalFacturas: 0,
        totalRecaudos: 0,
        totalRecaudado: 0,
        totalRecuperada: 0,
        totalGenerada: 0,

        nomUsuarioReporte: "",
        regimenUsuarioReporte: "",

    },


    methods: {

        modalUsuario: function () {
            var app = this;
            if (app.valorMun != "") {
                $("#inputBusqueda").focus();
                app.tablaClientes = [];

                if (app.usuarioBuscar != "") {
                    app.buscarUsuario = app.usuarioBuscar;
                    $('#modalMateriales').modal('show');
                    app.getUsuarios();
                    app.usuarioBuscar = "";
                } else {
                    app.usuarioBuscar = "";
                    $('#modalMateriales').modal('show');
                }


            } else {
                alertify.warning('Por favor seleccione un municipio')
            }
        },

        getUsuarios: function () {
            var app = this;
            $.post('./request/getClientes.php', {
                    mun: app.valorMun,
                    busqueda: app.buscarUsuario,
                },

                function (data) {
                    var data = JSON.parse(data);
                    app.tablaClientes = data;
                }
            )
            ;
        },

        seleccionarCliente: function (data) {
            var app = this;
            app.idUsuario = data.codigo;
            app.nombreUsuario = data.codigo + " - " + data.nombre;
            app.nomUsuarioReporte = data.nombre;
            app.regimenUsuarioReporte = data.regimen;
            app.buscarUsuario = "";
            app.verificar();
            $('#modalMateriales').modal('hide');
        },

        loadClientes() {

            var app = this;

            app.selectUsuarios = [];

            $.post('./request/getClientes.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                comer: app.valorComercializador,
            }, function (data) {

                //console.log(data)

                var usuarios = JSON.parse(data);

                var uPropios = usuarios['propio'];
                var uComer = uPropios.concat(usuarios['comer'])

                app.selectUsuarios = uComer;

            });

        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')


            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "
            app.usuarioBuscar = "";

        },

        verificar: function (msg) {

            let app = this;

            if (
                app.valorMun == "" ||
                app.nombreUsuario == ""
            ) {

                alertify.error("Por favor selecciona un Usuario")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio');
                app.disableSelect2('.selectAño');
                app.disableSelect2('.selectMes');
                app.disableSelect2('.selectComercializador');


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    cliente: app.idUsuario
                }, function (data) {

                    console.log(data);

                    try {

                        app.tablaHistorico = JSON.parse(data);

                        let auxRecaudado = 0;
                        let auxFacturado = 0;
                        let auxFacturas = 0;
                        let auxRecaudos = 0;

                        for (let i in app.tablaHistorico) {

                            auxRecaudado += parseFloat(app.tablaHistorico[i].recaudado);
                            auxFacturado += parseFloat(app.tablaHistorico[i].facturado);
                            auxFacturas += parseFloat(app.tablaHistorico[i].nFact);
                            auxRecaudos += parseFloat(app.tablaHistorico[i].nRec);

                        }


                        let saldo = auxFacturado - auxRecaudado;

                        console.log(saldo)

                        if (saldo < 0) {

                            app.totalRecuperada = (saldo * -1);

                        } else if (saldo > 0) {

                            app.totalGenerada = saldo;

                        } else {

                            app.totalGenerada = saldo;
                            app.totalRecuperada = saldo;

                        }

                        //app.sumaComer = "Cart Recuperada: $" + app.totalRecuperada.toLocaleString() + " - " + "Cart Generada: $" + app.totalGenerada.toLocaleString();

                        app.totalRecaudado = "$ " + auxRecaudado.toLocaleString();
                        app.totalFacturado = "$ " + auxFacturado.toLocaleString();
                        app.totalFacturas = "" + auxFacturas.toLocaleString();
                        app.totalRecaudos = "" + auxRecaudos.toLocaleString();
                        app.totalRecuperada = "$ " + app.totalRecuperada.toLocaleString();
                        app.totalGenerada = "$ " + app.totalGenerada.toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&cliente=" + app.idUsuario +
                            "&regimen=" + app.regimenUsuarioReporte +
                            "&nombre=" + app.nomUsuarioReporte
                        );

                    } catch (e) {
                        alertify.error('Error')
                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();


    },

});
