// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });
    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,
        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectMunicipio: [],
        valorMun: "",

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectComercializador: [],
        valorComercializador: "",

        tablaConsumos: [],
        arrayEexcel: [],
        camposTabla: [],
        arrayConsumos: [],

    },


    methods: {

        loadMunicipios: function () {

            let app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            let app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            let app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            let app = this;
            app.valorAño = cod;
        },

        loadMes: function () {

            let app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            let app = this;
            app.valorMes = cod;
        },

        loadComercializador: function () {

            let app = this;

            $.post('./request/getComercializador.php', function (data) {
                app.selectComercializador = JSON.parse(data);
            });

        },

        onChangeComercializador: function (cod) {
            let app = this;
            app.valorComercializador = cod;
        },

        loadCamposTabla: function () {

            let app = this;

            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == "" ||
                app.valorComercializador == ""
            ) {

                alertify.error("Por favor selecciona todas las opciones")

            } else {

                $.post('./request/getCamposConsumo.php', {
                    mun: app.valorMun,
                    comer: app.valorComercializador,
                }, function (data) {
                    //console.log(data)
                    app.camposTabla = JSON.parse(data);


                    if (app.camposTabla.length <= 0) {
                        alertify.error('El comercializador de este municipio no cuenta con una configuracion de campos para importacion')
                    } else {
                        console.log(app.camposTabla)
                        app.verificar();
                    }

                });
            }
        },


        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectComercializador')

            app.tablaConsumos = [];

        },

        verificar: function (msg) {

            var app = this;
            //console.log(app.valorMun)
            //console.log(app.valorAño)
            //console.log(app.valorMes)
            //console.log(app.valorComercializador)


            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == "" ||
                app.valorComercializador == ""
            ) {

                alertify.error("Por favor selecciona todas las opciones")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                }, function (data) {

                    console.log(data)

                    if (data.length <= 2) {
                        alertify.success("No se ha importado registros de cartera para esta busqueda todavia")

                        app.btn_cargar = true;

                    } else {

                        if (msg != 1) {
                            alertify.warning("Existen registros de cartera para esta busqueda")
                        }

                        app.btn_borra = true;

                        var datos = jQuery.parseJSON(data);
                        app.tablaConsumos = datos;

                    }

                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });

            }


        },

        eliminar: function (dato) {
            var app = this;
            alertify.confirm("Eliminar", ".. Desea eliminar los registros de Recaudo?",
                function () {
                    $.post('./request/deleteConsumo.php', {
                        mun: dato.mun,
                        anio: dato.anio,
                        mes: dato.codMes,
                        comer: dato.codComer,
                    }, function (data) {
                        console.log(data);
                        if (data == 1) {
                            alertify.success("Registros Eliminados.");
                            app.tablaConsumos = [];
                            app.verificar()
                        } else {
                            alertify.error("Error al eliminar");

                        }
                    });

                },
                function () {
                    // alertify.error('Cancel');
                });
        },

        cargar: function () {


            $("#archivo").trigger("click");

        },

        leerExcel: function (excel) {

            var app = this;
            /* set up XMLHttpRequest */
            var url = excel;
            var oReq = new XMLHttpRequest();
            oReq.open("GET", url, true);
            oReq.responseType = "arraybuffer";

            oReq.onload = function (e) {
                var arraybuffer = oReq.response;

                /* convert data to binary string */
                var data = new Uint8Array(arraybuffer);
                var arr = new Array();
                for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
                var bstr = arr.join("");

                /* Call XLSX */
                var workbook = XLSX.read(bstr, {type: "binary"});

                /* DO SOMETHING WITH workbook HERE */
                var first_sheet_name = workbook.SheetNames[0];
                /* Get worksheet */
                var worksheet = workbook.Sheets[first_sheet_name];


                app.arrayEexcel = (XLSX.utils.sheet_to_json(worksheet, {header: 1}, {defval: ""}));

                app.armarArray()

            }

            oReq.send();

        },

        subeExcel: function () {


            var app = this;
            var formData = new FormData($('.formGuardar')[0]);

            //hacemos la petición ajax
            $.ajax({
                url: './uploadFile.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: formData,

                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,
                //mientras enviamos el archivo
                beforeSend: function () {
                },
                success: function (data) {

                    app.leerExcel("./temp/" + data)

                },
                //si ha ocurrido un error
                error: function (FormData) {
                    ////console.log(data)
                }
            });

        },

        armarArray: function () {

            var app = this;

            //console.log(app.camposTabla)
            //console.log(app.arrayEexcel)


            app.arrayConsumos = [];

            for (var i in app.arrayEexcel) {
                var array = [];
                for (var j in app.camposTabla) {

                    if (i != 0) {

                        if ((app.arrayEexcel[i][app.camposTabla[j].codigo]) != null) {

                            array[j] = app.arrayEexcel[i][app.camposTabla[j].codigo];

                        } else {

                            array[j] = "";

                        }

                    }

                }
                app.arrayConsumos.push(array)
            }
            app.arrayConsumos.shift()
            //console.log(app.arrayConsumos)

            app.enviarArray()


        },

        enviarArray: function () {

            var app = this;

            $.ajax({
                url: './request/insertConsumo.php',
                type: 'POST',
                // Form data
                //datos del formulario
                data: {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                    array: app.arrayConsumos,
                    campos: app.camposTabla
                },

            }).done(function (data) {

                console.log(data)

                if (data.indexOf("1[]") > -1) {

                    alertify.success("Cartera importada correctamente");
                    app.verificar(1)

                } else {
                    alertify.error("Ha ocurrido un error");
                }

            }).fail(function (data) {

            });

        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        this.loadComercializador();


    },

});
