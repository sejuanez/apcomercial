<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 69vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Importar Cartera Fallida

            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_cargar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 65px;" @click="cargar();">
					<i class="fa fa-upload" aria-hidden="true"></i> Cargar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="loadCamposTabla();">
					<i class="fa fa-check" aria-hidden="true"></i> Verificar
		    	</span>

            <span id="btnBuscar" v-show="btn_borrando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spin fa-spinner" aria-hidden="true"></i> Borrando
		    	</span>

            <span id="btnBuscar" v-show="btn_cargando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Cargando
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">


        <div class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectAño" id="selectAño" name="selectAño"
                            v-model="valorAño">
                        <option value="">Año...</option>
                        <option v-for="selectActividad in selectAño"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-label-group">
                    <input type="text" id="codigo" class="form-control" placeholder="Email address" required=""
                           autofocus=""
                           autocomplete="nope"
                           v-model="numero">
                    <label for="codigo">Numero</label>
                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectComercializador" id="selectComercializador" name="selectComercializador"
                            v-model="valorComercializador">
                        <option value="">Comercializador...</option>
                        <option v-for="selectActividad in selectComercializador"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <form enctype="multipart/form-data" class="formGuardar" v-show="false">
                <div class="col">
                    <div class="form-group">
                        <input type="file" id="archivo" name="archivo" class="" accept=".xlsx, .xls, .csv"
                               autocomplete="nope"
                        >

                    </div>
                </div>
            </form>

        </div>


    </div>

    <div class="container" style="max-width: 100%;">

        <div class="row">
            <div class="col-12 my-tbody">

                <table class="table table-sm responsive-table table-striped">
                    <thead class="fondoGris">
                    <tr class="cabecera">
                        <th width="150">Municipio</th>
                        <th width="100">Año</th>
                        <th width="100">Numero</th>
                        <th width="200">Comercializador</th>
                        <th style="text-align: center;" width="100">Acción</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaConsumos.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaConsumos">
                        <td v-text="dato.mun" width="150"></td>
                        <td v-text="dato.anio" width="100"></td>
                        <td v-text="dato.nro" width="100"></td>
                        <td v-text="dato.comer" width="200"></td>
                        <td width="100" style="text-align: center;">
                            <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="eliminar(dato)"></i>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
