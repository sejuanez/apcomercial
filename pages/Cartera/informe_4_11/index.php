<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">

        @media (min-width: 992px) {
            .modal-lg {
                max-width: 1000px;
            }
        }

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }


        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }

        table tr.cabecera2 th {
            color: #000000;
            text-align: center;
            font-weight: normal;
            font-size: 1em;
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 50vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            text-align: center;
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Reporte de Facturacion corriente y Cartera 4.11


            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verificar();">
					<i class="fa fa-check" aria-hidden="true"></i> Verificar
		    	</span>


            <span v-show="tablaHistorico.length != 0" id="btnExportarComer"
                  class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>


            <span id="btnBuscar" v-show="btn_cargando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Cargando
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>
        <div class="row">


            <div class="col-sm-3"></div>

            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">
                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>
                </div>
            </div>


            <div class="col-12 col-sm-2">

                <input type="checkbox" id="checkbox" @click="check(1)" v-model="checkGen" disabled>
                <label for="checkbox">General</label>

                <input type="checkbox" id="checkbox2" @click="check(2)" v-model="checkEsp" disabled>
                <label for="checkbox2">Especiales</label>

            </div>


            <!--
            <div class="col-12 col-sm-6">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control col-2" readonly placeholder="Usuario: "
                    >
                    <span class="input-group-btn" @click="modalUsuario()">
								        <button class="btn btn-secondary" type="button" data-toggle="modal"

                                        ><i class=" fa fa-search"></i></button>
								      </span>
                    <input type="text" class="form-control" name="proveedor" required aria-label="" disabled
                           v-model="nombreUsuario">
                </div>
            </div>
            -->

        </div>

        <div class="row" style="margin-top: 8px">
            <div class="col-sm-3"></div>
            <div class="col-12 col-sm-3">
                <fieldset>
                    <legend style="text-align: center;">
                        &nbsp; Fecha &nbsp;
                    </legend>
                    <div class="row">

                        <div class="col-12 col-sm-12">
                            <div class="form-group input-group-sm">


                                <select class="selectAño" id="selectAño" name="selectAño"
                                        v-model="valorAño">
                                    <option value="">Año...</option>
                                    <option v-for="item in selectAño"
                                            :value="item.codigo">
                                        {{item.nombre}}
                                    </option>
                                </select>

                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>

            <div class="col-12 col-sm-3">
                <fieldset>
                    <legend style="text-align: center;">
                        &nbsp Fecha Fin &nbsp
                    </legend>
                    <div class="row">
                        <div class="col-12 col-sm-12">
                            <div class="form-group input-group-sm">
                                <select class="selectAñoFin" id="selectAñoFin" name="selectAñoFin"
                                        v-model="valorAñoFin">
                                    <option value="">Año...</option>
                                    <option v-for="item in selectAñoFin"
                                            :value="item.codigo">
                                        {{item.nombre}}
                                    </option>
                                </select>
                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>
        </div>


    </div>

    <div class="container" style="max-width: 100%;margin-top: 8px;">

        <div class="row">
            <div class="col-12 my-tbody">

                <!--
                                <table class="table" v-show="false">
                                    <thead>
                                    <tr>
                                        <th colspan="6"
                                            class="text-center"
                                            style="border-bottom: 2px solid #16243100;border-top: 1px solid #ffffff;font-size: 1.2em;"
                                            v-text="sumaComer"
                                        >
                                            Total Liquidado: $
                                        </th>
                                    </tr>
                                    </thead>
                                </table>
                -->
                <table class="table table-sm responsive-table table-striped">


                    <thead class="fondoGris">


                    <tr class="cabecera2" v-show="tablaHistorico.length != 0">
                        <th width="50"><strong>Totales</strong></th>
                        <th v-text="totalCartera"></th>
                        <th v-text="totalCorriente"></th>
                        <th v-text="total30"></th>
                        <th v-text="total60"></th>
                        <th v-text="total90"></th>
                        <th width="40"></th>
                    </tr>


                    <tr class="cabecera">
                        <th width="50">Año</th>
                        <th>Cartera</th>
                        <th>Corriente</th>
                        <th>Car < 30</th>
                        <th>Car < 60</th>
                        <th>Car > 90</th>
                        <th width="40">+</th>
                    </tr>
                    </thead>
                    <tbody id="detalle_gastos">
                    <tr v-if="tablaHistorico.length == 0">
                        <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                    </tr>
                    <tr v-for="(dato, index) in tablaHistorico">
                        <td v-text="dato.ano" width="50"></td>
                        <td>$ {{parseInt(dato.cartera, 10).toLocaleString()}}</td>
                        <td>$ {{parseInt(dato.corriente, 10).toLocaleString()}}</td>
                        <td>$ {{parseInt(dato.car30, 10).toLocaleString()}}</td>
                        <td>$ {{parseInt(dato.car60, 10).toLocaleString()}}</td>
                        <td>$ {{parseInt(dato.car90, 10).toLocaleString()}}</td>
                        <td width="40" style="text-align: center;">
                            <i class="fa fa-eye" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                               @click="detalle(dato)"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>


    <div id="modalDetalle" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"

         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Detalle</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">

                        <div class="col-12">
                            <table class="table table-sm table-hover" style="font-size: 0.7em">
                                <thead class="fondoGris">

                                <tr class="cabecera2">
                                    <th width="50">
                                        <strong>Totales</strong></th>
                                    <th width="60"></th>
                                    <th v-text="totalCarteraModal"></th>
                                    <th v-text="totalCorrienteModal"></th>
                                    <th v-text="total30Modal"></th>
                                    <th v-text="total60Modal"></th>
                                    <th v-text="total90Modal"></th>


                                </tr>

                                <tr>
                                    <th class="text-center" width="50">Año</th>
                                    <th class="text-center" width="60">Mes</th>
                                    <th class="text-center">Cartera</th>
                                    <th class="text-center">Corriente</th>
                                    <th class="text-center">Car < 30</th>
                                    <th class="text-center">Car < 60</th>
                                    <th class="text-center">Car > 90</th>


                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaDetalle">
                                    <td width="50" v-text="item.ano"></td>
                                    <td width="60" v-text="item.mes"></td>
                                    <td>$ {{parseInt(item.cartera, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(item.corriente, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(item.car30, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(item.car60, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(item.car90, 10).toLocaleString()}}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="button" class="btn btn-success" id="exportarDetalle">
                        <a style="text-decoration: none; color: #FFFF;">
                            <i aria-hidden="true" class="fa fa-download"></i> Exportar
                        </a>
                    </button>

                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>

            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
