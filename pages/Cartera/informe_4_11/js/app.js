// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        let usuario = $(".selectComercializador").val();
        let nombreUsuario = this.options[this.selectedIndex].text;
        app.onChangeComercializador(usuario, nombreUsuario);
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistorico_Resultado: [],

        tablaDetalle: [],

        selectComercializador: [],
        valorComercializador: "",
        nomComer: "",

        totalCartera: 0,
        totalCorriente: 0,
        total30: 0,
        total60: 0,
        total90: 0,
        totalRecuperada: 0,

        totalCarteraModal: 0,
        totalCorrienteModal: 0,
        total30Modal: 0,
        total60Modal: 0,
        total90Modal: 0,
        totalRecuperadaModal: 0,

        checkEsp: false,
        checkGen: false,

    },


    methods: {

        check: function (opcion) {
            var app = this;

            if (opcion === 1) {

                if (app.checkEsp) {
                    app.checkGen = true;
                    app.checkEsp = false;
                } else {
                    app.checkGen = false;
                }

            } else {

                if (app.checkGen) {
                    app.checkEsp = true;
                    app.checkGen = false;
                } else {
                    app.checkEsp = true;
                }


            }

        },

        modalDetalle: function (opcion) {

            if (opcion) {
                $('#modalDetalle').modal('show');
            } else {
                $('#modalDetalle').modal('hide');
            }

        },

        loadComercializador: function () {

            var app = this;

            $.post('./request/getComercializador.php', function (data, nom) {
                console.log(data)
                app.selectComercializador = JSON.parse(data);
            });

        },

        loadCamposTabla: function () {

            var app = this;

            $.post('./request/getCamposConsumo.php', {
                mun: app.valorMun
            }, function (data) {
                //console.log(data)
                app.camposTabla = JSON.parse(data);

                if (app.camposTabla.length <= 0) {
                    alertify.error('Este municipio no cuenta con una configuracion de campos para importacion')
                } else {

                    app.verificar();
                }

            });

        },

        onChangeComercializador: function (cod) {
            var app = this;
            app.valorComercializador = cod;
        },

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectAñoFin')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectMesFin')
            app.enableSelect2('.selectComercializador')

            /*
            $("#checkbox").prop("disabled", false);
            $("#checkbox2").prop("disabled", false);
            */

            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;


            if (
                app.valorMun == "" ||
                app.valorAño == ""
            ) {

                alertify.error("Por favor selecciona municipio y fecha")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;

                app.disableSelect2('.selectMunicipio');
                app.disableSelect2('.selectAño');
                app.disableSelect2('.selectAñoFin');
                app.disableSelect2('.selectMes');
                app.disableSelect2('.selectMesFin');
                app.disableSelect2('.selectComercializador');
                $("#checkbox").prop("disabled", true);
                $("#checkbox2").prop("disabled", true);


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    comer: app.valorComercializador,
                    anio: app.valorAño,
                    anioFin: app.valorAñoFin,
                    gen: app.checkGen,
                    esp: app.checkEsp,
                }, function (data) {

                    //console.log(data)

                    try {

                        app.tablaHistorico = JSON.parse(data);

                        let sumCartera = 0;
                        let susmCorriente = 0;
                        let sum30 = 0;
                        let sum60 = 0;
                        let sum90 = 0;
                        let sumRecuperada = 0;

                        for (let i in app.tablaHistorico) {
                            sumCartera += parseInt(app.tablaHistorico[i].cartera);
                            susmCorriente += parseFloat(app.tablaHistorico[i].corriente);
                            sum30 += parseInt(app.tablaHistorico[i].car30);
                            sum60 += parseFloat(app.tablaHistorico[i].car60);
                            sum90 += parseInt(app.tablaHistorico[i].car90);
                            sumRecuperada += parseFloat(app.tablaHistorico[i].recuperada);

                        }

                        app.totalCartera = "$ " + sumCartera.toLocaleString();
                        app.totalCorriente = "$ " + parseInt(susmCorriente, 10).toLocaleString();
                        app.total30 = "$ " + parseInt(sum30, 10).toLocaleString();
                        app.total60 = "$ " + parseInt(sum60, 10).toLocaleString();
                        app.total90 = "$ " + parseInt(sum90, 10).toLocaleString();
                        app.totalRecuperada = "$ " + parseInt(sumRecuperada, 10).toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&anio=" + app.valorAño +
                            "&anioFin=" + app.valorAñoFin +
                            "&esp=" + app.checkEsp +
                            "&gen=" + app.checkGen
                        );

                    } catch (e) {
                        alertify.error('Busqueda mal formulada')
                        console.log(e)
                    }

                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

        detalle: function (item) {

            var app = this;

            console.log(item)

            $.ajax({
                url: './request/getDetalleMes.php',
                type: 'POST',
                data: {
                    mun: app.valorMun,
                    anio: item.ano,
                    esp: app.checkEsp,
                    gen: app.checkGen,
                },

            }).done(function (response) {


                try {

                    app.tablaDetalle = JSON.parse(response);
                    app.modalDetalle(true);

                    let sumCartera = 0;
                    let susmCorriente = 0;
                    let sum30 = 0;
                    let sum60 = 0;
                    let sum90 = 0;
                    let sumRecuperada = 0;

                    for (let i in app.tablaDetalle) {
                        sumCartera += parseInt(app.tablaDetalle[i].cartera);
                        susmCorriente += parseFloat(app.tablaDetalle[i].corriente);
                        sum30 += parseInt(app.tablaDetalle[i].car30);
                        sum60 += parseFloat(app.tablaDetalle[i].car60);
                        sum90 += parseInt(app.tablaDetalle[i].car90);
                        sumRecuperada += parseFloat(app.tablaDetalle[i].recuperada);
                    }

                    app.totalCarteraModal = "$ " + sumCartera.toLocaleString();
                    app.totalCorrienteModal = "$ " + parseInt(susmCorriente, 10).toLocaleString();
                    app.total30Modal = "$ " + parseInt(sum30, 10).toLocaleString();
                    app.total60Modal = "$ " + parseInt(sum60, 10).toLocaleString();
                    app.total90Modal = "$ " + parseInt(sum90, 10).toLocaleString();
                    app.totalRecuperadaModal = "$ " + parseInt(sumRecuperada, 10).toLocaleString();


                    $("#exportarDetalle a").attr("href",
                        "request/exportarDetalleMes.php?mun=" + app.valorMun +
                        "&anio=" + item.ano +
                        "&esp=" + app.checkEsp +
                        "&gen=" + app.checkGen
                    );


                } catch (e) {

                    console.log(response)

                }


                //return response;
            }).fail(function (error) {

                console.log(error)

            });

        }

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        //this.loadComercializador();
        // this.modalDetalle(true)

    },

});
