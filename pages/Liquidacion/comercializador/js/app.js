// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        Pace.start();
    });
    $(document).ajaxComplete(function () {
        Pace.restart();
    });

});


// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            id: "",
            codigo: "",
            nit: "",
            nombre: "",

            busqueda: "",

            codigo_actualizar: "",
            nit_modal: "",
            nombre_modal: "",


            tablaDepartamentos: [],

            noResultados: false,

        },


        methods: {

            loadAnio: function () {


                var app = this;
                $.get('./request/getAno.php', function (data) {
                    app.tablaDepartamentos = JSON.parse(data);
                });
            },

            verDepartamento: function (dato) {

                this.codigo_actualizar = dato.id;
                this.nit_modal = dato.nit;
                this.nombre_modal = dato.nombre;


                $('#addFucionario').modal('show');
            },

            resetModal: function () {
                $('#addFucionario').modal('hide');

                this.nit_modal = "";
                this.nombre_modal = "";
                this.codigo_actualizar = "";

            },

            buscar: function () {
                var app = this;

                if (app.busqueda != "") {
                    $.post('./request/getAno2.php', {
                        buscar: app.busqueda.toUpperCase()
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaDepartamentos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.loadAnio()

                }

            },

            actualizar: function () {

                if (this.nit_modal == "" ||
                    this.nombre_modal == "") {

                    $('.formGuardar').addClass('was-validated');
                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;

                    console.log(app.nombre_modal)
                    console.log(app.nit_modal)

                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/updateMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            id: app.codigo_actualizar,
                            nit_nuevo: app.nit_modal,
                            nombre_nuevo: app.nombre_modal,
                        },

                    }).done(function (data) {


                        console.log(data);

                        if (data == 1) {
                            alertify.success("Registro Actualizado Exitosamente");

                            app.resetModal();
                            app.buscar();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Ya existe un mes con este codigo");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }


                        //si ha ocurrido un error
                    }).fail(function (data) {
                        console.log(data)
                    });

                }


            },

            eliminar: function (dato) {
                var app = this;
                alertify.confirm("Eliminar", ".. Desea eliminar el item " + dato.nombre + " ?",
                    function () {
                        $.post('./request/deleteMes.php', {
                            codigo: dato.id,
                        }, function (data) {
                            console.log(data);
                            if (data == 1) {
                                alertify.success("Registro Eliminado.");
                                app.resetModal();
                                app.buscar();

                            } else {
                                alertify.error("Error al eliminar");
                                app.buscar();
                            }
                        });

                    },
                    function () {
                        // alertify.error('Cancel');
                    });
            },

            guardar: function () {


                if (
                    this.codigo == "" ||
                    this.nit == "" ||
                    this.nombre == ""
                ) {

                    alertify.error("Por favor ingresa todos los campos");

                } else {

                    var app = this;


                    //hacemos la petición ajax
                    $.ajax({
                        url: './request/insertMes.php',
                        type: 'POST',
                        // Form data
                        //datos del formulario
                        data: {
                            id: this.id,
                            codigo: this.codigo,
                            nit: this.nit,
                            nombre: this.nombre
                        },

                    }).done(function (data) {

                        console.log(data)

                        if (data == 1) {
                            alertify.success("Registro guardado Exitosamente");

                            app.resetCampos();
                            app.loadAnio();

                        } else {
                            if (data.indexOf("PRIMARY or UNIQUE KEY constraint") > -1) {


                                alertify.warning("Este codigo ya se encuentra registrado");

                            } else {
                                alertify.error("Ha ocurrido un error");

                            }
                        }

                    }).fail(function (data) {
                        console.log(data)
                    });


                }
            },

            resetCampos: function () {

                app = this;

                app.codigo = "";
                app.nombre = "";
                app.nit = "";
                app.id = "";

            }
        },


        watch: {}
        ,

        mounted() {

            this.loadAnio();


        }
        ,

    })
;
