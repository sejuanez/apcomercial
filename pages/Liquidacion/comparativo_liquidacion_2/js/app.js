// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);

    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);

    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);

    });

    $(".selectAñoFin").select2().change(function (e) {
        var usuario = $(".selectAñoFin").val();
        app.onChangeAñoFin(usuario);

    });

    $(".selectMesFin").select2().change(function (e) {
        var usuario = $(".selectMesFin").val();
        app.onChangeMesFin(usuario);

    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
        app.loadClientes();
    });

    $(".selectUsuario").select2().change(function (e) {
        var usuario = $(".selectUsuario").val();
        app.onChangeCliente(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectAñoFin: [],
        valorAñoFin: "",

        selectMesFin: [],
        valorMesFin: "",

        selectMunicipio: [],
        valorMun: "",

        selectUsuarios: [],
        valorUsuario: "",

        tablaPropios: [],
        tablaNoPropios: [],

        tablaClientes: [],
        buscarUsuario: "",

        idUsuario: "",
        nombreUsuario: "",

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: " ",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaHistorico: [],
        tablaHistoricoEspProp: [],
        tablaHistorico_Resultado: [],

        totalClientes: 0,
        totalAlumb: 0,
        totalFact: 0,
        totalDiferencia: 0,

        textComer: "Clientes Comercializador",
        textPropios: "Clientes Propios",


        totalClientesEspProp: 0,
        totalAlumbEspProp: 0,
        totalFactEspProp: 0,
        totalDiferenciaEspProp: 0,

        tabActive: 1,
    },


    methods: {


        modalUsuario: function () {
            var app = this;
            if (app.valorMun != "") {
                $("#inputBusqueda").focus();
                app.tablaClientes = [];
                $('#modalMateriales').modal('show');
            } else {
                alertify.warning('Por favor seleccione un municipio')
            }
        },


        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            $("#btnExportarComer a").attr("href",
                "request/exportarComer.php?mun=" + app.valorMun +
                "&anio=" + app.valorAño +
                "&mes=" + app.valorMes +
                "&opcion=" + app.tabActive
            );

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
                app.selectAñoFin = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
            app.verificaFecha()
        },

        onChangeAñoFin: function (cod) {
            var app = this;
            app.valorAñoFin = cod;
            app.verificaFecha()
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
                app.selectMesFin = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
            app.verificaFecha()
        },

        onChangeMesFin: function (cod) {
            var app = this;
            app.valorMesFin = cod;
            app.verificaFecha()
        },

        verificaFecha: function () {

            var app = this;

            if (
                this.valorAño != "" &&
                this.valorMes != "" &&
                this.valorAñoFin != "" &&
                this.valorMesFin != ""
            ) {


                var fi = new Date(parseInt(this.valorAño), parseInt(this.valorMes - 1), 0);
                fi.setHours(0, 0, 0, 0);


                var ff = new Date(parseInt(this.valorAñoFin), parseInt(this.valorMesFin - 1), 0);
                ff.setHours(0, 0, 0, 0);


                if (fi.getTime() > ff.getTime()) {
                    alertify.error("La fecha final no puede ser menor");
                }

            }
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectAñoFin')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectMesFin')
            app.enableSelect2('.selectComercializador')


            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];
            app.tablaHistorico_Resultado = [];
            app.tablaHistorico = [];
            app.idUsuario = "";
            app.nombreUsuario = "";
            app.buscarUsuario = "";
            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;


            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == ""
            ) {

                alertify.error("Selecciona todos los campos")

            } else {

                //app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectAñoFin')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectMesFin')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {

                    console.log(data)

                    try {

                        let todos = JSON.parse(data);
                        app.tablaHistorico = todos['CliComer'];
                        app.tablaHistoricoEspProp = todos['CliEspec'];

                        let auxVrAlum = 0;
                        let auxVrFac = 0;
                        let auxDiferencia = 0;

                        let auxVrAlumEspProp = 0;
                        let auxVrFacEspProp = 0;
                        let auxDiferenciaEspProp = 0;


                        for (let i in app.tablaHistorico) {


                            auxVrAlum += parseInt(app.tablaHistorico[i].vr_alumb);
                            auxVrFac += parseFloat(app.tablaHistorico[i].vr_fac);


                        }

                        for (let i in app.tablaHistoricoEspProp) {


                            auxVrAlumEspProp += parseInt(app.tablaHistoricoEspProp[i].vr_alumb);
                            auxVrFacEspProp += parseFloat(app.tablaHistoricoEspProp[i].vr_fac);


                        }

                        auxDiferenciaEspProp = auxVrAlumEspProp - auxVrFacEspProp;

                        app.totalClientesEspProp = (app.tablaHistoricoEspProp.length).toLocaleString();
                        app.totalAlumbEspProp = "$ " + parseInt(auxVrAlumEspProp, 10).toLocaleString();
                        app.totalFactEspProp = "$ " + parseInt(auxVrFacEspProp, 10).toLocaleString();
                        app.totalDiferenciaEspProp = "$ " + parseInt(auxDiferenciaEspProp, 10).toLocaleString();


                        auxDiferencia = auxVrAlum - auxVrFac;

                        app.totalClientes = (app.tablaHistorico.length).toLocaleString();
                        app.totalAlumb = "$ " + parseInt(auxVrAlum, 10).toLocaleString();
                        app.totalFact = "$ " + parseInt(auxVrFac, 10).toLocaleString();
                        app.totalDiferencia = "$ " + parseInt(auxDiferencia, 10).toLocaleString();


                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&anio=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&opcion=" + app.tabActive
                        );


                    } catch (e) {
                        alertify.error('Busqueda mal formulada')
                        console.log(e)
                    }


                    app.btn_cancelar = true;
                    //app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        acumular: function (dato) {

            console.log(dato)


            $.post('./request/insertAcumular.php', {
                mun: app.valorMun,
                anio: app.valorAño,
                mes: app.valorMes,
                cliente: dato.codCliente,
                valor: dato.diferencia,
            }, function (data) {

                console.log(data)

                if (data.indexOf("UNIQUE KEY constraint") > -1) {


                    alertify.confirm("Eliminar", ".. El cliente " + dato.cliente + " ya cuenta con un acumulado, desea eliminarlo" + " ?",
                        function () {
                            $.post('./request/deleteAcumulado.php', {
                                mun: app.valorMun,
                                anio: app.valorAño,
                                mes: app.valorMes,
                                cliente: dato.codCliente,
                                valor: dato.diferencia,
                            }, function (data) {
                                console.log(data);
                                if (data == 1) {
                                    alertify.success("Registro Eliminado.");

                                } else {
                                    alertify.error("Error al eliminar");
                                }
                            });

                        },
                        function () {
                            // alertify.error('Cancel');
                        });


                }


                if (data == 1) {

                    alertify.success("Acumulado");

                }


            });


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();

    },

});
