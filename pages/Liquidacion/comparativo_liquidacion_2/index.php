<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">


    <style type="text/css">

        fieldset {
            min-width: 0;
            padding-left: 10px;
            padding-right: 10px;
            border: 1px solid #cccccc;
        }

        legend {
            font-size: .7em;
            font-weight: bold;
            width: inherit;
        }


        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }

        table tr.cabecera2 th {
            color: #000000;
            text-align: center;
            font-weight: normal;
            font-size: 1.1em;
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 50vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            color: #444;
            line-height: 28px;
            text-align: center;
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Analisis Comparativo Liquidación

            <span id="btnBuscar" v-show="true" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verificar();">
					<i class="fa fa-check" aria-hidden="true"></i> Verificar
		    	</span>


            <span v-show="tablaHistorico.length != 0" id="btnExportarComer"
                  class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>


            <span id="btnBuscar" v-show="btn_cargando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Cargando
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>
        <div class="row">


            <div class="col-sm-4"></div>

            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>


            <!--
            <div class="col-12 col-sm-6">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control col-2" readonly placeholder="Usuario: "
                    >
                    <span class="input-group-btn" @click="modalUsuario()">
								        <button class="btn btn-secondary" type="button" data-toggle="modal"

                                        ><i class=" fa fa-search"></i></button>
								      </span>
                    <input type="text" class="form-control" name="proveedor" required aria-label="" disabled
                           v-model="nombreUsuario">
                </div>
            </div>
            -->

        </div>

        <div class="row" style="margin-top: 8px">
            <div class="col-sm-3"></div>
            <div class="col-12 col-sm-6">
                <fieldset>
                    <legend>&nbsp Fecha &nbsp</legend>
                    <div class="row">

                        <div class="col-12 col-sm-6">
                            <div class="form-group input-group-sm">


                                <select class="selectAño" id="selectAño" name="selectAño"
                                        v-model="valorAño">
                                    <option value="">Año...</option>
                                    <option v-for="item in selectAño"
                                            :value="item.codigo">
                                        {{item.nombre}}
                                    </option>
                                </select>

                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            <div class="form-group input-group-sm">


                                <select class="selectMes" id="selectMes" name="selectMes"
                                        v-model="valorMes">
                                    <option value="">Mes...</option>
                                    <option v-for="item in selectMes"
                                            :value="item.codigo">
                                        {{item.nombre}}
                                    </option>
                                </select>

                            </div>
                        </div>

                    </div>
                    <br>

                </fieldset>

            </div>
        </div>


    </div>

    <div class="container" style="max-width: 100%;margin-top: 8px;">

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                   aria-selected="true" v-text="textComer" @click="changeTab(1)">Consumos</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false" v-text="textPropios" @click="changeTab(2)">Errores</a>
            </li>

        </ul>

        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">
                            <table class="table table-sm responsive-table table-striped">


                                <thead class="fondoGris">


                                <tr class="cabecera2" v-show="tablaHistorico.length != 0">
                                    <th width="120"><strong>Totales</strong></th>
                                    <th v-text="totalClientes"></th>
                                    <th width="70"></th>
                                    <th v-text="totalAlumb"></th>
                                    <th width="70"></th>
                                    <th v-text="totalFact"></th>
                                    <th v-text="totalDiferencia"></th>
                                    <th width="80"></th>
                                </tr>


                                <tr class="cabecera">
                                    <th width="120">Cod Cliente</th>
                                    <th>Cliente</th>
                                    <th width="70">Tar Liq</th>
                                    <th>Vr Alumb</th>
                                    <th width="70">Tar Fac</th>
                                    <th>Vr Fac</th>
                                    <th>Diferencia</th>
                                    <th width="80">Acumular</th>

                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaHistorico.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaHistorico">
                                    <td v-text="dato.codCliente" width="120"></td>
                                    <td v-text="dato.cliente"></td>
                                    <td v-text="dato.tar_liq" width="70"></td>
                                    <td>$ {{parseInt(dato.vr_alumb,10).toLocaleString()}}</td>
                                    <td v-text="dato.tar_fac" width="70"></td>
                                    <td>$ {{parseInt(dato.vr_fac, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(dato.diferencia, 10).toLocaleString()}}</td>
                                    <td width="80" style="text-align: center;">
                                        <i class="fa fa-plus" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                                           @click="acumular(dato)"></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>


            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">
                            <table class="table table-sm responsive-table table-striped">


                                <thead class="fondoGris">


                                <tr class="cabecera2" v-show="tablaHistoricoEspProp.length != 0">
                                    <th width="120"><strong>Totales</strong></th>
                                    <th v-text="totalClientesEspProp"></th>
                                    <th width="70"></th>
                                    <th v-text="totalAlumbEspProp"></th>
                                    <th width="70"></th>
                                    <th v-text="totalFact"></th>
                                    <th v-text="totalDiferenciaEspProp"></th>
                                    <th width="80"></th>
                                </tr>


                                <tr class="cabecera">
                                    <th width="120">Cod Cliente</th>
                                    <th>Cliente</th>
                                    <th width="70">Tar Liq</th>
                                    <th>Vr Alumb</th>
                                    <th width="70">Tar Fac</th>
                                    <th>Vr Fac</th>
                                    <th>Diferencia</th>
                                    <th width="80">Acumular</th>

                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaHistoricoEspProp.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaHistoricoEspProp">
                                    <td v-text="dato.codCliente" width="120"></td>
                                    <td v-text="dato.cliente"></td>
                                    <td v-text="dato.tar_liq" width="70"></td>
                                    <td>$ {{parseInt(dato.vr_alumb,10).toLocaleString()}}</td>
                                    <td v-text="dato.tar_fac" width="70"></td>
                                    <td>$ {{parseInt(dato.vr_fac, 10).toLocaleString()}}</td>
                                    <td>$ {{parseInt(dato.diferencia, 10).toLocaleString()}}</td>
                                    <td width="80" style="text-align: center;">
                                        <i class="fa fa-plus" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                                           @click="acumular(dato)"></i>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

            </div>

        </div>


    </div>


    <div id="modalMateriales" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Usuarios</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <!-- <label for="proveedor">Marca</label> -->
                            <div class="input-group input-group-sm">
                                <input type="text" class="form-control col-sm-12"
                                       id="inputBusqueda"
                                       placeholder="Buscar por Codigo o Nombres"
                                       v-model="buscarUsuario"
                                       autocomplete="nope"
                                       v-on:keyup.enter="getUsuarios()"
                                >
                                <span class="input-group-btn">
								        <button class="btn btn-secondary" type="button" @click="getUsuarios()"><i
                                                    class="fa fa-search"></i></button>
								      </span>
                            </div>
                        </div>
                        <hr style="margin-bottom: 25px">
                        <div class="col-12">
                            <table class="table table-sm table-hover">
                                <thead class="fondoGris">
                                <tr>
                                    <th width="100">Codigo</th>
                                    <th class="text-center">Nombre</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="item in tablaClientes" @click="seleccionarCliente(item)">
                                    <td width="100" v-text="item.codigo"></td>
                                    <td v-text="item.nombre"></td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
