<?php
    session_start();
    if (!$_SESSION['user']) {
        echo
        "<script>
            window.location.href='../../inicio/index.php';
        </script>";
        exit();
    }
    ini_set('max_execution_time', 0);

    echo "\xEF\xBB\xBF"; // UTF-8 BOM

    include("../../../../init/gestion.php");

    $mun = $_GET['mun'];
    $anio = $_GET['anio'];
    $mes = $_GET['mes'];
    $op = $_GET['opcion'];

    $nombre = $mun . "_" . $anio . "_" . $mes;


    header("Content-type: application/vnd.ms-excel; name='excel'");
    header("Pragma: no-cache");
    header("Expires: 0");

    if ($op == '1') {

        $stmt = "Select * from INF_COMPARATIVO_LIQ(   '2' , 
                                              '" . $mun . "', 
                                              '" . $anio . "',                                                 
                                              '" . $mes . "'
                                                      )";

        header("Content-Disposition: filename=Analisis_Comparativo_Liquidacion(Comercializador)_" . $nombre . ".xls");

        $tabla = "<table>" .
            "<tr>" .
            "<th style='text-align: center' colspan='8'>Analisis Comparativo Liquidacion(Comercializador)  - $nombre</th>" .
            "</tr>" .
            "<tr class='cabecera'>" .
            "<th>Codigo Cliente</th>" .
            "<th>Nombre Cliente</th>" .
            "<th>Tarifa Liquidada</th>" .
            "<th>Vr Alumbrado</th>" .
            "<th>Tarifa Facturada</th>" .
            "<th>Vr Facturado</th>" .
            "<th>Diferencia</th>" .
            "</tr>";

    } else {

        $stmt = "Select * from INF_COMPARATIVO_LIQ(   '3' , 
                                              '" . $mun . "', 
                                              '" . $anio . "',                                                 
                                              '" . $mes . "'
                                                      )";

        header("Content-Disposition: filename=Analisis_Comparativo_Liquidacion(Especiales)_" . $nombre . ".xls");

        $tabla = "<table>" .
            "<tr>" .
            "<th style='text-align: center' colspan='8'>Analisis Comparativo Liquidacion(Especiales) - $nombre</th>" .
            "</tr>" .
            "<tr class='cabecera'>" .
            "<th>Codigo Cliente</th>" .
            "<th>Nombre Cliente</th>" .
            "<th>Tarifa Liquidada</th>" .
            "<th>Vr Alumbrado</th>" .
            "<th>Tarifa Facturada</th>" .
            "<th>Vr Facturado</th>" .
            "<th>Diferencia</th>" .
            "</tr>";

    }

    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);
    $return_arr = array();


    $vrAlumb = 0;
    $verFact = 0;
    $diferencia = 0;

    while ($fila = ibase_fetch_row($result)) {

        $vrAlumb += intval($fila[3]);
        $verFact += intval($fila[5]);


        $tabla .= "<tr class='fila'>" .
            "<td>" . ($fila[0]) . "</td>" .
            "<td>" . ($fila[1]) . "</td>" .
            "<td>" . ($fila[2]) . "</td>" .
            "<td>" . intval($fila[3]) . "</td>" .
            "<td>" . ($fila[4]) . "</td>" .
            "<td>" . intval($fila[5]) . "</td>" .
            "<td>" . intval($fila[6]) . "</td>" .
            "</tr>";

    }


    $tabla .= "<tr class='fila'>" .
        "<td style='text-align: center' colspan='3'> <strong>TOTALES</strong> </td>" .
        "<td>" . $vrAlumb . "</td>" .
        "<td>" . "</td>" .
        "<td>" . $verFact . "</td>" .
        "<td>" . intval($vrAlumb - $verFact) . "</td>" .
        "</tr>";

    $tabla .= "</table>";

    echo $tabla;

