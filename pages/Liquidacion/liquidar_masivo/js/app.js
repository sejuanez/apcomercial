// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------


let app = new Vue({
        el: '#app',
        data: {

            ajax: false,

            btn_cargando: false,
            btn_borrando: false,
            btn_cargar: false,
            btn_verificar: true,
            btn_verificando: false,
            btn_cancelar: false,
            btn_liquidar: false,
            btn_Re_liquidar: false,

            busqueda: "",

            selectMunicipio: [],
            valorMun: "",

            selectAño: [],
            valorAño: "",

            selectMes: [],
            valorMes: "",

            selectComercializador: [],
            valorComercializador: "",

            tablaConsumos: [],
            tablaExcluidos: [],
            arrayEexcel: [],
            camposTabla: [],
            arrayConsumos: [],

            liquidando: false,
            datos_liquidados: false,

            liquidados: "",

        },


        methods: {

            loadMunicipios: function () {

                var app = this;

                $.post('./request/getMunicipios.php', function (data) {
                    app.selectMunicipio = JSON.parse(data);
                });

            },

            onChangeMun: function (cod) {
                var app = this;
                app.valorMun = cod;
            },

            loadAño: function () {

                var app = this;

                $.post('./request/getAnio.php', function (data) {
                    app.selectAño = JSON.parse(data);
                });

            },

            onChangeAño: function (cod) {
                var app = this;
                app.valorAño = cod;
            },

            loadMes: function () {

                var app = this;

                $.post('./request/getMes.php', function (data) {
                    app.selectMes = JSON.parse(data);
                });

            },

            onChangeMes: function (cod) {
                var app = this;
                app.valorMes = cod;
            },

            cancelar: function () {

                var app = this;

                app.btn_cargando = false;
                app.btn_borrando = false;
                app.btn_cargar = false;
                app.btn_verificar = true;
                app.btn_verificando = false;
                app.btn_cancelar = false;
                app.btn_liquidar = false;
                app.btn_Re_liquidar = false;

                app.enableSelect2('.selectMunicipio')
                app.enableSelect2('.selectAño')
                app.enableSelect2('.selectMes')
                app.enableSelect2('.selectComercializador')

                app.tablaConsumos = [];
                app.tablaExcluidos = [];

            },

            verifica: function () {

                var app = this;


                if (
                    app.valorMun == "" ||
                    app.valorAño == "" ||
                    app.valorMes == ""
                ) {

                    alertify.error("Por favor selecciona todas las opciones")

                } else {

                    app.btn_verificar = false;
                    app.btn_verificando = true;


                    app.disableSelect2('.selectMunicipio')
                    app.disableSelect2('.selectAño')
                    app.disableSelect2('.selectMes')


                    $.post('./request/verificaLiquidados.php', {
                        mun: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes,
                    }, function (data) {

                        console.log(data)

                        if (data.length <= 2) {

                            app.buscar('liquidar');
                        } else {

                            app.buscar('re-liquidar');

                        }


                    });

                }

            },

            buscar: function (opcion) {

                var app = this;


                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')


                $.post('./request/buscaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {


                    if (data.length <= 2) {
                        alertify.success("No se ha importado un consumo para esta busqueda todavia")

                        app.btn_cargar = true;
                        app.btn_cancelar = true;
                        app.btn_verificar = true;
                        app.btn_cargando = false;
                        app.btn_verificando = false;

                    } else {

                        app.btn_borra = true;
                        var datos = jQuery.parseJSON(data);
                        app.tablaConsumos = datos;
                        app.buscarExcluidos();

                        if (opcion == 'liquidar') {

                            app.btn_cancelar = true;
                            app.btn_liquidar = true;
                            app.btn_Re_liquidar = false;
                            app.btn_verificar = false;
                            app.btn_cargando = false;
                            app.btn_verificando = false;

                        } else {

                            app.btn_cancelar = true;
                            app.btn_liquidar = false;
                            app.btn_Re_liquidar = true;
                            app.btn_verificar = false;
                            app.btn_cargando = false;
                            app.btn_verificando = false;

                        }

                    }

                });


            },

            filtrar: function () {
                var app = this;


                if (app.busqueda != "") {
                    $.post('./request/filtraConsumo.php', {
                        mun: app.valorMun,
                        cliente: app.busqueda.toUpperCase(),
                        anio: app.valorAño,
                        mes: app.valorMes,
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaConsumos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.buscar()

                }

            },

            buscarExcluidos: function () {
                $.post('./request/buscaExcluidos.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {
                    console.log(data)
                    app.tablaExcluidos = jQuery.parseJSON(data);
                });
            },

            excluir: function (dato, index) {
                var app = this;

                $.post('./request/excluir.php', {
                    mun: dato.mun,
                    anio: dato.anio,
                    mes: dato.mes,
                    cliente: dato.cliente
                }, function (data) {
                    console.log(data)
                    app.verifica();
                });

            },

            liquidar: function (dato, index) {
                var app = this;

                console.log(dato)

                $.post('./request/liquidar_uno.php', {
                    id: dato.id
                }, function (data) {
                    console.log(data)
                    app.verifica();
                });

            },

            liquidarTodo: function (opcion) {
                var app = this;


                $.ajax({
                    url: './request/liquidar_todo.php',
                    type: 'POST',
                    data: {
                        mun: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes,
                        opcion: opcion
                    },

                }).done(function (response) {

                    console.log(response)

                    var liquidados = JSON.parse(response);
                    var propios = 0;
                    var noPropios = 0;

                    for (liquidado in liquidados) {
                        if (liquidados[liquidado]['array']['propio'] == '0') {
                            propios++;
                        } else if (liquidados[liquidado]['array']['propio'] == '1') {
                            noPropios++;
                        }

                    }

                    var msg = "";

                    opcion == 1 ? msg = "Liquidados" : msg = "Re-Liquidados";

                    alertify.alert(msg,
                        "<strong>Liquidados por alumbrado: </strong>" + propios +
                        "<br>" +
                        "<strong>Liquidados por comercializador: </strong>" + noPropios);


                    app.verifica();


                }).fail(function (error) {

                    console.log(error)

                });


            },

            Re_liquidarTodo: function () {
                var app = this;

                app.valorMun = "CR76248";
                app.valorAño = "2018";
                app.valorMes = "2";

                $.post('./request/liquidar_todo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes
                }, function (data) {


                    console.log(data)

                    console.log(JSON.parse(data))


                    /*
                    if (data.indexOf("Resource id #7") > -1) {
                        alertify.success("Liquidacion masiva realizada.");
                        app.buscar();
                    } else {
                        if (data.indexOf("violation of PRIMARY or UNIQUE KEY constraint") > -1) {
                            alertify.warning("Estos clientes ya fueron liquidados");
                        } else {
                            alertify.error("Ha ocurrido un error");
                        }
                    }
                    */

                });

            },

            disableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', true);
                $(class_select).select2();

            },

            enableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', false);
                $(class_select).select2();

            },


        },


        watch: {},

        mounted() {
            this.loadMunicipios();
            this.loadAño();
            this.loadMes();
        },

    })
;
