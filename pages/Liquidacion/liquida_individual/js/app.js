// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
        app.buscaUVT();
        app.loadZona();
        app.loadTarifa();
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
        app.buscaUVT();
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
        app.buscaUVT()
    });

    $(".selectCliente").select2().change(function (e) {
        var usuario = $(".selectCliente").val();
        app.onChangeCliente(usuario);

    });

    $(".selectZona").select2().change(function (e) {
        var usuario = $(".selectZona").val();
        app.onChangeZona(usuario);
    });

    $(".selectTarifa").select2().change(function (e) {
        var usuario = $(".selectTarifa").val();
        app.onChangeTarifa(usuario);
    });

    $('.noSigno').keydown(function (e) {
        if (e.keyCode == 109 || e.keyCode == 107 || e.keyCode == 189 || e.keyCode == 187) {
            return false;
        }
    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
        el: '#app',
        data: {

            ajax: false,

            btn_cargando: false,
            btn_borrando: false,
            btn_cargar: false,
            btn_verificar: true,
            btn_verificando: false,
            btn_cancelar: false,
            btn_liquidar: false,
            btn_Re_liquidar: false,

            busqueda: "",
            uvt: "",

            selectMunicipio: [],
            valorMun: "",

            selectAño: [],
            valorAño: "",

            selectMes: [],
            valorMes: "",

            selectCliente: [],
            valorCliente: "",

            comer: "",

            tipoCliente: "",

            selectZona: [],
            valorZona: "",

            selectTarifa: [],
            valorTarifa: "",

            consumo: "",
            consumoKWH: "",
            vr: "",

            tablaConsumos: [],
            tablaExcluidos: [],
            arrayEexcel: [],
            camposTabla: [],
            arrayConsumos: [],

            liquidando: false,
            datos_liquidados: false,

            liquidados: "",

        },


        methods: {

            buscaUVT: function () {
                var app = this;

                $("#selectCliente").val("").trigger('change')
                $("#selectZona").val("").trigger('change')
                $("#selectTarifa").val("").trigger('change')
                app.valorCliente = "";
                app.tipoCliente = "";
                app.valorZona = "";
                app.valorTarifa = "";
                app.consumo = "";
                app.consumoKWH = "";

                app.btn_Re_liquidar = false;
                app.btn_liquidar = false;

                if (app.valorMun != "" && app.valorAño != "" && app.valorMes != "") {

                    app.selectCliente = []
                    app.loadCliente();
                    app.loadClientePropio();

                    $.post('./request/getUVT.php', {
                        municipio: app.valorMun,
                        anio: app.valorAño,
                        mes: app.valorMes
                    }, function (data) {
                        console.log(data)
                        app.uvt = data;
                    });

                } else {
                    app.uvt = "";
                }


            },

            loadMunicipios: function () {

                var app = this;

                $.post('./request/getMunicipios.php', function (data) {
                    app.selectMunicipio = JSON.parse(data);
                });

            },

            onChangeMun: function (cod) {
                var app = this;
                app.valorMun = cod;
            },

            loadAño: function () {

                var app = this;

                $.post('./request/getAnio.php', function (data) {
                    app.selectAño = JSON.parse(data);
                });

            },

            onChangeAño: function (cod) {
                var app = this;
                app.valorAño = cod;
            },

            loadMes: function () {
                var app = this;
                $.post('./request/getMes.php', function (data) {
                    app.selectMes = JSON.parse(data);
                });
            },

            onChangeMes: function (cod) {
                var app = this;
                app.valorMes = cod;
            },

            loadTarifa: function () {
                var app = this;
                $.post('./request/getTarifa.php', {
                    mun: app.valorMun
                }, function (data) {
                    app.selectTarifa = JSON.parse(data);
                });
            },

            onChangeTarifa: function (cod) {
                var app = this;
                app.valorTarifa = cod;
            },

            loadZona: function () {
                var app = this;
                $.post('./request/getZona.php', {
                    mun: app.valorMun
                }, function (data) {
                    app.selectZona = JSON.parse(data);
                });
            },

            onChangeZona: function (cod) {
                var app = this;
                app.valorZona = cod;
            },

            loadCliente: function () {
                var app = this;
                $.post('./request/getCliente.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes
                }, function (data) {
                    var data = JSON.parse(data);
                    console.log(data)
                    if (data.length > 0) {
                        app.selectCliente = app.selectCliente.concat(data);
                    }
                });
            },

            loadClientePropio: function () {
                var app = this;
                $.post('./request/getClientePropio.php', {
                    mun: app.valorMun
                }, function (data) {
                    var data = JSON.parse(data);
                    if (data.length > 0) {
                        console.log(data)
                        app.selectCliente = app.selectCliente.concat(data);
                    }
                });
            },

            onChangeCliente: function (cod) {
                var app = this;
                app.valorCliente = cod;

                console.log(app.selectCliente)


                for (cliente in app.selectCliente) {

                    if (app.selectCliente[cliente]['codigo'] == cod) {

                        app.verifica();
                        app.consumo = "";
                        app.consumoKWH = "";

                        app.comer = app.selectCliente[cliente]['comer'];

                        console.log(app.comer)

                        if (app.selectCliente[cliente]['tipo'] == 0) {

                            app.tipoCliente = 'No Propio';

                            $("#selectZona").val(app.selectCliente[cliente]['zona']).trigger('change')
                            $("#selectTarifa").val(app.selectCliente[cliente]['tarifa']).trigger('change')

                        } else if (app.selectCliente[cliente]['tipo'] == 1) {

                            app.tipoCliente = 'Propio';
                            $("#selectZona").val(app.selectCliente[cliente]['zona']).trigger('change')
                            $("#selectTarifa").val(app.selectCliente[cliente]['tarifa']).trigger('change')

                        }

                    }

                }

            },

            cancelar: function () {

                var app = this;

                app.btn_cargando = false;
                app.btn_borrando = false;
                app.btn_cargar = false;
                app.btn_verificar = true;
                app.btn_verificando = false;
                app.btn_cancelar = false;
                app.btn_liquidar = false;
                app.btn_Re_liquidar = false;

                app.enableSelect2('.selectMunicipio')
                app.enableSelect2('.selectAño')
                app.enableSelect2('.selectMes')
                app.enableSelect2('.selectComercializador')

                app.tablaConsumos = [];
                app.tablaExcluidos = [];

            },

            verifica: function () {

                var app = this;


                $.post('./request/verificaLiquidados.php', {
                    mun: app.valorMun,
                    cliente: app.valorCliente,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {

                    var liquidado = JSON.parse(data);

                    console.log(liquidado.length);

                    if (liquidado.length == 0) {
                        app.btn_liquidar = true
                        app.btn_Re_liquidar = false
                    } else {
                        app.btn_liquidar = false
                        app.btn_Re_liquidar = true
                    }


                });


            },

            buscar: function (opcion) {

                var app = this;


                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')


                $.post('./request/buscaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {

                    console.log(data)

                    if (data.length <= 2) {
                        alertify.success("No se ha importado un consumo para esta busqueda todavia")

                        app.btn_cargar = true;
                        app.btn_cancelar = true;
                        app.btn_verificar = true;
                        app.btn_cargando = false;
                        app.btn_verificando = false;

                    } else {

                        app.btn_borra = true;
                        var datos = jQuery.parseJSON(data);
                        app.tablaConsumos = datos;
                        app.buscarExcluidos();

                        if (opcion == 'liquidar') {

                            app.btn_cancelar = true;
                            app.btn_liquidar = true;
                            app.btn_Re_liquidar = false;
                            app.btn_verificar = false;
                            app.btn_cargando = false;
                            app.btn_verificando = false;

                        } else {

                            app.btn_cancelar = true;
                            app.btn_liquidar = false;
                            app.btn_Re_liquidar = true;
                            app.btn_verificar = false;
                            app.btn_cargando = false;
                            app.btn_verificando = false;

                        }

                    }

                });


            },

            filtrar: function () {
                var app = this;


                if (app.busqueda != "") {
                    $.post('./request/filtraConsumo.php', {
                        mun: app.valorMun,
                        cliente: app.busqueda.toUpperCase(),
                        anio: app.valorAño,
                        mes: app.valorMes,
                    }, function (data) {
                        console.log(data)
                        var datos = jQuery.parseJSON(data);
                        app.tablaConsumos = datos;

                        app.busqueda = "";

                    });
                } else {

                    app.buscar()

                }

            },

            buscarExcluidos: function () {
                $.post('./request/buscaExcluidos.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                }, function (data) {
                    console.log(data)
                    app.tablaExcluidos = jQuery.parseJSON(data);
                });
            },

            excluir: function (dato, index) {
                var app = this;

                $.post('./request/excluir.php', {
                    mun: dato.mun,
                    anio: dato.anio,
                    mes: dato.mes,
                    cliente: dato.cliente
                }, function (data) {
                    console.log(data)
                    app.verifica();
                });

            },

            liquidar: function (dato, index) {
                var app = this;

                console.log(dato)

                $.post('./request/liquidar_uno.php', {
                    id: dato.id
                }, function (data) {
                    console.log(data)
                    app.verifica();
                });

            },

            liquidarTodo: function (opcion) {
                var app = this;

                if (app.consumo == "" || app.consumoKWH == "") {
                    alertify.error('Por favor ingresa los consumos')
                } else {

                    $.ajax({
                        url: './request/liquidar_todo.php',
                        type: 'POST',
                        data: {
                            opcion: opcion,
                            mun: app.valorMun,
                            cliente: app.valorCliente,
                            zona: app.valorZona,
                            tarifa: app.valorTarifa,
                            anio: app.valorAño,
                            mes: app.valorMes,
                            vr: app.consumo,
                            kwh: app.consumoKWH,
                            comer: app.comer
                        },

                    }).done(function (response) {

                        console.log(response)

                        var liquidado = (JSON.parse(response))
                        console.log(liquidado)

                        app.vr = liquidado[0]['array']['vr_alumbrado'];
                        alertify.success('Cliente Liquidado')

                    }).fail(function (error) {

                        console.log(error)

                    });
                }

            },

            Re_liquidarTodo: function () {
                var app = this;


                $.post('./request/liquidar_todo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes
                }, function (data) {


                    console.log(data)

                    console.log(JSON.parse(data))


                    /*
                    if (data.indexOf("Resource id #7") > -1) {
                        alertify.success("Liquidacion masiva realizada.");
                        app.buscar();
                    } else {
                        if (data.indexOf("violation of PRIMARY or UNIQUE KEY constraint") > -1) {
                            alertify.warning("Estos clientes ya fueron liquidados");
                        } else {
                            alertify.error("Ha ocurrido un error");
                        }
                    }
                    */

                });

            },

            disableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', true);
                $(class_select).select2();

            },

            enableSelect2: function (class_select) {

                $(class_select).select2('destroy');
                $(class_select).prop('disabled', false);
                $(class_select).select2();

            },


        },


        watch: {},

        mounted() {
            this.loadMunicipios();
            this.loadAño();
            this.loadMes();
        },

    })
;
