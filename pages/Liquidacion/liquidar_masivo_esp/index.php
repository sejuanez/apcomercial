<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">


    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 57vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }


        .loader {
            border: 16px solid #f3f3f3; /* Light grey */
            border-top: 16px solid #00034A; /* Blue */
            border-radius: 50%;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }

    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Liquidar Masivo Clientes Especiales Propios

            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verifica();">
					<i class="fa fa-search" aria-hidden="true"></i> Buscar
		    	</span>

            <span id="btnBuscar" v-show="btn_liquidar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="liquidarTodo(1);">
					<i class="fa fa-list" aria-hidden="true"></i> Liquidar
		    	</span>

            <span id="btnBuscar" v-show="btn_Re_liquidar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="liquidarTodo(2);">
					<i class="fa fa-list" aria-hidden="true"></i> Re-Liquidar
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">


        <div v-if="ajax" class="loading">Loading&#8230;</div>


        <div class="row">


            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo"
                                v-text="selectActividad.codigo + ' - ' + selectActividad.nombre">

                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">


                    <select class="selectAño" id="selectAño" name="selectAño"
                            v-model="valorAño">
                        <option value="">Año...</option>
                        <option v-for="selectActividad in selectAño"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>


            <div class="col-12 col-sm-4">
                <div class="form-group input-group-sm">


                    <select class="selectMes" id="selectMes" name="selectMes"
                            v-model="valorMes">
                        <option value="">Mes...</option>
                        <option v-for="selectActividad in selectMes"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-4"></div>
            <!--
                        <div class="col-12 col-sm-4" style="padding-top: 5px">

                            <div class="col-sm-12 ">
                                <div class="input-group float-right input-group-sm"><input type="text"
                                                                                           placeholder="Filtrar por codigo nombre"
                                                                                           v-model="busqueda"
                                                                                           class="form-control"> <span
                                            class="input-group-btn"><button type="button"
                                                                            v-on:click="filtrar()"
                                                                            class="btn btn-secondary">Buscar</button></span>
                                </div>
                            </div>

                        </div>
            -->

        </div>

        <br>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                   aria-selected="true" v-text="liquidarText">Liquidar</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false" v-text="excluidosText">Excluidos</a>
            </li>

        </ul>
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th width="150">Codigo</th>
                                    <th>Cliente</th>
                                    <th style="text-align: center;" width="100">Excluir</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaConsumos.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaConsumos">
                                    <td v-text="dato.codigo" width="150"></td>
                                    <td v-text="dato.cliente"></td>
                                    <td width="100" style="text-align: center;">
                                        <i class="fa fa-trash-o" title="" style="cursor:pointer; margin: 0 0 10px 10px;"
                                           @click="excluir(dato, index)"></i>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>


            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th width="150">Municipio</th>
                                    <th width="100">Año</th>
                                    <th width="100">Mes</th>
                                    <th width="200">Cod Cliente</th>
                                    <th style="text-align: center;" width="100">Liquidar</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaExcluidos.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaExcluidos">
                                    <td v-text="dato.mun" width="150"></td>
                                    <td v-text="dato.anio" width="100"></td>
                                    <td v-text="dato.mes" width="100"></td>
                                    <td v-text="dato.cliente" width="200"></td>
                                    <td width="100" style="text-align: center;">
                                        <i class="fa fa-check" title=""
                                           style="cursor:pointer; margin: 0 0 10px 10px;"
                                           @click="liquidar(dato, index)"></i>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>

        </div>


    </div>

    <div id="addFucionario" class="modal fade bd-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">

                    <h6 class="modal-title" id="exampleModalLabel"> Año
                    </h6>


                </div>

                <div class="modal-body">

                    <div v-if="liquidando" class="row">

                        <div class="col"></div>

                        <div class="col-2">

                            <div class="loader text-center"></div>

                        </div>

                        <div class="col"></div>

                    </div>

                    <div v-if="datos_liquidados" class="row">

                        <code v-text="liquidados"></code>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" v-if="true" class="btn btn-secondary btn-sm" @click="resetModal()">
                        Cerrar
                    </button>

                    <button type="button" v-if="true" class="btn btn-success btn-sm" @click="actualizar()">
                        Exportar
                    </button>


                </div>

            </div>


        </div>
    </div>


</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
