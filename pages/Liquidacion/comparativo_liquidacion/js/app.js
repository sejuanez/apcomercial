// ----------------------------------------------
// Evento select Placa
// ----------------------------------------------

jQuery(document).ready(function ($) {

    $(document).ajaxStart(function () {
        //Pace.start();
        app.ajax = true;

    });
    $(document).ajaxComplete(function () {
        //Pace.restart();
        app.ajax = true;

        setTimeout(function () {
            app.ajax = false;
        }, 500);

    });

    $(".selectMunicipio").select2().change(function (e) {
        var usuario = $(".selectMunicipio").val();
        app.onChangeMun(usuario);
    });

    $(".selectAño").select2().change(function (e) {
        var usuario = $(".selectAño").val();
        app.onChangeAño(usuario);
    });

    $(".selectMes").select2().change(function (e) {
        var usuario = $(".selectMes").val();
        app.onChangeMes(usuario);
    });

    $(".selectComercializador").select2().change(function (e) {
        var usuario = $(".selectComercializador").val();
        app.onChangeComercializador(usuario);
    });


    $("#archivo").change(function () {

        app.subeExcel();

    });


});

// ----------------------------------------------
// ----------------------------------------------

let app = new Vue({
    el: '#app',
    data: {
        ajax: false,

        btn_cargando: false,
        btn_borrando: false,
        btn_cargar: false,
        btn_verificar: true,
        btn_verificando: false,
        btn_cancelar: false,

        selectMunicipio: [],
        valorMun: "",

        selectAño: [],
        valorAño: "",

        selectMes: [],
        valorMes: "",

        selectComercializador: [],
        valorComercializador: "",

        tablaPropios: [],
        tablaNoPropios: [],

        sumaPropio: "Total Liquidado: $",
        sumaComer: "Total Liquidado: $",

        textComercializador: "Comercializador",
        textPropio: "Especiales Propios",

        tabActive: 1,

        sumatoria: "Total Liquidado: $",

        tablaExportarCOmer: [],

    },


    methods: {

        changeTab: function (num) {
            var app = this;
            app.tabActive = num;

            if (app.tabActive == 1) {
                app.sumatoria = app.sumaComer
            } else {
                app.sumatoria = app.sumaPropio
            }

        },

        loadMunicipios: function () {

            var app = this;

            $.post('./request/getMunicipios.php', function (data) {
                app.selectMunicipio = JSON.parse(data);
            });

        },

        onChangeMun: function (cod) {
            var app = this;
            app.valorMun = cod;
        },

        loadAño: function () {

            var app = this;

            $.post('./request/getAnio.php', function (data) {
                app.selectAño = JSON.parse(data);
            });

        },

        onChangeAño: function (cod) {
            var app = this;
            app.valorAño = cod;
        },

        loadMes: function () {

            var app = this;

            $.post('./request/getMes.php', function (data) {
                app.selectMes = JSON.parse(data);
            });

        },

        onChangeMes: function (cod) {
            var app = this;
            app.valorMes = cod;
        },

        loadComercializador: function () {

            var app = this;

            $.post('./request/getComercializador.php', function (data) {
                app.selectComercializador = JSON.parse(data);
            });

        },

        loadCamposTabla: function () {

            var app = this;

            $.post('./request/getCamposConsumo.php', {
                mun: app.valorMun
            }, function (data) {
                //console.log(data)
                app.camposTabla = JSON.parse(data);


                if (app.camposTabla.length <= 0) {
                    alertify.error('Este municipio no cuenta con una configuracion de campos para importacion')
                } else {

                    app.verificar();
                }

            });

        },

        onChangeComercializador: function (cod) {
            var app = this;
            app.valorComercializador = cod;
        },

        cancelar: function () {

            var app = this;

            app.btn_cargando = false;
            app.btn_borrando = false;
            app.btn_cargar = false;
            app.btn_verificar = true;
            app.btn_verificando = false;
            app.btn_cancelar = false;

            app.enableSelect2('.selectMunicipio')
            app.enableSelect2('.selectAño')
            app.enableSelect2('.selectMes')
            app.enableSelect2('.selectComercializador')

            app.tablaConsumos = [];
            app.tablaPropios = [];
            app.tablaNoPropios = [];

            app.textComercializador = "Comercializador";
            app.textPropio = "Especiales Propios";
            app.sumatoria = "Total Liquidado: $ "

        },

        verificar: function (msg) {

            var app = this;
            //console.log(app.valorMun)
            //console.log(app.valorAño)
            //console.log(app.valorMes)
            //console.log(app.valorComercializador)


            if (
                app.valorMun == "" ||
                app.valorAño == "" ||
                app.valorMes == "" ||
                app.valorComercializador == ""
            ) {

                alertify.error("Por favor selecciona todas las opciones")

            } else {

                app.btn_verificar = false;
                app.btn_verificando = true;


                app.disableSelect2('.selectMunicipio')
                app.disableSelect2('.selectAño')
                app.disableSelect2('.selectMes')
                app.disableSelect2('.selectComercializador')


                $.post('./request/verificaConsumo.php', {
                    mun: app.valorMun,
                    anio: app.valorAño,
                    mes: app.valorMes,
                    comer: app.valorComercializador,
                }, function (data) {

                    console.log(data)


                    try {
                        var array = JSON.parse(data);
                        app.tablaPropios = array['propio']
                        app.tablaNoPropios = array['comer']

                        var totalPropios = 0;
                        var totalComer = 0;

                        for (i in app.tablaNoPropios) {
                            totalComer += parseFloat(app.tablaNoPropios[i]['vr_alumb'])
                        }

                        for (i in app.tablaPropios) {
                            totalPropios += parseFloat(app.tablaPropios[i]['vr_alumb'])
                        }

                        app.sumaPropio = "Total Liquidado: $" + totalPropios.toLocaleString('ES');
                        app.sumaComer = "Total Liquidado: $" + totalComer.toLocaleString('ES');

                        if (app.tabActive == 1) {
                            app.sumatoria = app.sumaComer
                        } else {
                            app.sumatoria = app.sumaPropio
                        }

                        app.textComercializador = "Comercializador " + "(" + app.tablaNoPropios.length + ")";
                        app.textPropio = "Especiales Propios " + "(" + app.tablaPropios.length + ")";

                        $("#btnExportarComer a").attr("href",
                            "request/exportarComer.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador
                        );

                        $("#btnExportarPropio a").attr("href",
                            "request/exportarPropio.php?mun=" + app.valorMun +
                            "&ano=" + app.valorAño +
                            "&mes=" + app.valorMes +
                            "&comer=" + app.valorComercializador
                        );
                    } catch (e) {
                        alertify.error('Error')
                    }


                    app.btn_cancelar = true;
                    app.btn_verificar = false;
                    app.btn_cargando = false;
                    app.btn_verificando = false;

                });


            }


        },

        disableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', true);
            $(class_select).select2();

        },

        enableSelect2: function (class_select) {

            $(class_select).select2('destroy');
            $(class_select).prop('disabled', false);
            $(class_select).select2();

        },

        JSONToCSVConvertor: function (JSONData, ReportTitle, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            //CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {

                    if (index == "valor") {
                        row += '' + arrData[i][index] + '';
                    } else {
                        row += '' + arrData[i][index] + ',';
                    }
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "CSV_Liquidacion";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        },

        exportarComer: function () {

            if (app.tablaNoPropios.length > 0) {

                var arrayTemp = [];
                app.tablaExportarCOmer = [];

                for (i in app.tablaNoPropios) {

                    var array = {
                        cliente: app.tablaNoPropios[i].cliente,
                        valor: parseInt(app.tablaNoPropios[i].vr_alumb, 10)
                    }

                    app.tablaExportarCOmer.push(array);

                }

                app.JSONToCSVConvertor(app.tablaExportarCOmer, app.valorMun + "_" + app.valorAño + "_" + app.valorMes, false)


            } else {
                alertify.error('No hay registros que exportar');
            }

        },

        exportarProp: function () {

            if (app.tablaPropios.length > 0) {

                var arrayTemp = [];
                app.tablaExportarCOmer = [];

                for (i in app.tablaPropios) {

                    var array = {
                        cliente: app.tablaPropios[i].cliente,
                        valor: parseInt(app.tablaPropios[i].vr_alumb, 10)
                    }

                    app.tablaExportarCOmer.push(array);

                }

                app.JSONToCSVConvertor(app.tablaExportarCOmer, 'Esp_Prop_' + app.valorMun + "_" + app.valorAño + "_" + app.valorMes, false)


            } else {
                alertify.error('No hay registros que exportar');
            }

        },

    },


    watch: {},

    mounted() {

        this.loadMunicipios();
        this.loadAño();
        this.loadMes();
        this.loadComercializador();


    },

});
