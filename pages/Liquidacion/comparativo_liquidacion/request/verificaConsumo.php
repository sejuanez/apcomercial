<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */

ini_set('max_execution_time', 0);

session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");


$mun = utf8_decode($_POST['mun']);
$anio = utf8_decode($_POST['anio']);
$mes = utf8_decode($_POST['mes']);
$comer = utf8_decode($_POST['comer']);

$return_arr = array();
$array_comer = array();
$array_propio = array();


$stmt = "Select * from LIQUIDA_COMPARA_BUSCA_COMERC(
                                      '1' ,
                                      '" . $mun . "' ,
                                      '" . $anio . "' ,
                                      '" . $mes . "' ,
                                      '" . $comer . "'
                                      )";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


while ($fila = ibase_fetch_row($result)) {
    $row_array['mun'] = utf8_encode($fila[0]);
    $row_array['anio'] = utf8_encode($fila[1]);
    $row_array['mes'] = utf8_encode($fila[2]);
    $row_array['cliente'] = utf8_encode($fila[3]);
    $row_array['nom_cliente'] = utf8_encode($fila[4]);
    $row_array['zona'] = utf8_encode($fila[5]);
    $row_array['tarifa'] = utf8_encode($fila[6]);
    $row_array['nomTarifa'] = utf8_encode($fila[7]);
    $row_array['tarifa_l'] = utf8_encode($fila[8]);
    $row_array['consumo_vr_l'] = utf8_encode($fila[9]);
    $row_array['consumo_kwh_l'] = utf8_encode($fila[10]);
    $row_array['vr_alumb'] = utf8_encode($fila[11]);
    array_push($array_comer, $row_array);
}


$stmt = "Select * from LIQUIDA_COMPARA_BUSCA_PROP(
                                      '1' ,
                                      '" . $mun . "' ,
                                      '" . $anio . "' ,
                                      '" . $mes . "' ,
                                      '" . $comer . "'
                                      )";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


while ($fila = ibase_fetch_row($result)) {
    $row_array['mun'] = utf8_encode($fila[0]);
    $row_array['anio'] = utf8_encode($fila[1]);
    $row_array['mes'] = utf8_encode($fila[2]);
    $row_array['cliente'] = utf8_encode($fila[3]);
    $row_array['nom_cliente'] = utf8_encode($fila[4]);
    $row_array['zona'] = utf8_encode($fila[5]);
    $row_array['tarifa'] = utf8_encode($fila[6]);
    $row_array['nomTarifa'] = utf8_encode($fila[7]);
    $row_array['tarifa_l'] = utf8_encode($fila[8]);
    $row_array['consumo_vr_l'] = utf8_encode($fila[9]);
    $row_array['consumo_kwh_l'] = utf8_encode($fila[10]);
    $row_array['vr_alumb'] = utf8_encode($fila[11]);
    array_push($array_propio, $row_array);
}

$return_arr = array(
    "comer" => $array_comer,
    "propio" => $array_propio
);

echo json_encode($return_arr);
// echo json_encode($array);



