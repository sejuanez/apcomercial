<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}

$mun = $_GET['mun'];
$ano = $_GET['ano'];
$mes = $_GET['mes'];
$comer = $_GET['comer'];

header("Content-type: application/vnd.ms-excel; name='excel'");
header("Pragma: no-cache");
header("Content-Disposition: filename=Liquidacion_Propios" . $mun . "_" . $ano . "_" . $mes . ".xls");
header("Expires: 0");

echo "\xEF\xBB\xBF"; // UTF-8 BOM

include("../../../../init/gestion.php");


$stmt = "Select * from LIQUIDA_COMPARA_BUSCA_PROP(
                                      '1' ,
                                      '" . $mun . "' ,
                                      '" . $ano . "' ,
                                      '" . $mes . "' ,
                                      '" . $comer . "'
                                      )";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


$tabla = "<table>" .
    "<tr>" .
    "<th style='text-align: center' colspan='12'>Liquidacion Clientes Especiales Propios - $mun $ano/$mes </th>" .
    "</tr>" .
    "<tr class='cabecera'>" .
    "<th>MUNICIPIO</th>" .
    "<th>ANIO</th>" .
    "<th>MES</th>" .
    "<th>COD CLIENTE</th>" .
    "<th>NOM CLIENTE</th>" .
    "<th>ZONA</th>" .
    "<th>TARIFA</th>" .
    "<th>NOM TARIFA</th>" .
    "<th>TARIFA LIQ</th>" .
    "<th>CONSUMO VR LIQ</th>" .
    "<th>CONSUMO KWH LIQ</th>" .
    "<th>VR ALUMBRADO</th>" .
    "</tr>";

$liquidado = 0;

while ($fila = ibase_fetch_row($result)) {

    $liquidado += intval($fila[11]);

    $tabla .= "<tr class='fila'>" .
        "<td>" . utf8_encode($fila[0]) . "</td>" .
        "<td>" . utf8_encode($fila[1]) . "</td>" .
        "<td>" . utf8_encode($fila[2]) . "</td>" .
        "<td>" . utf8_encode($fila[3]) . "</td>" .
        "<td>" . utf8_encode($fila[4]) . "</td>" .
        "<td>" . utf8_encode($fila[5]) . "</td>" .
        "<td>" . utf8_encode($fila[6]) . "</td>" .
        "<td>" . utf8_encode($fila[7]) . "</td>" .
        "<td>" . utf8_encode($fila[8]) . "</td>" .
        "<td>" . intval($fila[9]) . "</td>" .
        "<td>" . intval($fila[10]) . "</td>" .
        "<td>" . intval($fila[11]) . "</td>" .
        "</tr>";

}
$tabla .= "<tr class='fila'>" .
    "<td style='text-align: center' colspan='2'> <strong>TOTALES</strong> </td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td></td>" .
    "<td>" . $liquidado . "</td>" .
    "</tr>";

$tabla .= "</table>";


echo $tabla;

