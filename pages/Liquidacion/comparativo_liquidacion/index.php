<!DOCTYPE html>
<html xmlns:v-on="http://www.w3.org/1999/xhtml">

<head>
    <title>Incripcion</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/select2.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/floating-labels.css"/>
    <link rel="stylesheet" type="text/css" href="js/alertify/css/alertify.min.css">
    <link rel="stylesheet" type="text/css" href="js/alertify/css/themes/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css"/>

    <link rel="stylesheet" type="text/css" href="js/pace/themes/blue/pace-theme-flash.css">

    <style type="text/css">
        table tr.cabecera {
            background-color: #00034A;
        }


        .container-long {
            max-width: 80%;
        }

        table tr.cabecera th {
            color: #fff;
            text-align: center;
            font-weight: normal;
            /*font-size: .8em;*/
        }


        label {
            font-size: 0.8em;
        }

        .btn-label {

            padding-bottom: 5px;
            padding-top: 5px;

        }


        tbody {
            display: block;
            height: 59vh;
            overflow-y: scroll;
        }

        thead, tbody tr {
            display: table;
            width: 100%;
            text-align: center;
            table-layout: fixed; /* even columns width , fix width of table too*/
        }

        thead {
            width: calc(100% - 1em) /* scrollbar is average 1em/16px width, remove it from thead width */
        }

        table {
            width: 200px;
        }

        .fondoGris {
            background: #EFEFEF;
        }

        .form-group {
            margin-bottom: 0.2rem;
        }

        .nav-tabs .nav-link {
            background: #EFEFEF;
            color: #999;
        }


        .form-control[readonly] {
            background-color: #eee;
            opacity: 0.8;
        }

        #btnBuscar:hover {
            color: #333;
            text-shadow: 1px 0 #CCC;
        }

        select {
            padding: 3px;
            width: 100%;
        }

        .danger label {
            color: red;
        }

        .danger input {
            border: 1px solid red
        }

        .danger select {
            border: 1px solid red
        }
    </style>

</head>

<body>

<div id="app">

    <header>
        <p class="text-center fondoGris" style="padding: 10px;">

            Liquidacion

            <span id="btnBuscar" v-show="btn_cancelar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="cancelar();">
					<i class="fa fa-user-times" aria-hidden="true"></i> Cancelar
		    	</span>

            <span v-show="tablaNoPropios.length != 0 && tabActive==1" id="btnExportarComer" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>

            <span v-show="tablaNoPropios.length != 0 && tabActive==1" @click="exportarComer()" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar CSV
                    </a>
            </span>

            <span v-show="tablaPropios.length != 0 && tabActive==2" id="btnExportarPropio" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar
                    </a>
            </span>

            <span v-show="tablaNoPropios.length != 0 && tabActive==2" @click="exportarProp()" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
                <a style="text-decoration: none; color: #273b4f;">
                    <i aria-hidden="true" class="fa fa-download"></i> Exportar CSV
                    </a>
            </span>


            <span id="btnBuscar" v-show="btn_verificar" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;" @click="verificar();">
					<i class="fa fa-check" aria-hidden="true"></i> Verificar
		    	</span>

            <span id="btnBuscar" v-show="btn_borrando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spin fa-spinner" aria-hidden="true"></i> Borrando
		    	</span>

            <span id="btnBuscar" v-show="btn_cargando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Cargando
		    	</span>

            <span id="btnBuscar" v-show="btn_verificando" class="float-right"
                  style="font-size: .9em; cursor: pointer; width: 100px;">
					<i class="fa fa-spinner fa-spin" aria-hidden="true"></i> Verificando
		    	</span>


        </p>
    </header>


    <div class="container" style="padding-bottom: 5px; max-width: 95%;">

        <div v-if="ajax" class="loading">Loading&#8230;</div>
        <div class="row">


            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMunicipio" id="selectMunicipio" name="selectMunicipio"
                            v-model="valorMun">
                        <option value="">Municipio...</option>
                        <option v-for="selectActividad in selectMunicipio"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectAño" id="selectAño" name="selectAño"
                            v-model="valorAño">
                        <option value="">Año...</option>
                        <option v-for="selectActividad in selectAño"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectMes" id="selectMes" name="selectMes"
                            v-model="valorMes">
                        <option value="">Mes...</option>
                        <option v-for="selectActividad in selectMes"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>

            <div class="col-12 col-sm-3">
                <div class="form-group input-group-sm">


                    <select class="selectComercializador" id="selectComercializador" name="selectComercializador"
                            v-model="valorComercializador">
                        <option value="">Comercializador...</option>
                        <option v-for="selectActividad in selectComercializador"
                                :value="selectActividad.codigo">
                            {{selectActividad.nombre}}
                        </option>
                    </select>

                </div>
            </div>


        </div>


    </div>

    <div class="container" style="max-width: 95%; padding-top: 5px">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                   aria-selected="true" v-text="textComercializador" @click="changeTab(1)">Comercializador</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                   aria-controls="profile" aria-selected="false" v-text="textPropio" @click="changeTab(2)">Especiales
                    Propios</a>
            </li>

            <li class="nav-item" style="width: 50%;text-align: center;font-weight: 900;font-size: 1.1em;"><a
                        id="profile-tab" aria-controls="profile" aria-selected="false" class="nav-link"
                        v-text="sumatoria">Total
                    Liquidado:</a>
            </li>


        </ul>
        <div class="tab-content" id="myTabContent">

            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th valign="middle" width="120">Id Cliente</th>
                                    <th valign="middle">Nom Cliente</th>
                                    <th width="70">Zona</th>
                                    <th width="70">Tarifa</th>
                                    <th width="80">Tarifa L</th>
                                    <th width="120">Consumo VR</th>
                                    <th width="120">Consumo KWH</th>
                                    <th width="120">VR Liquidado</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaNoPropios.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>
                                <tr v-for="(dato, index) in tablaNoPropios">
                                    <td v-text="dato.cliente" width="120"></td>
                                    <td v-text="dato.nom_cliente"></td>
                                    <td v-text="dato.zona" width="70"></td>
                                    <td v-text="dato.tarifa" width="70"></td>
                                    <td v-text="dato.tarifa_l" width="80"></td>
                                    <td width="130">$ {{parseInt(dato.consumo_vr_l, 10).toLocaleString()}}</td>
                                    <td width="130">{{parseInt(dato.FAC_TOTAL, 10).toLocaleString()}}</td>
                                    <td width="130">$ {{parseInt(dato.vr_alumb, 10).toLocaleString()}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>


            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <div class="container" style="max-width: 100%;">

                    <div class="row">
                        <div class="col-12 my-tbody">

                            <table class="table table-sm responsive-table table-striped">
                                <thead class="fondoGris">
                                <tr class="cabecera">
                                    <th width="120">Id Cliente</th>
                                    <th>Nom Cliente</th>
                                    <th width="70">Zona</th>
                                    <th width="70">Tarifa</th>
                                    <th width="80">Tarifa L</th>
                                    <th width="120">Consumo VR</th>
                                    <th width="120">Consumo KWH</th>
                                    <th width="120">VR Liquidado</th>
                                </tr>
                                </thead>
                                <tbody id="detalle_gastos">
                                <tr v-if="tablaPropios.length == 0">
                                    <td class="text-center fondoGris" colspan="3">No se encontraron resultados</td>
                                </tr>

                                <tr v-for="(dato, index) in tablaPropios">
                                    <td v-text="dato.cliente" width="120"></td>
                                    <td v-text="dato.nom_cliente"></td>
                                    <td v-text="dato.zona" width="70"></td>
                                    <td v-text="dato.tarifa" width="70"></td>
                                    <td v-text="dato.tarifa_l" width="80"></td>
                                    <td v-text="dato.consumo_vr_l" width="120"></td>
                                    <td v-text="dato.consumo_kwh_l" width="120"></td>
                                    <td v-text="dato.vr_alumb" width="120"></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>

            </div>

        </div>
    </div>

</div>


<script src="js/jquery/jquery-3.2.1.min.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/xlsx.full.min.js"></script>
<script src="js/bootstrap/popper.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/alertify/alertify.min.js"></script>
<script src="js/pace/pace.min.js"></script>
<script src="js/vue/vue.js"></script>
<script src="js/app.js"></script>


</body>

</html>
