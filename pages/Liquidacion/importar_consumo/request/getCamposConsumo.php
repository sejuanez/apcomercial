<?php
/**
 * Created by PhpStorm.
 * User: KIKE
 * Date: 06/01/2019
 * Time: 6.46
 */


session_start();

if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../index.php';
        </script>";
    exit();
}

include("../../../../init/gestion.php");
// include("gestion.php");

$mun = $_POST['mun'];
$comer = $_POST['comer'];

// 1 en la query equivale a los campos para el proceso consumo

$stmt = "Select * from IMPTABLA_BUSCAR_ID_X_PROCESO('" . $mun . "' , '1', '" . $comer . "', null)";
$query = ibase_prepare($stmt);
$result = ibase_execute($query);


$return_arr = array();

$idTabla = "";

while ($fila = ibase_fetch_row($result)) {
    $idTabla = utf8_encode($fila[0]);
}

if ($idTabla == null || $idTabla == "") {

    $return_arr = array();
    echo json_encode($return_arr);

} else {

    $stmt = "Select * from IMPCAMPOS_BUSCAR_X_MUNIC('" . $mun . "' , '" . $idTabla . "', null)";
    $query = ibase_prepare($stmt);
    $result = ibase_execute($query);


    $return_arr = array();


    while ($fila = ibase_fetch_row($result)) {
        $row_array['nombre'] = utf8_encode($fila[1]);
        $row_array['campo'] = utf8_encode($fila[2]);
        $row_array['codigo'] = utf8_encode($fila[3]);
        $row_array['tipo'] = utf8_encode($fila[4]);
        array_push($return_arr, $row_array);
    }

// $array = array("result"=>$return_arr);

    echo json_encode($return_arr);
// echo json_encode($array);

}

