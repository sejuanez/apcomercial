<?php
  session_start();

  //incluimos el archivo que contiene la configuracion de la conexion a la bd
  include('gestion.php');

  if(!$_SESSION['user']){//no se ha inciciado sesion
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    }
    else //si se ha inciciado sesion
    {
    	$consulta="select * from empresa";
	   	$resultado=ibase_query($conexion, $consulta);
	   	$empresa=ibase_fetch_assoc($resultado); 

	   	//$direccion=$fila["DIRECCION"];
	   	//$telefono=$fila["TELEFONO"];
	   	//$empresa=$fila["NOMBRE"];

    	//obtenemos el nombre de usuario de inicio de sesion
    	$consulta  = "select UW_USUARIO, UW_NOMBRE from usu_web where UW_USUARIO='".$_SESSION[user]."'";
    	$resultado = ibase_query($conexion,$consulta);
    	$fila      = ibase_fetch_assoc($resultado);

    	$_SESSION[nombreUsuario]=$fila['UW_NOMBRE'];
    	//consultamos las opciones de menu habilitadas para el usuario que inicio sesion
    	$query="SELECT URL,MOD.mod_descripcion,per.items FROM PERMISOS PER, USU_WEB usu,modulos MOD WHERE PER.per_usuario=USU.UW_USUARIO and per.per_usuario='".$_SESSION[user]."' AND MOD.mod_codmodulo=PER.modulo AND PER.MODULO='2' order by per.orden ";
	
		$query1="SELECT URL,MOD.mod_descripcion,per.items FROM PERMISOS PER, USU_WEB usu,modulos MOD WHERE PER.per_usuario=USU.UW_USUARIO and per.per_usuario='".$_SESSION[user]."' AND MOD.mod_codmodulo=PER.modulo AND PER.MODULO='3' order by per.orden";
	
		$query2="SELECT URL,MOD.mod_descripcion,per.items FROM PERMISOS PER, USU_WEB usu,modulos MOD WHERE PER.per_usuario=USU.UW_USUARIO and per.per_usuario='".$_SESSION[user]."' AND MOD.mod_codmodulo=PER.modulo AND PER.MODULO='4' order by per.orden";
	
		$query3="SELECT URL,MOD.mod_descripcion,per.items FROM PERMISOS PER, USU_WEB usu,modulos MOD WHERE PER.per_usuario=USU.UW_USUARIO and per.per_usuario='".$_SESSION[user]."' AND MOD.mod_codmodulo=PER.modulo AND PER.MODULO='5' order by per.orden";
	
		$result=ibase_query($conexion,$query);
		$result1=ibase_query($conexion,$query1);
	    $result2=ibase_query($conexion,$query2);
		$result3=ibase_query($conexion,$query3);

		//Array con los nombres de los iconos para cada opcion de menu
		$arrayNombreIconos = array('Localizar Cuadrillas' => /*'ion-ios-location submenu-icon'*/'ion-android-locate submenu-icon', 'Recorrido Diario' => 'ion-android-car submenu-icon','Produccion Diaria' => 'ion-arrow-graph-up-right submenu-icon', 'Alumbrado' => 'ion-ios-lightbulb submenu-icon', 'Evidencias Alumbrado' => 'ion-images submenu-icon', 'Evidencias Fotograficas' => 'ion-images submenu-icon', 'Consulta Evidencias por Actas' => 'ion-image submenu-icon', 'Reporte de Daños' => 'ion-alert-circled submenu-icon', 'Luminarias por Municipio' => 'ion-ios-lightbulb submenu-icon'/*'ion-earth submenu-icon'*/, 'Funcionarios Activos' => 'ion-ios-body submenu-icon', 'Personigrama' => 'ion-ios-people submenu-icon', 'Novedades de Inasistencia' => 'ion-compose submenu-icon', 'Programacion Diaria' => 'ion-android-calendar submenu-icon','Visitas' =>'ion-android-bicycle submenu-icon', 'Consultar Orden' => 'ion-ios-book submenu-icon', 'Consultar Actas' => 'ion-ios-book submenu-icon', 'Estado de Ordenes' => 'ion-ios-information submenu-icon', 'Asignar Visitas' => 'ion-clipboard submenu-icon','Asignación de Ordenes' => 'ion-android-checkbox-outline submenu-icon', 'Resultado Visitas' => 'ion-stats-bars submenu-icon', 'Ingreso de Personas' => 'ion-android-contacts submenu-icon','Tablero de Control' => 'ion-ios-pulse-strong submenu-icon','Tablero Preventas' => 'ion-ios-pulse-strong submenu-icon','Seguimiento Ordenes' => 'ion-eye submenu-icon','Consultar Visitas' => 'ion-ios-search-strong submenu-icon','Vencimientos' => 'ion-alert-circled submenu-icon', 'ANS'=>'ion-android-time submenu-icon', 'Ordenes Pendientes Por Dia'=>'ion-clipboard submenu-icon', 'Historial Usuario'=>'ion-clipboard submenu-icon', 'Metas por Tecnicos'=>'ion-trophy submenu-icon', 'Informe del dia por gestor'=>'ion-document-text submenu-icon', 'Informe de Gestion de Cobro'=>'ion-calculator submenu-icon', 'Ubicacion de clientes' => 'ion-ios-location submenu-icon', 'Compras totales por proveedor' => 'ion-android-cart submenu-icon', 'Compras por meses' => 'ion-bag submenu-icon', 'Metas por vendedor' => 'ion-flag submenu-icon');
		
    }  
?>

<!DOCTYPE html>

<html>

<head>
	<meta charset="UTF-8">

	<title>Inicio</title>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

	<link rel="stylesheet" type="text/css" href="../css/estilo.css">

	<!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
	<link rel='stylesheet' href='../css/ionicons/css/ionicons.min.css' type='text/css' />


	<link rel='stylesheet' href='../css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)' />

	<script type="text/javascript" src="../js/jquery-ui/js/jquery-1.10.2.js"></script>

	<script type="text/javascript" src="../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js	"></script>

	<link rel="stylesheet" type="text/css" href="../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

	
	<script type="text/javascript">
		
		$(function(){


			
			/*------------- listener boton panel-menu -------------*/
			$("#navicon").click(function(){
				//$("#panel-menu").css('left',0);
				$("#panel-menu").toggleClass('show-panel-menu');
			});
			/*------------- fin listener boton panel-menu ----------*/


			$('#seccion-menu>nav>ul>li>a').click(function(e){
				e.preventDefault();
				
				//selector para cambiar la apariencia del la opcion seleccionada anteriormente
				$('#seccion-menu>nav>ul>li.menu-opcion-seleccionada').removeClass('menu-opcion-seleccionada');

				//selector para asignar la apariencia del la opcion de menu seleccionada ahora
				$(this).parent().toggleClass('menu-opcion-seleccionada');
				
						
				//selector para ocultar el submenu abierto anteriormente
				$(".submenu[data-submenuid!='"+$(this).attr('data-menuid')+"']").removeClass('show-submenu');
				

				//selector para mostrar/ocultar el submenu de la opcion cliqueada
				$(".submenu[data-submenuid='"+$(this).attr('data-menuid')+"']").toggleClass('show-submenu');
				
			});

			/*------------- listener clic opcion de submenu -------------*/
			$('ul.submenu>li>a').click(function(e){
				
				//desactivamos la navegacion del hipervinculo
				e.preventDefault();


				
				if($('#navicon').css('display')=='inline-block'){// si estamos en modo smartphone
					
					//ocultamos panel-menu
					$("#panel-menu").toggleClass('show-panel-menu');
				}


				//notificamos al controllerMenu que queremos acceder a la url del link
				/*$.ajax({
					url:'../controller/controllerMenu.php',
	                type:'POST',
	                dataType:'json',
	                data:e.target
				}).done(function(){
					$('iframe').attr('src',e.target);
				});*/
				//fin del ajax



				if(e.target.href!=document.getElementById('viewport').src){// si la opcion escogida es diferente a opcion actual
					
					//selector para remover la apariencia de la opcion de submenu seleccionada anteriormente
					$('ul.submenu>li>a.submenu-opcion-seleccionada').removeClass('submenu-opcion-seleccionada');

					//selector para asignar la apariencia de la opcion de submenu seleccionada ahora
					$(this).toggleClass('submenu-opcion-seleccionada');

					//cargamos la pagina del atributo href del vinculo
					$('iframe').attr('src',e.target);
				}

				
			});
			/*------------- fin listener clic opcion de menu -------------*/
			

		});

	</script>
	
</head>

	<body>

		<div id='wrapper'>

			<section id='seccion-header'>
				<header id='header-titulo'><span class='ion-navicon' id='navicon'></span><h3 id='titulo'><?php echo $empresa['NOMBRE']; ?></h3></header>
			</section>

			<section id = 'seccion-contenido'>
				
				<aside id= 'panel-menu'>
					<section id ='perfil-usuario'>
						<aside id ='panel-img-usuario'><img src="getFoto.php?usuario=<?php echo $fila['UW_USUARIO']; ?>" id='img-usuario'></aside>
						<aside id = 'panel-info-usuario'>
							<h5 id = 'nombre-usuario'><?php echo $fila['UW_NOMBRE']; ?></h5>
							<h6 id = 'rol-usuario'><?php echo $fila['UW_USUARIO']; ?></h6>
							<div id = 'estado-usuario'><span id='msg-estado'>Conectado</span><span id='icono-estado'></span><a href='cerrarSesion.php' id='salir'>Salir</a></div>
						</aside>
					</section>

					<section id='seccion-menu'>
						<nav>
							<ul>
								<li>
									<a href="#" id = 'menu-opcion-1' data-menuid='1'><span class='ion-settings menu-icon'></span>Operativa</a>
									<ul class='submenu' data-submenuid='1'>
										

										<?php 
											while($row=ibase_fetch_assoc($result))
											{ ?>
						   						<li><a href="<?php echo $row['URL']; ?>" title=""><span class='wrapermenu-icon'><span class='<?php echo $arrayNombreIconos[utf8_encode($row['ITEMS'])]; ?>'></span></span><?php echo utf8_encode($row['ITEMS']); ?></a></li>
                           				<?php } ?>

									</ul>
								</li>
								<li>
									<a href="#" id = 'menu-opcion-2' data-menuid='2'><span class='ion-person-stalker menu-icon'></span>Recursos humanos</a>
									<ul class='submenu' data-submenuid='2'>
										
										<?php 
											while($row1=ibase_fetch_assoc($result1))
											{ ?>
						   						<li><a href="<?php echo $row1['URL']; ?>" title=""><span class='wrapermenu-icon'><span class='<?php echo $arrayNombreIconos[utf8_encode($row1['ITEMS'])]; ?>'></span></span><?php echo utf8_encode($row1['ITEMS']); ?></a></li>
                           				<?php } ?>
									</ul>
								</li>
								<li>
									<a href="#" id = 'menu-opcion-3' data-menuid='3'><span class='ion-cash menu-icon'></span>Comercial</a>
									<ul class='submenu' data-submenuid='3'>
										<?php 
											while($row2=ibase_fetch_assoc($result2))
											{ ?>
						   						<li><a href="<?php echo $row2['URL']; ?>" title=""><span class='wrapermenu-icon'><span class='<?php echo $arrayNombreIconos[utf8_encode($row2['ITEMS'])]; ?>'></span></span><?php echo utf8_encode($row2['ITEMS']); ?></a></li>
                           				<?php } ?>
									</ul>
								</li>
								<!--<li><a href=""><span class='ion-gear-a menu-icon'></span>Configuracion</a></li>-->
								<li><a href=""><span class='ion-ios-information menu-icon'></span>Acerca de</a></li>
							</ul>
						</nav>
					</section>
				</aside>

				<aside id='panel-viewport'>
					<iframe id='viewport' src="../pages/inicio/index.php"></iframe>
				</aside>
			</section>

		</div>
		

	</body>

</html>