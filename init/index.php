<?php
    session_start();

    //incluimos el archivo que contiene la configuracion de la conexion a la bd
    include('gestion.php');

    if (!$_SESSION['user']) {//no se ha inciciado sesion
        echo
        "<script>
            window.location.href='../index.php';
        </script>";
        exit();
    } else //si se ha inciciado sesion
    {
        $consulta = "select * from empresa1";
        $resultado = ibase_query($conexion, $consulta);
        $empresa = ibase_fetch_assoc($resultado);

        //$direccion=$fila["DIRECCION"];
        //$telefono=$fila["TELEFONO"];
        //$empresa = $fila["NOMBRE"];


        //obtenemos el nombre de usuario de inicio de sesion
        $consulta = "select UW_USUARIO, UW_NOMBRE from usu_web where UW_USUARIO='" . $_SESSION['user'] . "'";
        $resultado = ibase_query($conexion, $consulta);
        $fila = ibase_fetch_assoc($resultado);

        $_SESSION['nombreUsuario'] = $fila['UW_NOMBRE'];


    }
?>

<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">

    <title>Inicio</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" type="text/css" href="../css/estilo.css">

    <!--<link rel='stylesheet' href='http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css' type='text/css' />-->
    <link rel='stylesheet' href='../css/ionicons/css/ionicons.min.css' type='text/css'/>


    <link rel='stylesheet' href='../css/smartphone.css' type='text/css' media='only screen and (max-width: 480px)'/>

    <script type="text/javascript" src="../js/jquery-ui/js/jquery-1.10.2.js"></script>

    <script type="text/javascript" src="../js/jquery-ui/js/jquery-ui-1.10.4.custom.min.js	"></script>

    <link rel="stylesheet" type="text/css" href="../js/jquery-ui/css/custom-theme/jquery-ui-1.10.4.custom.min.css">

    <link rel="shortcut icon" type="image/png" href="../img/icon.ico"/>


    <script type="text/javascript">

        $(function () {

            //Array con los nombres de los iconos para cada opcion de menu


            //console.log(arrayNombreIconos);
            $.ajax({
                url: 'getMenu.php',
                type: 'POST',
                dataType: 'json'

            }).done(function (repuesta) {

                //console.log(repuesta)

                var menu = "<ul>";

                for (var i = 0; i < repuesta.length; i++) {

                    //menu+="<li><a href='#' id = 'menu-opcion-"+repuesta[i].codModulo+"' data-menuid='"+repuesta[i].codModulo+"'><span class='wrapermenu-icon'><span class='"+arrayNombreIconos[0][repuesta[i].nombreModulo]+"'></span></span>"+repuesta[i].nombreModulo+"</a>";
                    menu += "<li><a href='#' style='font-weight: 700;' id = 'menu-opcion-" + repuesta[i].codModulo + "' data-menuid='" + repuesta[i].codModulo + "'><span class='wrapermenu-icon'><img src='getIconoModulo.php?codModulo=" + repuesta[i].codModulo + "' style='width:30px; height:30px;'></img></span>" + repuesta[i].nombreModulo + "</a>";


                    if (repuesta[i].subMenu.length > 0) {// si tiene submenu

                        var submenu = "<ul class='submenu' data-submenuid='" + repuesta[i].codModulo + "'>";
                        for (var j = 0; j < repuesta[i].subMenu.length; j++) {

                            //submenu+="<li><a href='"+repuesta[i].subMenu[j].url+"' title=''><span class='wrapermenu-icon'><span class='"+arrayNombreIconos[0][repuesta[i].subMenu[j].item]+"'></span></span>"+repuesta[i].subMenu[j].item+"</a></li>";
                            submenu += "<li><a href='" + repuesta[i].subMenu[j].url + "' title=''><span class='wrapermenu-icon'><img src='getIconoSubmenu.php?modulo=" + repuesta[i].subMenu[j].modulo + "&orden=" + repuesta[i].subMenu[j].item + "' style='width:15px; height:15px;'></img></span>" + repuesta[i].subMenu[j].descripcion + "</a></li>";

                        }
                        submenu += "</ul>";
                        menu += submenu;
                    }
                    menu += "</li>";
                }
                menu += "</ul>";

                $("#seccion-menu>nav").html(menu);


                /*------------- listener clic opcion de menu -------------*/
                $('#seccion-menu>nav>ul>li>a').click(function (e) {
                    e.preventDefault();

                    //selector para cambiar la apariencia del la opcion seleccionada anteriormente
                    $('#seccion-menu>nav>ul>li.menu-opcion-seleccionada').removeClass('menu-opcion-seleccionada');

                    //selector para asignar la apariencia del la opcion de menu seleccionada ahora
                    $(this).parent().toggleClass('menu-opcion-seleccionada');


                    //selector para ocultar el submenu abierto anteriormente
                    $(".submenu[data-submenuid!='" + $(this).attr('data-menuid') + "']").removeClass('show-submenu');


                    //selector para mostrar/ocultar el submenu de la opcion cliqueada
                    $(".submenu[data-submenuid='" + $(this).attr('data-menuid') + "']").toggleClass('show-submenu');

                });


                /*------------- listener clic opcion de submenu -------------*/
                $('ul.submenu>li>a').click(function (e) {

                    //desactivamos la navegacion del hipervinculo
                    e.preventDefault();


                    if ($('#navicon').css('display') == 'inline-block') {// si estamos en modo smartphone

                        //ocultamos panel-menu
                        $("#panel-menu").toggleClass('show-panel-menu');
                    }


                    //notificamos al controllerMenu que queremos acceder a la url del link
                    /*$.ajax({
                        url:'../controller/controllerMenu.php',
                        type:'POST',
                        dataType:'json',
                        data:e.target
                    }).done(function(){
                        $('iframe').attr('src',e.target);
                    });*/
                    //fin del ajax

                    //console.log(e.target.parentNode.parentNode.href);

                    if (e.target.href != document.getElementById('viewport').src) {// si la opcion escogida es diferente a opcion actual

                        //selector para remover la apariencia de la opcion de submenu seleccionada anteriormente
                        $('ul.submenu>li>a.submenu-opcion-seleccionada').removeClass('submenu-opcion-seleccionada');

                        //selector para asignar la apariencia de la opcion de submenu seleccionada ahora
                        $(this).toggleClass('submenu-opcion-seleccionada');


                        if (e.target.href) {
                            //cargamos la pagina del atributo href del vinculo
                            $('iframe').attr('src', e.target.href);
                        } else {
                            //cargamos la pagina del atributo href del vinculo
                            $('iframe').attr('src', e.target.parentNode.parentNode.href);
                        }


                    }


                });
                /*------------- fin listener clic opcion de menu -------------*/


                $("#panel-menu").show();
                $('iframe').attr('src', '../pages/inicio/index.php');
                //$('iframe').attr('src', '../pages/graficos/estadistico_recaudo_real/index.php');
                //$('iframe').attr('src','../pages/resumen_materiales/index.php');

            });

            /*------------- listener boton panel-menu -------------*/
            $("#navicon").click(function () {
                //$("#panel-menu").css('left',0);
                $("#panel-menu").toggleClass('show-panel-menu');
            });
            /*------------- fin listener boton panel-menu ----------*/


        });

    </script>

</head>

<body>

<div id='wrapper'>

    <section id='seccion-header'>
        <header id='header-titulo'><span class='ion-navicon' id='navicon'></span>
            <h3 id='titulo'><?php echo $empresa['NOMBRE']; ?></h3></header>
    </section>

    <section id='seccion-contenido'>

        <aside id='panel-menu' style='display:none'>
            <section id='perfil-usuario'>
                <aside id='panel-img-usuario'><img src="getFoto.php?usuario=<?php echo $fila['UW_USUARIO']; ?>"
                                                   id='img-usuario'></aside>
                <aside id='panel-info-usuario'>
                    <h5 id='nombre-usuario'><?php echo $fila['UW_NOMBRE']; ?></h5>
                    <h6 id='rol-usuario'><?php echo $fila['UW_USUARIO']; ?></h6>
                    <div id='estado-usuario'><span id='msg-estado'>Conectado</span><span id='icono-estado'></span><a
                                href='cerrarSesion.php' id='salir'>Salir</a></div>
                </aside>
            </section>

            <section id='seccion-menu'>
                <nav id='nav'></nav>

            </section>
        </aside>

        <aside id='panel-viewport'>
            <!--<iframe id='viewport' src="../pages/inicio/index.php"></iframe>-->
            <iframe id='viewport'></iframe>
        </aside>
    </section>

</div>


</body>

</html>
