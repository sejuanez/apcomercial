<?php
session_start();
if (!$_SESSION['user']) {
    echo
    "<script>
            window.location.href='../../inicio/index.php';
        </script>";
    exit();
}


include("gestion.php");

$usuario = $_SESSION['user'];

$consulta = "SELECT m.mod_codmodulo, m.mod_descripcion from modulos m order by m.mod_codmodulo";


function getSubmenu($cnx, $usuario, $codModulo)
{

    //$consulta = "SELECT URL, MOD.mod_descripcion, per.items FROM PERMISOS PER, USU_WEB usu,modulos MOD WHERE PER.per_usuario=USU.UW_USUARIO and per.per_usuario='".$usuario."' AND MOD.mod_codmodulo=PER.modulo AND PER.MODULO='".$codModulo."' order by per.modulo, per.orden ";

    $consulta = "
                    select ow.ow_modulo, ow.ow_orden,ow.ow_items, ow.ow_url, ow.ow_icono
                    from opciones_web ow
                    inner join permisos p on p.modulo = ow.ow_modulo and
                                        p.orden = ow.ow_orden and
                                        p.per_usuario = '" . $usuario . "'
                    where ow.ow_modulo = '" . $codModulo . "'
                    order by ow_orden
        ";

    $result = ibase_query($cnx, $consulta);

    $arr_submenu = array();

    while ($fila = ibase_fetch_row($result)) {

        $row_array['modulo'] = ($fila[0]);
        $row_array['item'] = ($fila[1]);
        $row_array['descripcion'] = ($fila[2]);
        $row_array['url'] = ($fila[3]);

        array_push($arr_submenu, $row_array);
    }


    return $arr_submenu;
}


$return_arr = array();

$result = ibase_query($conexion, $consulta);


while ($fila = ibase_fetch_row($result)) {


    $row_array['codModulo'] = utf8_encode($fila[0]);
    $row_array['nombreModulo'] = utf8_encode($fila[1]);
    $row_array['subMenu'] = getSubmenu($conexion, $usuario, $fila[0]);


    array_push($return_arr, $row_array);
}

echo json_encode($return_arr);

?>