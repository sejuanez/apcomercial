﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <link href="imagenes/empresap.gif" type="Imagen X-ICON" rel="shortcut icon"/>
    <link rel="STYLESHEET" type="text/css" href="css/fg_membersite.css"/>
    <link rel="shortcut icon" type="image/png" href="img/icon.ico"/>
    <title>.: Servicios Publicos :.</title>
</head>
<body>
<?php

include('init/gestion.php');
$sql = "select * from empresa1";
$resultado = ibase_query($conexion, $sql);
$row = ibase_fetch_assoc($resultado);
$direccion = $row["DIRECCION"];
$telefono = $row["TELEFONO"];
$empresa = $row["NOMBRE"];

?>

<div class="header">
    <h3>Iniciar sesión</h3>
    <!--<img src="css/images/empresa1.jpg" width="15%" height="15%"/>-->
</div>

<?php if (isset($_REQUEST['failed']) && $_REQUEST['failed'] == 'true') { ?>
    <div class="msg-error">
        <p>Fallo al iniciar sesión</p>
    </div>
<?php } ?>


<div id='fg_membersite'>

    <div id='div-logo'><img src="img/empresa.jpg" width="30%" height="30%"/></div>

    <form id="login" action="init/login.php" method="post">
        <fieldset>

            <div>
                <label for='username'>Usuario</label>
                <input type='text' name='usuario' id='usuario' maxlength="50" value=""/>
            </div>

            <div>
                <label for='password'>Contraseña</label>
                <input type='password' name='clave' id='clave' maxlength="50" value=""/>
            </div>

            <div>
                <input type='submit' name='Submit' value='Entrar'/>

            </div>

        </fieldset>
    </form>
</div>

<div class="footer">
    <?php
    echo "<h3>" . $empresa . "</h3>";
    echo "<h5>" . $direccion . "</h5>";
    echo "<h5>Teléfono(s): " . $telefono . "</h5>";
    ?>
</div>

</body>
</html>